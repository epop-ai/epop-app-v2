/* eslint-disable prettier/prettier */
import styled from 'styled-components/native';
import {GREEN} from '../../helper/Color';

const SplashStyles = {
  WrapperViewVertical: styled.View`
    flex: 1;
    justifyContent: center;
    backgroundColor: ${GREEN.app};
  `,
  SplashIcon: styled.Image`
    height: 100%;
    width: 100%;
    alignSelf: center;
    resizeMode: stretch;
  `,
};

export default SplashStyles;
