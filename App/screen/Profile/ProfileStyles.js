/* eslint-disable prettier/prettier */
import styled from 'styled-components/native';
import {BLACK, GREEN, WHITE} from '../../helper/Color';
import {FONT, SCREEN} from "../../helper/Constant";

const ProfileStyles = {
  WrapperViewVertical: styled.View`
    flex: 1;
    justifyContent: center;
    backgroundColor: ${GREEN.app};
  `,
  SafeView: styled.SafeAreaView`
    flex: 1;
  `,
  Scroll: styled.ScrollView`
    flex: 1;
  `,
  TopView: styled.View`
    flex: 0.2;
    justify-content: center;
  `,
  BottomView: styled.View`
    flex: 0.8;
  `,
  WhiteWrapperView: styled.View`
    backgroundColor: ${WHITE.dark};
    borderRadius: 10px;
    alignSelf: center;
    height: 95%;
    width: ${SCREEN.width - 40}px;
    paddingLeft: 18px;
    paddingRight: 25px;
    paddingTop: 35px;
    paddingBottom: 18px;
    alignItems: center;
  `,
  TopInnerWrapper: styled.View`
    height: 110px;
    width: 100px;
    align-self: center;
  `,
  InnerImage: styled.Image`
    height: 100px;
    width: 100px;
    borderRadius: 15px;
    align-self: flex-end;
    marginRight: 13px;
  `,
  UploadImageIcon: styled.TouchableOpacity`
    position: absolute;
    right: 10px;
    bottom: 0px;
    justify-content: center;
    align-items: center;
  `,
  UploadIcon: styled.Image`
    height: 25px;
    width: 25px;
    resizeMode: contain;
  `,
  SourceSans10: styled.Text`
    fontSize: 10px;
    color: ${WHITE.dark};
    fontFamily: ${FONT.SourceSansPro.bold};
    alignSelf: center;
    marginLeft: 25px;
  `,
  DetailViewWrapper: styled.View`
    width: 100%;
    height: 25px;
    justify-content: center;
    marginTop: 7.5px;
  `,
  Muli15Bold: styled.Text`
    fontFamily: ${FONT.Muli.bold};
    fontSize: 15px;
    color: ${BLACK.dark};
  `,
  AbsoluteRightView: styled.View`
    height: 10px;
    position: absolute;
    right: 0px;
    justify-content: center;
  `,
  TickIcon: styled.Image`
    height: 15px;
    width: 15px;
    resizeMode: contain;
    marginRight: 18px;
  `,
  Divider: styled.View`
    height: 1px;
    width: 100%;
    background-color: ${BLACK.borderLight};
    marginVertical: 14px;
    align-self: center;
  `,
  MuliRegular14: styled.Text`
    fontFamily: ${FONT.Muli.regular};
    fontSize: 15px;
    color: ${BLACK.appExtraLight};
  `,
  SupportBtn: styled.TouchableHighlight`
    height: 32px;
    paddingHorizontal: 18px;
    background-color: ${GREEN.btn};
    align-self: center;
    marginVertical: 6px;
    justifyContent: center;
    align-items: center;
    borderRadius: 4px;
  `,
  SignOutBtn: styled.TouchableHighlight`
    height: 44px;
    width: 100%;
    background-color: ${GREEN.app};
    align-self: center;
    marginVertical: 8px;
    justifyContent: center;
    align-items: center;
    borderRadius: 10px;
    margin-bottom: 40px;
  `,
  Muli16Bold: styled.Text`
    fontFamily: ${FONT.Muli.bold};
    color: ${WHITE.dark};
    fontSize: 16px;
  `,
  AddInterestWrapper: styled.TouchableOpacity`
    height: ${SCREEN.height}px;
    width: ${SCREEN.width}px;
    align-self: center;
  `,
  AddInterestInnerWrapper: styled.View`
    border-radius: 10px;
    width: 95%;
    background-color: ${WHITE.dark};
    margin-bottom: 10px;
    align-self: center;
  `,
  QuickSand16Regular: styled.Text`
    font-size: 16px;
    font-family: ${FONT.QuickSand.regular};
    color: ${BLACK.appLight};
    align-self: center;
    paddingHorizontal: 10px;
    padding-top: 5px;
  `,
  QuickSand16Bold: styled.Text`
    font-family: ${FONT.QuickSand.bold};
    font-size: 16px;
    color: ${BLACK.dark};
    font-weight: bold;
    paddingHorizontal: 10px;
    align-self: center;
  `,
  InterestList: styled.FlatList`
  `,
  InterestItemWrapper: styled.TouchableHighlight`
    height: ${SCREEN.width / 3.2}px;
    width: ${SCREEN.width / 3.2}px;
    justify-content: center;
    align-items: center;
  `,
  InterestItemInnerWrapper: styled.View`
    height: 100%;
    width: 100%;
    justify-content: center;
    align-items: center;
  `,
  InterestImage: styled.Image`
    height: 70%;
    width: 70%;
  `,
  InterestName: styled.Text`
    marginTop: 5px;
    fontFamily: ${FONT.SourceSansPro.regular};
    color: ${GREEN.app};
  `,
  TickInterestAbsolute: styled.Image`
    position: absolute;
    height: 40px;
    width: 40px;
    top: ${(SCREEN.width / 3.2)/4.5}px
  `,
  BottomButtonView: styled.View`
    height: 90px;
    justify-content: center;
    width: 100%;
  `,
  BottomRowView: styled.View`
    height: 46px;
    width: 100%;
    flex-direction: row;
  `,
  ButtonMainWrapper: styled.View`
    flex: 1;
    justify-content: center;
    align-items: center;
  `,
  ButtonInnerWrapper: styled.TouchableHighlight`
    height: 100%;
    width: 85%;
    border-radius: 4px;
    border-width: 0.3px;
    border-color: ${BLACK.appLight};
    shadowColor: ${BLACK.appLight};
    shadowOpacity: 0.3;
    shadowRadius: 10px;
    justify-content: center;
    align-items: center;
  `,
  BtnText: styled.Text`
    font-size: 16px;
    font-family: ${FONT.Muli.bold};
    color: ${WHITE.dark};
  `,
};

export default ProfileStyles;
