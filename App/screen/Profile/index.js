import React, {Component} from 'react';
import {Switch} from 'react-native';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Modal from "react-native-modal";

import * as Actions from '../../redux/actions';
import ProfileStyles from './ProfileStyles';
import HeaderWithTwoBtn from '../../component/HeaderWithTwoBtn';
import * as ImagePicker from "expo-image-picker";
import {GREEN, WHITE} from "../../helper/Color";
import {FONT, InterestList} from "../../helper/Constant";

var selectedInterest = [];

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageUri: '',
      backUpSwitch: false,
      openInterestModal: false,
      selectedItemList: [],
    }
  }
  
  pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });
    
    if (!result.cancelled) {
      this.setState({imageUri: result.uri});
    }
  };
  
  interestItemSelect(itemName) {
    if (selectedInterest.includes(itemName)) {
      const indexOfItem = selectedInterest.indexOf(itemName);
      selectedInterest.splice(indexOfItem, 1);
      this.setState({ selectedItemList: selectedInterest });
    } else {
      selectedInterest.push(itemName);
      this.setState({ selectedItemList: selectedInterest });
    }
  }
  
  
  render() {
    return (
      <ProfileStyles.WrapperViewVertical>
        <HeaderWithTwoBtn
          leftIcon={require('../../assets/Icons/32px/WithDarkBackground/back.png')}
          leftPress={() => this.props.navigation.pop()}
          title={'Hi Sunay'}/>
        <ProfileStyles.SafeView>
          <ProfileStyles.Scroll>
          <ProfileStyles.TopView>
            <ProfileStyles.TopInnerWrapper>
              <ProfileStyles.InnerImage source={this.state.imageUri ? {uri: this.state.imageUri} :
                require('../../assets/OtherIcons/contact.png')}/>
              <ProfileStyles.UploadImageIcon onPress={() => this.pickImage()} activeOpacity={0.9}>
                <ProfileStyles.UploadIcon source={require('../../assets/OtherIcons/updatePic.png')}/>
              </ProfileStyles.UploadImageIcon>
            </ProfileStyles.TopInnerWrapper>
            <ProfileStyles.SourceSans10>
              Update your profile picture
            </ProfileStyles.SourceSans10>
          </ProfileStyles.TopView>
          <ProfileStyles.BottomView>
            <ProfileStyles.WhiteWrapperView>
              <ProfileStyles.DetailViewWrapper>
                <ProfileStyles.Muli15Bold>
                  Sunay
                </ProfileStyles.Muli15Bold>
                <ProfileStyles.AbsoluteRightView>
                  <ProfileStyles.TickIcon source={require('../../assets/OtherIcons/greenTick.png')}/>
                </ProfileStyles.AbsoluteRightView>
              </ProfileStyles.DetailViewWrapper>
              <ProfileStyles.DetailViewWrapper>
                <ProfileStyles.Muli15Bold>
                  +44 7945 678 646
                </ProfileStyles.Muli15Bold>
                <ProfileStyles.AbsoluteRightView>
                  <ProfileStyles.TickIcon source={require('../../assets/OtherIcons/crossRed.png')}/>
                </ProfileStyles.AbsoluteRightView>
              </ProfileStyles.DetailViewWrapper>
              <ProfileStyles.DetailViewWrapper>
                <ProfileStyles.Muli15Bold>
                  Sunay
                </ProfileStyles.Muli15Bold>
                <ProfileStyles.AbsoluteRightView>
                  <ProfileStyles.TickIcon source={require('../../assets/OtherIcons/greenTick.png')}/>
                </ProfileStyles.AbsoluteRightView>
              </ProfileStyles.DetailViewWrapper>
              <ProfileStyles.Divider/>
              <ProfileStyles.DetailViewWrapper style={{marginVertical: 10}}>
                <ProfileStyles.MuliRegular14>
                  Backup to the Cloud
                </ProfileStyles.MuliRegular14>
                <ProfileStyles.AbsoluteRightView>
                  <Switch
                    style={{height: 23}}
                    value={this.state.backUpSwitch}
                    trackColor={GREEN.btn}
                    trackColor={{ false: "#3e3e3e", true: GREEN.switch_color }}
                    onValueChange={(value) => this.setState({backUpSwitch: value})}/>
                </ProfileStyles.AbsoluteRightView>
              </ProfileStyles.DetailViewWrapper>
              <ProfileStyles.DetailViewWrapper style={{marginVertical: 10}}>
                <ProfileStyles.MuliRegular14>
                  Face ID
                </ProfileStyles.MuliRegular14>
                <ProfileStyles.AbsoluteRightView>
                  <Switch
                    style={{height: 23}}
                    value={this.state.faceIdSwitch}
                    trackColor={GREEN.btn}
                    trackColor={{ false: "#3e3e3e", true: GREEN.switch_color }}
                    onValueChange={(value) => this.setState({faceIdSwitch: value})}/>
                </ProfileStyles.AbsoluteRightView>
              </ProfileStyles.DetailViewWrapper>
              <ProfileStyles.DetailViewWrapper style={{marginVertical: 10}}>
                <ProfileStyles.MuliRegular14>
                  Notifications
                </ProfileStyles.MuliRegular14>
                <ProfileStyles.AbsoluteRightView>
                  <Switch
                    style={{height: 23}}
                    value={this.state.notificationSwitch}
                    trackColor={GREEN.btn}
                    trackColor={{ false: "#3e3e3e", true: GREEN.switch_color }}
                    onValueChange={(value) => this.setState({notificationSwitch: value})}/>
                </ProfileStyles.AbsoluteRightView>
              </ProfileStyles.DetailViewWrapper>
              <ProfileStyles.DetailViewWrapper style={{marginVertical: 10}}>
                <ProfileStyles.MuliRegular14>
                  Interests
                </ProfileStyles.MuliRegular14>
                <ProfileStyles.AbsoluteRightView>
                  <Switch
                    trackStyle={{height: 10}}
                    value={this.state.interest}
                    trackColor={{ false: "#3e3e3e", true: GREEN.switch_color }}
                    onValueChange={(value) => {
                      if(value === true){
                        this.setState({openInterestModal: true})
                      }
                      this.setState({interest: value})}}/>
                </ProfileStyles.AbsoluteRightView>
              </ProfileStyles.DetailViewWrapper>
              <ProfileStyles.Divider />
              <ProfileStyles.MuliRegular14 styles={{alignSelf: 'center', textAlign: 'center'}}>
                Can we Help you with anything?
              </ProfileStyles.MuliRegular14>
              <ProfileStyles.SupportBtn>
                <ProfileStyles.MuliRegular14 style={{color: WHITE.dark}}>
                  Support
                </ProfileStyles.MuliRegular14>
              </ProfileStyles.SupportBtn>
              <ProfileStyles.SupportBtn style={{backgroundColor: GREEN.app}}>
                <ProfileStyles.MuliRegular14 style={{color: WHITE.dark}}>
                  Join ePOP Premium Today
                </ProfileStyles.MuliRegular14>
              </ProfileStyles.SupportBtn>
              <ProfileStyles.MuliRegular14 style={{fontFamily: FONT.Muli.bold, alignSelf: 'center'}}>
                Customer Reference Identifier
              </ProfileStyles.MuliRegular14>
              <ProfileStyles.MuliRegular14 style={{ alignSelf: 'center'}}>
                UKI-E32-SUN7
              </ProfileStyles.MuliRegular14>
              <ProfileStyles.SignOutBtn>
                <ProfileStyles.Muli16Bold>
                  Sign Out
                </ProfileStyles.Muli16Bold>
              </ProfileStyles.SignOutBtn>
            </ProfileStyles.WhiteWrapperView>
          </ProfileStyles.BottomView>
          </ProfileStyles.Scroll>
        </ProfileStyles.SafeView>
        <Modal
          coverScreen={true}
          hasBackdrop={true}
          backdropColor={GREEN.app}
          backdropOpacity={1}
          animationIn={"slideInUp"}
          isVisible={this.state.openInterestModal}
        >
          <ProfileStyles.AddInterestWrapper activeOpacity={1}>
            <HeaderWithTwoBtn
              leftIcon={require('../../assets/Icons/36px/WithDarkBackground/closeIconBackBlack.png')}
              leftPress={() => this.setState({openInterestModal: false})}
              title={'Your Interest'}
              rightIcon={require('../../assets/Icons/36px/WithDarkBackground/saveGreenBackBlack.png')}
            />
            <ProfileStyles.AddInterestInnerWrapper>
              <ProfileStyles.QuickSand16Regular>
                To help you get the most out of ePOP
              </ProfileStyles.QuickSand16Regular>
              <ProfileStyles.QuickSand16Bold>
                Choose 6 topics that interest you
              </ProfileStyles.QuickSand16Bold>
              <ProfileStyles.InterestList
                data={InterestList}
                keyExtractor={(item, index) => index.toString()}
                numColumns={3}
                renderItem={(item, index) => (
                  <ProfileStyles.InterestItemWrapper
                    onPress={() => this.interestItemSelect(item.item.name)}
                    underlayColor={WHITE.dark}
                  >
                    <ProfileStyles.InterestItemInnerWrapper>
                      <ProfileStyles.InterestImage
                        source={item.item.image}
                      />
                      <ProfileStyles.InterestName>
                        {item.item.name}
                      </ProfileStyles.InterestName>
                      {this.state.selectedItemList.includes(item.item.name) && (
                        <ProfileStyles.TickInterestAbsolute
                          source={require("../../assets/Icons/36px/WithDarkBackground/saveGreenBackBlack.png")}
                        />
                      )}
                    </ProfileStyles.InterestItemInnerWrapper>
                  </ProfileStyles.InterestItemWrapper>
                )}
              /></ProfileStyles.AddInterestInnerWrapper>
          </ProfileStyles.AddInterestWrapper>
        </Modal>
      </ProfileStyles.WrapperViewVertical>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    userDetail: state.user.userDetail,
    userToken: state.user.userToken,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
