import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as Actions from '../../redux/actions';
import SupportsStyles from './SupportsStyles';
import HeaderWithTwoBtn from '../../component/HeaderWithTwoBtn';
import {BLACK} from "../../helper/Color";

class Supports extends Component {
  
  render() {
    return (
      <SupportsStyles.WrapperViewVertical>
        <HeaderWithTwoBtn
          rightPress={() => this.props.navigation.navigate('Profile')}
          title={'Support'}
          rightIcon={require('../../assets/Interests/closeUp.png')}
        />
        <SupportsStyles.SafeView>
          <SupportsStyles.Scroll>
          <SupportsStyles.WhiteWrapperView>
            <SupportsStyles.Muli15Regular>
              Hey! How can we help?
            </SupportsStyles.Muli15Regular>
            <SupportsStyles.RowView activeOpacity={1}>
              <SupportsStyles.RowViewLeftIconWrapper>
                <SupportsStyles.RowLeftIcon style={{height: 18, width: 18}} source={require('../../assets/OtherIcons/supportRead.png')}/>
              </SupportsStyles.RowViewLeftIconWrapper>
              <SupportsStyles.RowRightView>
                <SupportsStyles.Muli15Regular>
                  Read Blog
                </SupportsStyles.Muli15Regular>
                <SupportsStyles.Muli15Regular style={{color: BLACK.appLight, marginTop: 2}}>
                  Get latest news and stories
                </SupportsStyles.Muli15Regular>
              </SupportsStyles.RowRightView>
            </SupportsStyles.RowView>
            
            <SupportsStyles.RowView activeOpacity={1}>
              <SupportsStyles.RowViewLeftIconWrapper>
                <SupportsStyles.RowLeftIcon style={{height: 24, width: 43}} source={require('../../assets/OtherIcons/askCommunity.png')}/>
              </SupportsStyles.RowViewLeftIconWrapper>
              <SupportsStyles.RowRightView>
                <SupportsStyles.Muli15Regular>
                  Ask Community
                </SupportsStyles.Muli15Regular>
                <SupportsStyles.Muli15Regular style={{color: BLACK.appLight, marginTop: 2}}>
                  Get help from users
                </SupportsStyles.Muli15Regular>
              </SupportsStyles.RowRightView>
            </SupportsStyles.RowView>
            
            <SupportsStyles.RowView activeOpacity={0.8} onPress={() => this.props.navigation.navigate('Tutorial')}>
              <SupportsStyles.RowViewLeftIconWrapper>
                <SupportsStyles.RowLeftIcon style={{height: 39, width: 36}} source={require('../../assets/OtherIcons/tutorials.png')}/>
              </SupportsStyles.RowViewLeftIconWrapper>
              <SupportsStyles.RowRightView>
                <SupportsStyles.Muli15Regular>
                  Tutorials
                </SupportsStyles.Muli15Regular>
                <SupportsStyles.Muli15Regular style={{color: BLACK.appLight, marginTop: 2}}>
                  Take a guided tour with us
                </SupportsStyles.Muli15Regular>
              </SupportsStyles.RowRightView>
            </SupportsStyles.RowView>
          </SupportsStyles.WhiteWrapperView>
  
  
          <SupportsStyles.WhiteWrapperView style={{marginTop: 20}}>
            <SupportsStyles.Muli15Regular>
              Chat to us
            </SupportsStyles.Muli15Regular>
            <SupportsStyles.RowView activeOpacity={1}>
              <SupportsStyles.RowViewLeftIconWrapper>
                <SupportsStyles.RowLeftIcon source={require('../../assets/OtherIcons/contact.png')}/>
              </SupportsStyles.RowViewLeftIconWrapper>
              <SupportsStyles.RowRightView>
                <SupportsStyles.Muli15Regular>
                  Hari
                </SupportsStyles.Muli15Regular>
                <SupportsStyles.Muli15Regular style={{color: BLACK.appLight, marginTop: 2}}>
                  Rate your conversation
                </SupportsStyles.Muli15Regular>
                <SupportsStyles.AbsoluteRightIcon source={require('../../assets/OtherIcons/plus.png')} />
              </SupportsStyles.RowRightView>
            </SupportsStyles.RowView>
    
            <SupportsStyles.RowView activeOpacity={1}>
              <SupportsStyles.RowViewLeftIconWrapper>
                <SupportsStyles.RowLeftIcon source={require('../../assets/OtherIcons/group.png')}/>
              </SupportsStyles.RowViewLeftIconWrapper>
              <SupportsStyles.RowRightView>
                <SupportsStyles.Muli15Regular>
                  All Chats
                </SupportsStyles.Muli15Regular>
                <SupportsStyles.Muli15Regular style={{color: BLACK.appLight, marginTop: 2}}>
                  View all your conversation
                </SupportsStyles.Muli15Regular>
                <SupportsStyles.AbsoluteRightIcon source={require('../../assets/OtherIcons/plus.png')} />
              </SupportsStyles.RowRightView>
            </SupportsStyles.RowView>
    
          </SupportsStyles.WhiteWrapperView>
  
          <SupportsStyles.WhiteWrapperView style={{marginTop: 20}}>
            <SupportsStyles.Muli15Regular>
              Follow us OR Let’s connect
            </SupportsStyles.Muli15Regular>
            <SupportsStyles.RowView activeOpacity={1}>
              <SupportsStyles.SocialWrapper>
                <SupportsStyles.SocialIcon source={require('../../assets/Social/facebook.png')} />
              </SupportsStyles.SocialWrapper>
              <SupportsStyles.SocialWrapper>
                <SupportsStyles.SocialIcon source={require('../../assets/Social/apple.png')} />
              </SupportsStyles.SocialWrapper>
              <SupportsStyles.SocialWrapper>
                <SupportsStyles.SocialIcon source={require('../../assets/Layout/linkedIn-Icon.png')} />
              </SupportsStyles.SocialWrapper>
              <SupportsStyles.SocialWrapper>
                <SupportsStyles.SocialIcon source={require('../../assets/Layout/twitter-icon.png')} />
              </SupportsStyles.SocialWrapper>
            </SupportsStyles.RowView>
          </SupportsStyles.WhiteWrapperView>
          </SupportsStyles.Scroll>
        </SupportsStyles.SafeView>
      </SupportsStyles.WrapperViewVertical>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    userDetail: state.user.userDetail,
    userToken: state.user.userToken,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Supports);
