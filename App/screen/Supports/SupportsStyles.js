/* eslint-disable prettier/prettier */
import styled from 'styled-components/native';
import {BLACK, GREEN, WHITE} from '../../helper/Color';
import {FONT, isIphoneXorAbove, SCREEN} from "../../helper/Constant";

const SupportsStyles = {
  WrapperViewVertical: styled.View`
    flex: 1;
    justifyContent: center;
    backgroundColor: ${GREEN.app};
    paddingBottom: ${isIphoneXorAbove ? 80 : 60}px ;
  `,
  SafeView: styled.SafeAreaView`
    flex: 1;
  `,
  Scroll: styled.ScrollView`
    flex: 1;
  `,
  WhiteWrapperView: styled.View`
    backgroundColor: ${WHITE.dark};
    borderRadius: 10px;
    paddingVertical: 23px;
    paddingHorizontal: 9px;
    width: ${SCREEN.width - 20}px;
    alignSelf: center;
  `,
  Muli15Regular: styled.Text`
    fontSize: 15px;
    fontFamily: ${FONT.Muli.regular};
    color: ${BLACK.app};
  `,
  RowView: styled.TouchableOpacity`
    flexDirection: row;
    alignItems: center;
    width: 100%;
    height: 60px;
    marginTop: 15px;
  `,
  RowViewLeftIconWrapper: styled.View`
    height: 100%;
    width: 60px;
    justifyContent: center;
    alignItems: center;
  `,
  RowLeftIcon: styled.Image`
    height: 36px;
    width: 30px;
    resizeMode: contain;
  `,
  RowRightView: styled.View`
    flex: 1
  `,
  SocialWrapper: styled.TouchableHighlight`
    flex: 1;
    justifyContent: center;
    alignItems: center;
  `,
  SocialIcon: styled.Image`
    height: 80%;
    width: 80%;
    borderRadius: 5px;
    resizeMode: contain;
  `,
  AbsoluteRightIcon: styled.Image`
    height: 10px;
    width: 10px;
    resizeMode: contain;
    position: absolute;
    right: 10px;
  `,
  
};

export default SupportsStyles;
