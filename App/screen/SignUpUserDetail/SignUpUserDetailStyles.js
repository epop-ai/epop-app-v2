/* eslint-disable prettier/prettier */
import styled from 'styled-components';
import {BLACK, GREEN, WHITE} from '../../helper/Color';
import {FONT, SCREEN} from '../../helper/Constant';

const SignUpUserDetailStyles = {
  WrapperViewVertical: styled.View`
    flex: 1;
    backgroundColor: ${WHITE.dark};
  `,
  SafeView: styled.SafeAreaView`
    flex: 1;
    padding-top: 9px;
  `,
  Scroll: styled.ScrollView`
    flex: 1;
  `,
  KeyboardAvoid: styled.KeyboardAvoidingView`
    flex: 1;
  `,
  Muli20Bold: styled.Text`
    font-size: 20px;
    font-family: ${FONT.Muli.bold};
    color: ${BLACK.app};
    margin-top: 19px;
    align-self: center;
  `,
  AppLogo: styled.Image`
    height: 117px;
    width: ${SCREEN.width -64}px;
    align-self: center;
    marginVertical: 15px;
    resizeMode: contain;
  `,
  BottomViewWrapper: styled.View`
    flex: 1;
    padding-top: 15px;
  `,
  LightBlackWrapper: styled.View`
    width: ${SCREEN.width - 20}px;
    height: 95%;
    align-self: center;
    border-radius: 12px;
    padding-top: 33px;
    padding-bottom: 200px;
    background-color: ${WHITE.light};
    border-width: 1px;
    border-color: ${BLACK.borderLight};
  `,
  QuickSandRegular16: styled.Text`
    font-family: ${FONT.QuickSand.regular};
    color: ${BLACK.textColorDark};
    font-size: 16px;
    align-self: center;
  `,
  PhoneInputWrapper: styled.View`
    height: 46px;
    width: ${SCREEN.width - 80}px;
    align-self: center;
    justify-content: center;
    background-color: ${WHITE.dark};
    border-radius: 6px;
    margin-top: 16px;
  `,
  InputText: styled.TextInput`
    height: 100%;
    width: 100%;
    paddingHorizontal: 17px;
    font-family: ${FONT.SourceSansPro.regular};
    background-color: ${WHITE.dark};
    border-radius: 6px;
    font-size: 16px;
  `,
  ContinueBtn: styled.TouchableHighlight`
    height: 46px;
    background-color: ${BLACK.transParent};
    width: ${SCREEN.width - 80}px;
    align-self: center;
    border-radius: 6px;
    margin-top: 34px;
    justify-content: center;
    align-items: center;
  `,
  ContinueBtnText: styled.Text`
    font-size: 16px;
    font-family: ${FONT.Muli.bold};
    color: ${WHITE.app};
  `,
  TermAndService: styled.Text`
    font-family: ${FONT.SourceSansPro.regular};
    marginTop: 21px;
    font-size: 12px;
    paddingHorizontal: 10px;
    align-self: center;
  `,
  TermAndServiceGreen: styled.Text`
    font-size: 12px;
    font-family: ${FONT.SourceSansPro.regular};
    color: ${GREEN.app};
  `,
  RowView: styled.View`
    flex-direction: row;
    height: 46px;
    width: ${SCREEN.width - 80}px;
    align-self: center;
    justify-content: center;
    margin-top: 16px;
  `,
  CityWrapper: styled.View`
    flex: 0.7;
    justify-content: center;
    align-items: center;
  `,
  ZipWrapper: styled.View`
   flex: 0.3;
    justify-content: center;
    align-items: center;
    paddingLeft: 10px;
  `,
};

export default SignUpUserDetailStyles;
