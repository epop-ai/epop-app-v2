/* eslint-disable react-native/no-inline-styles */
import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Platform } from "react-native";

import * as Actions from "../../redux/actions";
import SignUpUserDetailStyles from "./SignUpUserDetailStyles";
import { BLACK } from "../../helper/Color";

class SignUpUserDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fName: "",
      lName: "",
      email: "",
    };
  }

  render() {
    return (
      <SignUpUserDetailStyles.WrapperViewVertical>
        <SignUpUserDetailStyles.SafeView>
          <SignUpUserDetailStyles.Scroll>
            <SignUpUserDetailStyles.Muli20Bold>
              Let’s get started!
            </SignUpUserDetailStyles.Muli20Bold>
            <SignUpUserDetailStyles.AppLogo
              source={require("../../assets/Layout/logo.png")}
            />
            <SignUpUserDetailStyles.BottomViewWrapper>
              <SignUpUserDetailStyles.LightBlackWrapper>
                <SignUpUserDetailStyles.PhoneInputWrapper>
                  <SignUpUserDetailStyles.InputText
                    placeholder={"First Name"}
                    placeholderTextColor={BLACK.borderLight}
                    value={this.state.fName}
                    onChangeText={(fName) => this.setState({ fName })}
                  />
                </SignUpUserDetailStyles.PhoneInputWrapper>
                <SignUpUserDetailStyles.PhoneInputWrapper>
                  <SignUpUserDetailStyles.InputText
                    placeholder={"Last Name"}
                    placeholderTextColor={BLACK.borderLight}
                    value={this.state.lName}
                    onChangeText={(lName) => this.setState({ lName })}
                  />
                </SignUpUserDetailStyles.PhoneInputWrapper>
                <SignUpUserDetailStyles.PhoneInputWrapper>
                  <SignUpUserDetailStyles.InputText
                    placeholder={"Email address"}
                    placeholderTextColor={BLACK.borderLight}
                    value={this.state.lName}
                    keyboardType={"email-address"}
                    onChangeText={(lName) => this.setState({ lName })}
                  />
                </SignUpUserDetailStyles.PhoneInputWrapper>
                <SignUpUserDetailStyles.RowView>
                  <SignUpUserDetailStyles.CityWrapper>
                    <SignUpUserDetailStyles.InputText placeholder={"City"} />
                  </SignUpUserDetailStyles.CityWrapper>
                  <SignUpUserDetailStyles.ZipWrapper>
                    <SignUpUserDetailStyles.InputText placeholder={"Zip"} />
                  </SignUpUserDetailStyles.ZipWrapper>
                </SignUpUserDetailStyles.RowView>
                <SignUpUserDetailStyles.ContinueBtn
                  onPress={() =>
                    this.props.navigation.navigate("SignUpPasswordSet")
                  }
                  underlayColor={BLACK.appLight}
                >
                  <SignUpUserDetailStyles.ContinueBtnText>
                    Continue
                  </SignUpUserDetailStyles.ContinueBtnText>
                </SignUpUserDetailStyles.ContinueBtn>
                <SignUpUserDetailStyles.TermAndService>
                  By continuing, you agree to our{" "}
                  <SignUpUserDetailStyles.TermAndServiceGreen>
                    {" "}
                    Terms of Service{" "}
                  </SignUpUserDetailStyles.TermAndServiceGreen>{" "}
                  and{" "}
                  <SignUpUserDetailStyles.TermAndServiceGreen>
                    {" "}
                    Privacy Policy{" "}
                  </SignUpUserDetailStyles.TermAndServiceGreen>
                </SignUpUserDetailStyles.TermAndService>

                <SignUpUserDetailStyles.TermAndService>
                  Already have an account?{" "}
                  <SignUpUserDetailStyles.TermAndServiceGreen
                    onPress={() => this.props.navigation.navigate("SignIn")}
                  >
                    {" "}
                    Sign In Now{" "}
                  </SignUpUserDetailStyles.TermAndServiceGreen>
                </SignUpUserDetailStyles.TermAndService>
              </SignUpUserDetailStyles.LightBlackWrapper>
            </SignUpUserDetailStyles.BottomViewWrapper>
          </SignUpUserDetailStyles.Scroll>
        </SignUpUserDetailStyles.SafeView>
      </SignUpUserDetailStyles.WrapperViewVertical>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    userDetail: state.user.userDetail,
    userToken: state.user.userToken,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUpUserDetail);
