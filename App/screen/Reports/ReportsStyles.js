/* eslint-disable prettier/prettier */
import styled from 'styled-components/native';
import {BLACK, GREEN, WHITE} from '../../helper/Color';
import {FONT, isIphoneXorAbove, SCREEN} from "../../helper/Constant";

const ReportsStyles = {
  WrapperViewVertical: styled.View`
    flex: 1;
    backgroundColor: ${GREEN.app};
  `,
  SafeView: styled.SafeAreaView`
    flex: 1;
  `,
  RowView: styled.View`
    height: 35px;
    width: ${SCREEN.width - 20}px;
    align-self: center;
    flexDirection: row;
  `,
  WrapperChartView: styled.View`
    height: ${SCREEN.width - 80}px;
    width: 100%;
    justify-content: center;
    align-items: center;
  `,
  ChartAbsoluteInnerWrapper: styled.View`
    position: absolute;
    justify-content: center;
    align-items: center;
  `,
  InnerTabView: styled.View`
    flex: 0.7;
    borderRadius: 20px;
    background-color: ${BLACK.app};
    flexDirection: row;
  `,
  TabWrapper: styled.TouchableHighlight`
    flex: 1;
    justifyContent: center;
    alignItems: center;
    borderRadius: 20px;
    borderWidth: 2px;
    borderColor: ${BLACK.app};
  `,
  TabText: styled.Text`
    font-size: 12px;
    fontFamily: ${FONT.Muli.semiBold};
    color: ${WHITE.dark};
  `,
  RoundLeftAndRightView: styled.View`
    flex: 0.15;
    justify-content: center;
    align-items: center;
  `,
  IconLeftAndRight: styled.TouchableHighlight`
    height: 100%;
    width: 100%;
    justify-content: center;
    align-items: center;
  `,
  InnerIcon: styled.Image`
    height: 100%;
    width: 100%;
    resizeMode: contain;
  `,
  MainWrapperViewTab: styled.View`
    background-color: ${GREEN.app};
    paddingBottom: ${isIphoneXorAbove ?   60 : 40}px;
    flex: 1;
  `,
  Scroll: styled.ScrollView`
    flex: 1;
  `,
  InnerWhiteWrapper: styled.View`
    width: ${SCREEN.width - 20}px;
    align-self: center;
    backgroundColor: ${WHITE.app};
    borderRadius: 10px;
    marginVertical: 10px;
    paddingVertical: 5px;
  `,
  Muli16Regular: styled.Text`
    fontSize: 16px;
    fontFamily: ${FONT.Muli.regular};
    align-self: center;
  `,
  FlatList: styled.FlatList`
    align-self: center;
  `,
  DateText: styled.Text`
    fontSize: 12px;
    fontFamily: ${FONT.Muli.bold};
    color: ${BLACK.app};
  `,
  TotalText: styled.Text`
    fontSize: 20px;
    fontFamily: ${FONT.QuickSand.light};
    marginVertical: 5px;
  `,
  
};

export default ReportsStyles;
