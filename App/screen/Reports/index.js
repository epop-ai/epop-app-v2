import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {VictoryPie} from 'victory-native';
import {Platform, PixelRatio} from 'react-native';

import * as Actions from '../../redux/actions';
import ReportsStyles from './ReportsStyles';
import HeaderWithTwoBtn from "../../component/HeaderWithTwoBtn";
import {BLACK, GREEN, WHITE} from "../../helper/Color";
import {FONT, ReceiptList, SCREEN} from "../../helper/Constant";
import CalendarView from "../../component/CalendarView";
import ReportItem from "../../component/ReportItem";
import SearchModel from "../../component/SearchModel";
import SetCalendarPopUp from "../../component/SetCalendarPopUp";
import TimePeriodSelection from "../../component/TimePeriodSelection";

const graphicData = [
  {y: 10, x: '5%'},
  {y: 90, x: '90%'},
  {y: 50, x: '50%'},
  {y: 20, x: '20%'},
  {y: 70, x: '70%'},
]

const scale = SCREEN.width / 320;

class Reports extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTab: 0,
      itemSelected: {},
      detailItemModel: false,
      openCalendarModal: false,
      openSearchModal: false,
      editCalenderView: false,
      openTimePeriod: false,
      valueOfPeriod: '',
    }
  }
  
   normalize(size) {
    const newSize = size * scale
    if (Platform.OS === 'ios') {
      return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
    } else {
      return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
    }
  }
  
  _renderMerchants() {
    return (
      <ReportsStyles.MainWrapperViewTab>
        <ReportsStyles.FlatList
          keyExtractor={(item, index) => index.toString()}
          data={ReceiptList[0].data}
          ListHeaderComponent={
            <ReportsStyles.InnerWhiteWrapper>
              <ReportsStyles.RowView>
                <ReportsStyles.RoundLeftAndRightView>
                  <ReportsStyles.IconLeftAndRight
                    underlayColor={WHITE.dark}
                    onPress={() => this.setState({selectedTab: 2})}>
                    <ReportsStyles.InnerIcon
                      source={require('../../assets/Icons/32px/NoBackground/backBlack.png')}/>
                  </ReportsStyles.IconLeftAndRight>
                </ReportsStyles.RoundLeftAndRightView>
                <ReportsStyles.InnerTabView style={{backgroundColor: WHITE.app, justifyContent: 'center'}}>
                  <ReportsStyles.Muli16Regular>
                    My Spend by Merchant
                  </ReportsStyles.Muli16Regular>
                </ReportsStyles.InnerTabView>
                <ReportsStyles.RoundLeftAndRightView>
                  <ReportsStyles.IconLeftAndRight
                    underlayColor={WHITE.dark}
                    onPress={() => this.setState({selectedTab: 1})}>
                    <ReportsStyles.InnerIcon style={{
                      transform: [
                        {scaleX: -1}
                      ]
                    }} source={require('../../assets/Icons/32px/NoBackground/backBlack.png')}/>
                  </ReportsStyles.IconLeftAndRight>
                </ReportsStyles.RoundLeftAndRightView>
              </ReportsStyles.RowView>
              <ReportsStyles.WrapperChartView>
                <VictoryPie
                  data={graphicData}
                  colorScale={['red', 'blue', 'yellow', 'green', 'tomato']}
                  width={300}
                  height={300}
                  innerRadius={90}
                  padAngle={1} //defines the padding between adjacent data slices
                  style={{
                    labels: {
                      fill: BLACK.appLight, fontSize: 12, padding: 7
                    },
                  }}
                />
                <ReportsStyles.ChartAbsoluteInnerWrapper>
                  <ReportsStyles.DateText onPress={() => this.setState({openTimePeriod: true})}>
                    jan 2 - jan 8
                  </ReportsStyles.DateText>
                  <ReportsStyles.TotalText style={{color: BLACK.appLight}}>
                    Total
                  </ReportsStyles.TotalText>
                  <ReportsStyles.Muli16Regular style={{fontFamily: FONT.Muli.bold, fontSize: 20,}}>
                    $1750.03
                  </ReportsStyles.Muli16Regular>
                </ReportsStyles.ChartAbsoluteInnerWrapper>
              </ReportsStyles.WrapperChartView>
            </ReportsStyles.InnerWhiteWrapper>
          }
          renderItem={(item, index) => <ReportItem
            item={item.item}
            index={item.index}
            itemClick={() =>
              this.setState({itemSelected: item, detailItemModel: true})
            }
          />}
        />
      </ReportsStyles.MainWrapperViewTab>
    )
  }
  
  _renderCategories() {
    return (
      <ReportsStyles.MainWrapperViewTab>
        <ReportsStyles.FlatList
          keyExtractor={
            (
              item
              ,
              index
            ) =>
              index
                .toString()
          }
          
          data=
            {ReceiptList[1].data}
          ListHeaderComponent=
            {
              <ReportsStyles.InnerWhiteWrapper>
                <ReportsStyles.RowView>
                  <ReportsStyles.RoundLeftAndRightView>
                    <ReportsStyles.IconLeftAndRight
                      underlayColor={WHITE.dark}
                      onPress={() => this.setState({selectedTab: 0})}>
                      <ReportsStyles.InnerIcon
                        source={require('../../assets/Icons/32px/NoBackground/backBlack.png')}/>
                    </ReportsStyles.IconLeftAndRight>
                  </ReportsStyles.RoundLeftAndRightView>
                  <ReportsStyles.InnerTabView style={{backgroundColor: WHITE.app, justifyContent: 'center'}}>
                    <ReportsStyles.Muli16Regular>
                      My Spend by Categories
                    </ReportsStyles.Muli16Regular>
                  </ReportsStyles.InnerTabView>
                  <ReportsStyles.RoundLeftAndRightView>
                    <ReportsStyles.IconLeftAndRight
                      underlayColor={WHITE.dark}
                      onPress={() => this.setState({selectedTab: 2})}>
                      <ReportsStyles.InnerIcon style={{
                        transform: [
                          {scaleX: -1}
                        ]
                      }} source={require('../../assets/Icons/32px/NoBackground/backBlack.png')}/>
                    </ReportsStyles.IconLeftAndRight>
                  </ReportsStyles.RoundLeftAndRightView>
                </ReportsStyles.RowView>
                
                <ReportsStyles.WrapperChartView>
                  <VictoryPie
                    data={graphicData}
                    colorScale={['red', 'blue', 'yellow', 'green', 'tomato']}
                    width={300}
                    height={300}
                    innerRadius={90}
                    padAngle={1} //defines the padding between adjacent data slices
                    style={{
                      labels: {
                        fill: BLACK.appLight, fontSize: 12, padding: 7
                      },
                    }}
                  />
                  <ReportsStyles.ChartAbsoluteInnerWrapper>
                    <ReportsStyles.DateText onPress={() => this.setState({openTimePeriod: true})}>
                      jan 2 - jan 8
                    </ReportsStyles.DateText>
                    <ReportsStyles.TotalText style={{color: BLACK.appLight}}>
                      Total
                    </ReportsStyles.TotalText>
                    <ReportsStyles.Muli16Regular style={{fontFamily: FONT.Muli.bold, fontSize: 20,}}>
                      $1750.03
                    </ReportsStyles.Muli16Regular>
                  </ReportsStyles.ChartAbsoluteInnerWrapper>
                </ReportsStyles.WrapperChartView>
              
              </ReportsStyles.InnerWhiteWrapper>
            }
          renderItem=
            {
              (item, index) => <ReportItem
                item={item.item}
                index={item.index}
                itemClick={() =>
                  this.setState({itemSelected: item, detailItemModel: true})
                }
              />
            }
        />
      </ReportsStyles.MainWrapperViewTab>
    )
  }
  
  _renderTags() {
    return (
      <ReportsStyles.MainWrapperViewTab>
        <ReportsStyles.FlatList
          keyExtractor={(item, index) => index.toString()}
          data={ReceiptList[2].data}
          ListHeaderComponent={
            <ReportsStyles.InnerWhiteWrapper>
              <ReportsStyles.RowView>
                <ReportsStyles.RoundLeftAndRightView>
                  <ReportsStyles.IconLeftAndRight
                    underlayColor={WHITE.dark}
                    onPress={() => this.setState({selectedTab: 1})}>
                    <ReportsStyles.InnerIcon
                      source={require('../../assets/Icons/32px/NoBackground/backBlack.png')}/>
                  </ReportsStyles.IconLeftAndRight>
                </ReportsStyles.RoundLeftAndRightView>
                <ReportsStyles.InnerTabView style={{backgroundColor: WHITE.app, justifyContent: 'center'}}>
                  <ReportsStyles.Muli16Regular>
                    My Spend by Tags
                  </ReportsStyles.Muli16Regular>
                </ReportsStyles.InnerTabView>
                <ReportsStyles.RoundLeftAndRightView>
                  <ReportsStyles.IconLeftAndRight
                    underlayColor={WHITE.dark}
                    onPress={() => this.setState({selectedTab: 0})}>
                    <ReportsStyles.InnerIcon style={{
                      transform: [
                        {scaleX: -1}
                      ]
                    }} source={require('../../assets/Icons/32px/NoBackground/backBlack.png')}/>
                  </ReportsStyles.IconLeftAndRight>
                </ReportsStyles.RoundLeftAndRightView>
              </ReportsStyles.RowView>
              
              <ReportsStyles.WrapperChartView>
                <VictoryPie
                  data={graphicData}
                  colorScale={['red', 'blue', 'yellow', 'green', 'tomato']}
                  width={300}
                  height={300}
                  innerRadius={90}
                  padAngle={1} //defines the padding between adjacent data slices
                  style={{
                    labels: {
                      fill: BLACK.appLight, fontSize: 12, padding: 7
                    },
                  }}
                />
                <ReportsStyles.ChartAbsoluteInnerWrapper>
                  <ReportsStyles.DateText onPress={() => this.setState({openTimePeriod: true})}>
                    jan 2 - jan 8
                  </ReportsStyles.DateText>
                  <ReportsStyles.TotalText style={{color: BLACK.appLight}}>
                    Total
                  </ReportsStyles.TotalText>
                  <ReportsStyles.Muli16Regular style={{fontFamily: FONT.Muli.bold, fontSize: 20,}}>
                    $1750.03
                  </ReportsStyles.Muli16Regular>
                </ReportsStyles.ChartAbsoluteInnerWrapper>
              </ReportsStyles.WrapperChartView>
            
            </ReportsStyles.InnerWhiteWrapper>
          }
          renderItem={(item, index) => <ReportItem
            item={item.item}
            index={item.index}
            itemClick={() =>
              this.setState({itemSelected: item, detailItemModel: true})
            }
          />}
        />
      </ReportsStyles.MainWrapperViewTab>
    )
  }
  
  
  render() {
    const {selectedTab} = this.state;
    return (
      <ReportsStyles.WrapperViewVertical>
        <HeaderWithTwoBtn
          title={"My Report"}
          rightIcon={require("../../assets/Interests/cheerfulDog.png")}
          rightPress={() => this.props.navigation.navigate('Profile')}
          leftIcon={require('../../assets/Icons/36px/WithDarkBackground/searchBackBlack.png')}
          leftPress={() => this.setState({openSearchModal: true})}
        />
        <ReportsStyles.SafeView>
          <ReportsStyles.RowView>
            <ReportsStyles.RoundLeftAndRightView>
              <ReportsStyles.IconLeftAndRight
                underlayColor={GREEN.app}
                onPress={() => this.setState({openCalendarModal: true})}>
                <ReportsStyles.InnerIcon
                  source={require('../../assets/Icons/36px/NoBackground/calendarIconWhite.png')}/>
              </ReportsStyles.IconLeftAndRight>
            </ReportsStyles.RoundLeftAndRightView>
            <ReportsStyles.InnerTabView>
              <ReportsStyles.TabWrapper
                onPress={() => this.setState({selectedTab: 0})}
                style={{backgroundColor: selectedTab === 0 ? GREEN.btn : BLACK.app}}>
                <ReportsStyles.TabText style={{fontSize: this.normalize(11) }}>
                  MERCHANTS
                </ReportsStyles.TabText>
              </ReportsStyles.TabWrapper>
              <ReportsStyles.TabWrapper
                onPress={() => this.setState({selectedTab: 1})}
                style={{backgroundColor: selectedTab === 1 ? GREEN.btn : BLACK.app}}>
                <ReportsStyles.TabText style={{fontSize: this.normalize(11) }}>
                  CATEGORIES
                </ReportsStyles.TabText>
              </ReportsStyles.TabWrapper>
              <ReportsStyles.TabWrapper
                onPress={() => this.setState({selectedTab: 2})}
                style={{backgroundColor: selectedTab === 2 ? GREEN.btn : BLACK.app}}>
                <ReportsStyles.TabText style={{fontSize: this.normalize(11) }}>
                  TAGS
                </ReportsStyles.TabText>
              </ReportsStyles.TabWrapper>
            </ReportsStyles.InnerTabView>
            <ReportsStyles.RoundLeftAndRightView>
              <ReportsStyles.IconLeftAndRight>
                <ReportsStyles.InnerIcon source={require('../../assets/Icons/36px/NoBackground/shareIconWhite.png')}/>
              </ReportsStyles.IconLeftAndRight>
            </ReportsStyles.RoundLeftAndRightView>
          </ReportsStyles.RowView>
          {selectedTab === 0 ? this._renderMerchants() : selectedTab === 1 ? this._renderCategories() : this._renderTags()}
        </ReportsStyles.SafeView>
        {this.state.openCalendarModal && <CalendarView
          pressClose={() => this.setState({openCalendarModal: false})}
          openEditCalender={() => this.setState({editCalenderView: true})}
          pressSave={() => this.setState({openCalendarModal: false})}/>}
        
        {this.state.openSearchModal && <SearchModel
          pressClose={() => this.setState({openSearchModal: false})}/>}
        
        {this.state.editCalenderView &&
        <SetCalendarPopUp cancelPress={() => this.setState({editCalenderView: false})}/>}
        
        {this.state.openTimePeriod &&
        <TimePeriodSelection
          pressClose={() => this.setState({openTimePeriod: false})}
          pressSave={(item) => this.setState({openTimePeriod: false, valueOfPeriod: item})}/>}
      </ReportsStyles.WrapperViewVertical>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    userDetail: state.user.userDetail,
    userToken: state.user.userToken,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Reports);
