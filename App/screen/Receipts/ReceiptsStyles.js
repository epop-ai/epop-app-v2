/* eslint-disable prettier/prettier */
import styled from 'styled-components/native';
import {BLACK, GREEN, WHITE} from '../../helper/Color';
import { FONT, isIphoneXorAbove, SCREEN } from '../../helper/Constant';

const ReceiptsStyles = {
  WrapperViewVertical: styled.View`
    height: ${isIphoneXorAbove ?  SCREEN.height - 80 : SCREEN.height - 60}px;
    width: ${SCREEN.width}px;
    backgroundColor: ${GREEN.app};
  `,
  SafeView: styled.SafeAreaView`
    flex: 1;
    backgroundColor: ${WHITE.dark};
  `,
  TopWrapper: styled.View`
    backgroundColor: ${GREEN.app};
    paddingBottom: 5px;
  `,
  AddImage: styled.Image`
    height: 50px;
    width: 100%;
    resizeMode: contain;
    alignSelf: center;
  `,
  TopInnerWrapper: styled.View`
    marginTop: 10px;
    flexDirection: row;
    alignItems: center;
    width: ${SCREEN.width}px;
    paddingLeft: 15px;
  `,
  InnerView: styled.View`
    flexDirection: row;
    flex: 1;
    alignItems: center;
  `,
  CardScroll: styled.ScrollView`
    flex: 1;
  `,
  CalenderBtn: styled.TouchableHighlight`
    height: 70px;
    width: 70px;
  `,
  CalenderIcon: styled.Image`
    height: 70px;
    width: 70px;
    resizeMode: contain;
  `,
  CardView: styled.TouchableHighlight`
    height: 98px;
    width: 70px;
    marginRight: 5px;
    borderRadius: 8px;
    shadowColor: ${BLACK.transParent};
    shadowOpacity: 0.3;
    shadowRadius: 5px;
    margin-left: ${SCREEN.width / 2.4}px;
  `,
  CardInnerWrapper: styled.View`
    height: 98px;
    width: 70px;
    borderRadius: 8px;
    padding: 11px;
    justifyContent: flex-end;
    alignItems: flex-end;
  `,
  CardIcon: styled.Image`
    height: 40px;
    width: 40px;
    resizeMode: contain;
  `,
  CardText: styled.Text`
    fontSize: 10px;
    fontFamily: ${FONT.Muli.regular};
    color: ${WHITE.dark};
  `,
  AbsoluteTickIcon: styled.Image`
    height: 27px;
    width: 27px;
    resizeMode: contain;
    position: absolute;
    top: 5px;
    alignSelf: center;
    borderRadius: 14px;
    backgroundColor: ${WHITE.dark};
  `,
  ReceiptList: styled.SectionList`
  `,
  ListHeaderWrapper: styled.View`
    paddingVertical: 5px;
    paddingLeft: 23px;
  `,
  ListHeaderText: styled.Text`
    fontSize: 16px;
    fontFamily: ${FONT.Muli.regular};
  `,
  ReceiptDetailWrapper: styled.View`
    flex: 1;
  `,
  ReceiptDetailWrapper1: styled.View`
    height: ${isIphoneXorAbove ?  SCREEN.height - 80 : SCREEN.height - 60}px;
    width: ${SCREEN.width}px;
    backgroundColor: ${GREEN.app};
    alignItems: center;
  `,
};

export default ReceiptsStyles;
