/* eslint-disable no-alert */
import React, {Component} from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {Modal} from "react-native";
import {NavigationEvents} from "react-navigation";
import Carousel from 'react-native-snap-carousel';

import * as Actions from "../../redux/actions";
import ReceiptsStyles from "./ReceiptsStyles";
import HeaderWithTwoBtn from "../../component/HeaderWithTwoBtn";
import {BLACK, BLUE, GOLD, GREEN} from "../../helper/Color";
import {ReceiptList, SCREEN} from "../../helper/Constant";
import ReportListItem from "../../component/ReportListItem";
import ReceiptDetail from "../../component/ReceiptDetail";
import CalendarView from "../../component/CalendarView";
import SearchModel from "../../component/SearchModel";
import SetCalendarPopUp from "../../component/SetCalendarPopUp";

class Receipts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      detailItemModel: false,
      itemSelected: '',
      tagAddModel: false,
      selectedBankCard: 0,
      openCalendarModal: false,
      openSearchModal: false,
      selectedItemIndex: 0,
      editCalenderView: false,
      tagListSelected: [],
      defaultTags: ['Warranty', 'Second Home', 'Tax', 'HSA/FSA', 'Work Expense'],
      carousalScrollEnabled: true
    };
    this.renderItemPage=this.renderItemPage.bind(this)
  }
  
  async getAsyncData() {
    // this.props.navigation.navigate('Auth');
  }
  
  renderItemPage({item, index}) {
    return (
      <ReceiptDetail
        tagListUpdate={(tags) => this.setState({tagListSelected: tags})}
        extraData={this.state.tagListSelected}
        item={item}
        scrollEnableCall={(value) => this.setState({carousalScrollEnabled: value}) }
        defaultTags={this.state.defaultTags}
        updateDefaultTags={(tags) => this.setState({defaultTags: tags})}
        tagEditPress={() => this.setState({tagAddModel: true})}
        cancelPress={() =>
          this.setState({detailItemModel: false, itemSelected: {}})
        }
      />
    );
  }
  
  render() {
    return (
      <ReceiptsStyles.WrapperViewVertical>
        <NavigationEvents
          onDidFocus={() => {
            setTimeout(() => {
              this.getAsyncData();
            }, 700);
          }}
        />
        <HeaderWithTwoBtn
          leftIcon={require("../../assets/Icons/36px/WithDarkBackground/searchBackBlack.png")}
          leftPress={() => this.setState({openSearchModal: true})}
          title={"My Receipts"}
          rightIcon={require("../../assets/Interests/cheerfulDog.png")}
          rightPress={() => this.props.navigation.navigate('Profile')}
        />
        <ReceiptsStyles.SafeView>
          <ReceiptsStyles.TopWrapper>
            <ReceiptsStyles.AddImage
              source={require("../../assets/Ads/appleMusicBanner.png")}
            />
            <ReceiptsStyles.TopInnerWrapper>
              <ReceiptsStyles.CalenderBtn
                underlayColor={GREEN.app}
                onPress={() => this.setState({openCalendarModal: true})}>
                <ReceiptsStyles.CalenderIcon
                  source={require("../../assets/Icons/32px/NoBackground/calender.png")}
                />
              </ReceiptsStyles.CalenderBtn>
              <ReceiptsStyles.InnerView>
                <ReceiptsStyles.CardScroll
                  horizontal
                  showsHorizontalScrollIndicator={false}
                >
                  <ReceiptsStyles.CardView
                    style={{backgroundColor: GOLD.card}}
                    underlayColor={GOLD.card}
                    onPress={() => this.setState({selectedBankCard: 0})}
                  >
                    <ReceiptsStyles.CardInnerWrapper>
                      <ReceiptsStyles.CardIcon
                        source={require("../../assets/CardIssuers/family.png")}
                      />
                      {this.state.selectedBankCard === 0 && (
                        <ReceiptsStyles.AbsoluteTickIcon
                          source={require("../../assets/Icons/36px/NoBackground/saveIconGreen.png")}
                        />
                      )}
                    </ReceiptsStyles.CardInnerWrapper>
                  </ReceiptsStyles.CardView>
                  <ReceiptsStyles.CardView
                    style={{backgroundColor: BLACK.dark, marginLeft: 5,}}
                    underlayColor={BLACK.dark}
                    onPress={() => this.setState({selectedBankCard: 1})}
                  >
                    <ReceiptsStyles.CardInnerWrapper>
                      <ReceiptsStyles.CardIcon
                        source={require("../../assets/CardIssuers/mcLogo.png")}
                      />
                      <ReceiptsStyles.CardText>***4287</ReceiptsStyles.CardText>
                      {this.state.selectedBankCard === 1 && (
                        <ReceiptsStyles.AbsoluteTickIcon
                          source={require("../../assets/Icons/36px/NoBackground/saveIconGreen.png")}
                        />
                      )}
                    </ReceiptsStyles.CardInnerWrapper>
                  </ReceiptsStyles.CardView>
                  <ReceiptsStyles.CardView
                    style={{backgroundColor: BLACK.appLight, marginLeft: 5,}}
                    underlayColor={BLACK.appLight}
                    onPress={() => this.setState({selectedBankCard: 2})}
                  >
                    <ReceiptsStyles.CardInnerWrapper>
                      <ReceiptsStyles.CardIcon
                        source={require("../../assets/CardIssuers/visaLogo.png")}
                      />
                      <ReceiptsStyles.CardText>***4287</ReceiptsStyles.CardText>
                      {this.state.selectedBankCard === 2 && (
                        <ReceiptsStyles.AbsoluteTickIcon
                          source={require("../../assets/Icons/36px/NoBackground/saveIconGreen.png")}
                        />
                      )}
                    </ReceiptsStyles.CardInnerWrapper>
                  </ReceiptsStyles.CardView>
                  <ReceiptsStyles.CardView
                    style={{backgroundColor: BLUE.facebook, marginLeft: 5,}}
                    underlayColor={BLUE.app}
                    onPress={() => this.setState({selectedBankCard: 3})}
                  >
                    <ReceiptsStyles.CardInnerWrapper>
                      <ReceiptsStyles.CardIcon
                        source={require("../../assets/CardIssuers/amex.png")}
                      />
                      <ReceiptsStyles.CardText>***4287</ReceiptsStyles.CardText>
                      {this.state.selectedBankCard === 3 && (
                        <ReceiptsStyles.AbsoluteTickIcon
                          source={require("../../assets/Icons/36px/NoBackground/saveIconGreen.png")}
                        />
                      )}
                    </ReceiptsStyles.CardInnerWrapper>
                  </ReceiptsStyles.CardView>
                  <ReceiptsStyles.CardView
                    style={{backgroundColor: BLACK.app, marginLeft: 5,}}
                    underlayColor={BLACK.dark}
                    onPress={() => this.setState({selectedBankCard: 4})}
                  >
                    <ReceiptsStyles.CardInnerWrapper>
                      <ReceiptsStyles.CardIcon
                        source={require("../../assets/CardIssuers/unionPay.png")}
                      />
                      <ReceiptsStyles.CardText>***4287</ReceiptsStyles.CardText>
                      {this.state.selectedBankCard === 4 && (
                        <ReceiptsStyles.AbsoluteTickIcon
                          source={require("../../assets/Icons/36px/NoBackground/saveIconGreen.png")}
                        />
                      )}
                    </ReceiptsStyles.CardInnerWrapper>
                  </ReceiptsStyles.CardView>
                </ReceiptsStyles.CardScroll>
              </ReceiptsStyles.InnerView>
            </ReceiptsStyles.TopInnerWrapper>
          </ReceiptsStyles.TopWrapper>
          <ReceiptsStyles.ReceiptList
            stickySectionHeadersEnabled={false}
            sections={ReceiptList}
            keyExtractor={(item, index) => item + index}
            renderItem={({item, index}) => (
              <ReportListItem
                item={item}
                itemClick={() =>
                  this.setState({itemSelected: ReceiptList[item.type].data, selectedItemIndex: index ,detailItemModel: true})
                }
              />
            )}
            renderSectionHeader={({section: {title}}) => (
              <ReceiptsStyles.ListHeaderWrapper>
                <ReceiptsStyles.ListHeaderText>
                  {title}
                </ReceiptsStyles.ListHeaderText>
              </ReceiptsStyles.ListHeaderWrapper>
            )}
          />
        </ReceiptsStyles.SafeView>
        <Modal
          visible={this.state.detailItemModel}
          transparent={true}
          backdropColor={GREEN.app}
          backdropOpacity={0.7}
          animationInTiming={500}
          onShow={() => this._carousel.snapToItem(this.state.selectedItemIndex, true, true)}
          style={{backgroundColor: GREEN.app}}
        >
          <ReceiptsStyles.ReceiptDetailWrapper>
            <ReceiptsStyles.ReceiptDetailWrapper1>
            <HeaderWithTwoBtn
              leftIcon={require('../../assets/Icons/36px/WithDarkBackground/backBoxBlack.png')}
              leftPress={() =>
                this.setState({detailItemModel: false, itemSelected: {}})}
              title={'Receipt Details'}
              rightIcon={require('../../assets/Icons/36px/WithDarkBackground/shareIconBackBlack.png')}
              rightPress={() =>  alert('RightPress')}
            />
              {this.state.itemSelected !== '' &&<Carousel
                scrollEnabled={this.state.carousalScrollEnabled}
                ref={(c) => { this._carousel = c; }}
                data={this.state.itemSelected}
                renderItem={this.renderItemPage}
                sliderWidth={SCREEN.width - 30}
                itemWidth={SCREEN.width - 30}
              />}
            </ReceiptsStyles.ReceiptDetailWrapper1>
          </ReceiptsStyles.ReceiptDetailWrapper>
        </Modal>
        {this.state.openCalendarModal && <CalendarView
          pressClose={() => this.setState({openCalendarModal: false})}
          openEditCalender={() => this.setState({editCalenderView: true})}
          pressSave={() => this.setState({openCalendarModal: false})}/>}
        {this.state.openSearchModal && <SearchModel
          pressClose={() => this.setState({openSearchModal: false})}/>}
  
        {this.state.editCalenderView && <SetCalendarPopUp
          cancelPress={() => this.setState({editCalenderView: false})}
          openStartDate={this.state.openStartDate}
          openEndDate={this.state.openEndDate}
          openStartDatePress={(value) => this.setState({openStartDate: value})}
          openEndDatePress={(value) => this.setState({openEndDate: value })}
        />}
      </ReceiptsStyles.WrapperViewVertical>
    );
  }
}

function mapStateToProps(state) {
  return {
    userDetail: state.user.userDetail,
    userToken: state.user.userToken,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Receipts);
