import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {NavigationEvents} from 'react-navigation';

import * as Actions from '../../redux/actions';
import CardsStyles from './CardsStyles';
import HeaderWithTwoBtn from "../../component/HeaderWithTwoBtn";
import {BLACK, WHITE} from "../../helper/Color";

class Cards extends Component {
  async getAsyncData() {
    // this.props.navigation.navigate('Auth');
  }

  render() {
    return (
      <CardsStyles.WrapperViewVertical>
        <NavigationEvents
          onDidFocus={(payload) => {
            setTimeout(() => {
              this.getAsyncData();
            }, 700);
          }}
        />
  
        <HeaderWithTwoBtn
          title={"My Cards"}
          rightIcon={require("../../assets/Interests/cheerfulDog.png")}
          rightPress={() => alert("RightPress")}
          rightPress={() => this.props.navigation.navigate('Profile')}
        />
        <CardsStyles.SafeView>
          <CardsStyles.MainWrapper>
           <CardsStyles.CardList data={[
             {name: 'John Doe', date: '03/22', cardBack: require('../../assets/Cards/grey.png'), cardLogo: require('../../assets/CardIssuers/visaLogo.png'), lastDig: 6998},
             {name: 'Elsa Doe', date: '05/23', cardBack: require('../../assets/Cards/blue.png'), cardLogo: require('../../assets/CardIssuers/amex.png'), lastDig: 4287},
             {name: 'Lord Ash', date: '03/22', cardBack: require('../../assets/Cards/grey.png'), cardLogo: require('../../assets/CardIssuers/visaLogo.png'), lastDig: 4578},
             {name: 'John Mess', date: '03/22', cardBack: require('../../assets/Cards/green.png'), cardLogo: require('../../assets/CardIssuers/visaLogo.png'), lastDig: 7654},
             {name: 'Bee Doe', date: '03/22', cardBack: require('../../assets/Cards/red.png'), cardLogo: require('../../assets/CardIssuers/family.png'), lastDig: 4242},
           ]} keyExtractor={(item, index) => index.toString()} renderItem={(item, index) => (
             <CardsStyles.CardItemWrapper
               underlayColor={WHITE.dark}
               onPress={() => this.props.navigation.navigate('CardAdd', {item: item.item})}>
               <CardsStyles.CardItemInnerWrapper>
               <CardsStyles.CardItemImage source={item.item.cardBack} />
               <CardsStyles.CardIssuerTopText>
                 Card Issuer
               </CardsStyles.CardIssuerTopText>
                 <CardsStyles.CardTypeIcon source={item.item.cardLogo} />
                 <CardsStyles.CardInnerBottomView>
                     <CardsStyles.CardNumberText>
                         ****   ****   ****   {item.item.lastDig}
                     </CardsStyles.CardNumberText>
                     <CardsStyles.RowView>
                         <CardsStyles.Flex1View>
                            <CardsStyles.Muli8BoldText>
                                CARDHOLDER NAME
                            </CardsStyles.Muli8BoldText>
                         </CardsStyles.Flex1View>
                         <CardsStyles.Flex1View>
                             <CardsStyles.Muli8BoldText>
                                 EXPIRE DATE
                             </CardsStyles.Muli8BoldText>
                         </CardsStyles.Flex1View>
                     </CardsStyles.RowView>
                     <CardsStyles.RowView style={{marginBottom: 10}}>
                         <CardsStyles.Flex1View>
                             <CardsStyles.Muli17Regular>
                                 {item.item.name}
                             </CardsStyles.Muli17Regular>
                         </CardsStyles.Flex1View>
                         <CardsStyles.Flex1View>
                             <CardsStyles.Muli17Regular>
                                 {item.item.date}
                             </CardsStyles.Muli17Regular>
                         </CardsStyles.Flex1View>
                     </CardsStyles.RowView>
                 </CardsStyles.CardInnerBottomView>
               </CardsStyles.CardItemInnerWrapper>
             </CardsStyles.CardItemWrapper>
           )} />
          </CardsStyles.MainWrapper>
        </CardsStyles.SafeView>
          <CardsStyles.AbsoluteBottomAddBtn underlayColor={BLACK.transParent} onPress={() => this.props.navigation.navigate('CardAdd')}>
              <CardsStyles.AbsoluteBtnImage source={require('../../assets/Icons/46px/WithDarkBackground/iconPlusBlack.png')}/>
          </CardsStyles.AbsoluteBottomAddBtn>
      </CardsStyles.WrapperViewVertical>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    userDetail: state.user.userDetail,
    userToken: state.user.userToken,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Cards);
