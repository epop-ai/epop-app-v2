/* eslint-disable prettier/prettier */
import styled from 'styled-components/native';
import {GREEN, WHITE} from '../../helper/Color';
import {FONT, isIphoneXorAbove, SCREEN} from "../../helper/Constant";

const CardsStyles = {
  WrapperViewVertical: styled.View`
    flex: 1;
    backgroundColor: ${GREEN.app};
    padding-bottom: ${isIphoneXorAbove ?  80 : 60}px;
  `,
  SafeView: styled.SafeAreaView`
    flex: 1;
  `,
  MainWrapper: styled.View`
    marginVertical: 10px;
    border-radius: 10px;
    width: 95%;
    background-color: ${WHITE.dark};
    align-self: center;
  `,
  CardList: styled.FlatList`
    align-self: center;
  `,
  CardItemWrapper: styled.TouchableHighlight`
    height: ${(SCREEN.width -30 )/1.50}px;
    width: ${SCREEN.width - 30}px;
    align-self: center;
    borderRadius: 5px
  `,
  CardItemInnerWrapper: styled.View`
    height: ${(SCREEN.width -30 )/1.50}px;
    width: ${SCREEN.width - 30}px;
    align-self: center;
  `,
  
  CardItemImage: styled.Image`
    height: ${(SCREEN.width -30 )/1.50}px;
    width: ${SCREEN.width - 30}px;
    border-radius: 8px;
    align-self: center;
    position: absolute;
  `,
  CardIssuerTopText: styled.Text`
    margin-top: 40px;
    justify-content: center;
    color: ${WHITE.dark};
    font-size: 15px;
    font-family: ${FONT.SourceSansPro.regular};
    margin-left: 40px;
  `,
  CardTypeIcon: styled.Image`
    height: 36px;
    width: 40px;
    resizeMode: contain;
    align-self: flex-end;
    margin-right: 50px;
  `,
  CardInnerBottomView: styled.View`
    flex: 1;
    justify-content: flex-end;
    width: ${SCREEN.width - 55}px ;
    align-self: center;
    padding-bottom: 20px;
  `,
  CardNumberText: styled.Text`
    font-size: 17px;
    font-family: ${FONT.Muli.regular};
    paddingHorizontal: 30px;
    color: ${WHITE.dark};
    font-weight: bold;
  `,
  RowView: styled.View`
    margin-top: 13px;
    flexDirection: row;
    paddingHorizontal: 30px;
  `,
  Flex1View: styled.View`
    flex: 1;
  `,
  Muli8BoldText: styled.Text`
    font-family: ${FONT.Muli.bold};
    font-size: 8px;
    color: ${WHITE.dark};
  `,
  Muli17Regular: styled.Text`
    font-family: ${FONT.Muli.regular};
    font-size: 17px;
    color: ${WHITE.dark};
  `,
  AbsoluteBottomAddBtn: styled.TouchableHighlight`
    position: absolute;
    bottom: 105px;
    right: 20px;
    height: 46px;
    width: 46px;
    justify-content: center;
    align-items: center;
    border-radius: 23px;
  `,
  AbsoluteBtnImage: styled.Image`
    height: 46px;
    width: 46px;
    resizeMode: contain;
  `
};

export default CardsStyles;
