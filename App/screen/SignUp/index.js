/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import PhoneInput from 'react-native-phone-input';

import * as Actions from '../../redux/actions';
import SignUpStyles from './SignUpStyles';
import {BLACK, GREEN, WHITE} from "../../helper/Color";
import {FONT} from "../../helper/Constant";
import SignInStyles from "../SignIn/SignInStyles";

class SignUp extends Component {
  
  componentDidMount(){
  }
  
  render() {
    return (
      <SignUpStyles.WrapperViewVertical>
        <SignUpStyles.SafeView>
          <SignUpStyles.Muli20Bold>
            Let’s get you onboard!
          </SignUpStyles.Muli20Bold>
          <SignUpStyles.AppLogo source={require('../../assets/Layout/logo.png')} />
          <SignUpStyles.BottomViewWrapper>
            <SignUpStyles.LightBlackWrapper>
              <SignUpStyles.QuickSandRegular16>
                Choose your country
              </SignUpStyles.QuickSandRegular16>
              <SignUpStyles.PhoneInputWrapper>
              <PhoneInput
                ref={(ref) => {
                  this.phone = ref;
                }}
                initialCountry={'pk'}
                textProps={{
                  placeholder: 'Mobile Number',
                  placeholderTextColor: BLACK.appLight
                }}
                def
                flagStyle={{height: 30, width: 40, borderRadius: 5}}
                style={{
                  height: 46,
                  width: '100%',
                  alignSelf: 'center',
                }}
                textStyle={{color: WHITE.dark, fontSize: 15}}
              />
              </SignUpStyles.PhoneInputWrapper>
              <SignUpStyles.ContinueBtn onPress={() => this.props.navigation.navigate('SignUpCodeVerification')}
                                        underlayColor={BLACK.appLight}>
                <SignUpStyles.ContinueBtnText>
                  Continue
                </SignUpStyles.ContinueBtnText>
              </SignUpStyles.ContinueBtn>
              <SignUpStyles.AlreadyHaveText>
                Already have an account?{' '}<SignUpStyles.AlreadyHaveText onPress={() => this.props.navigation.pop()}>Sign In Now</SignUpStyles.AlreadyHaveText>
              </SignUpStyles.AlreadyHaveText>
            </SignUpStyles.LightBlackWrapper>
          </SignUpStyles.BottomViewWrapper>
        </SignUpStyles.SafeView>
      </SignUpStyles.WrapperViewVertical>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    userDetail: state.user.userDetail,
    userToken: state.user.userToken,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
