/* eslint-disable prettier/prettier */
import styled from 'styled-components/native';
import {BLACK, GREEN, WHITE} from '../../helper/Color';
import {FONT, SCREEN} from "../../helper/Constant";

const CardAddStyles = {
  WrapperViewVertical: styled.View`
    flex: 1;
    backgroundColor: ${GREEN.app};
  `,
  SafeView: styled.SafeAreaView`
    flex: 1;
  `,
  MainWrapper: styled.ScrollView`
    marginVertical: 10px;
    border-radius: 10px;
    height: 95%;
    width: 95%;
    background-color: ${WHITE.dark};
    align-self: center;
  `,
  CardList: styled.FlatList`
    align-self: center;
  `,
  CardItemWrapper: styled.TouchableHighlight`
    height: ${(SCREEN.width -30 )/1.50}px;
    width: ${SCREEN.width - 30}px;
    align-self: center;
    borderRadius: 10px;
  `,
  CardItemInnerWrapper: styled.View`
    height: ${(SCREEN.width -30 )/1.50}px;
    width: ${SCREEN.width - 30}px;
    align-self: center;
  `,

  CardItemImage: styled.Image`
    height: ${(SCREEN.width -30 )/1.50}px;
    width: ${SCREEN.width - 30}px;
    border-radius: 8px;
    align-self: center;
    position: absolute;
    borderRadius: 10px;
  `,
  CardIssuerTopText: styled.Text`
    margin-top: 40px;
    color: ${WHITE.dark};
    font-size: 15px;
    font-family: ${FONT.SourceSansPro.regular};
    marginLeft: 50px;
  `,
  CardTypeIcon: styled.Image`
    height: 36px;
    width: 40px;
    resizeMode: contain;
    align-self: flex-end;
    margin-right: 50px;
  `,
  CardInnerBottomView: styled.View`
    flex: 1;
    justify-content: flex-end;
    width: ${SCREEN.width - 55}px;
    align-self: center;
    padding-bottom: 10px;
  `,
  CardNumberText: styled.Text`
    font-size: 17px;
    font-family: ${FONT.Muli.regular};
    paddingHorizontal: 35px;
    color: ${WHITE.dark};
    font-weight: bold;
  `,
  RowView: styled.View`
    margin-top: 13px;
    flexDirection: row;
    paddingHorizontal: 30px;
  `,
  Flex1View: styled.View`
    flex: 1;
  `,
  Muli8BoldText: styled.Text`
    font-family: ${FONT.Muli.bold};
    font-size: 8px;
    color: ${WHITE.dark};
  `,
  Muli17Regular: styled.Text`
    font-family: ${FONT.Muli.regular};
    font-size: 17px;
    color: ${WHITE.dark};
  `,
  AddCardDetailView: styled.KeyboardAvoidingView`
    width: 95%;
    background: ${WHITE.light};
    height: 80%;
    align-self: center;
  `,

  KeyBoardAvoid: styled.KeyboardAvoidingView`
  `,

  SourceSansPro16Bold: styled.Text`
    fontSize: 16px;
    fontFamily: ${FONT.SourceSansPro.bold};
    marginTop: 5px;
    align-self: center;
  `,
  Muli12Regular: styled.Text`
    marginTop: 3px;
    align-self: center;
    font-size: 12px;
    font-family: ${FONT.Muli.regular};
  `,
  TextInputWrapper: styled.View`
    height: 50px;
    width: 85%;
    background: ${WHITE.dark};
    marginTop: 10;
    align-self: center;
    paddingHorizontal: 10px;
  `,
  SourceSansPro10Bold: styled.Text`
    font-family: ${FONT.SourceSansPro.bold};
    font-size: 10px;
  `,
  InputField: styled.TextInput`
    marginTop: 10px;
    font-size: 20px;
    height: 25px;
    width: 100%;
  `,
  AbsoluteRightScanBtn: styled.TouchableOpacity`
    height: 50px;
    width: 50px;
    justify-content: center;
    align-items: center;
    position: absolute;
    right: 10px;
  `,
  ScanImage: styled.Image`
    height: 40px;
    width: 50px;
    resizeMode: contain;
  `,
  RowInput: styled.TextInput`
    width: 100%;
    font-size: 20px;
    font-family: ${FONT.SourceSansPro.regular};
  `,
  SaveBtn: styled.TouchableHighlight`
    background: ${GREEN.app};
    height: 44px;
    width: 85%;
    justify-content: center;
    align-items: center;
    align-self: center;
    marginVertical: 15px
  `,
  SaveBtnText: styled.Text`
    font-family: ${FONT.SourceSansPro.bold};
    color: ${WHITE.dark};
    font-size: 16px;
  `,
  SwitchWrapper: styled.View`
    height: 40px;
    border-radius: 20px;
    width: 200px;
    align-self: center;
    background: ${BLACK.app};
    flex-direction: row;
    margin-top: 20px;
  `,
  SwitchInnerWrapper: styled.TouchableHighlight`
    height: 40px;
    width: 100px;
    justify-content: center;
    align-items: center;
    border-radius: 20px;
    border-width: 2px;
    border-color: ${BLACK.app};
  `,
  SwitchInnerText: styled.Text`
    font-size: 12px;
    font-family: ${FONT.Muli.semiBold};
    color: ${WHITE.dark};
  `,

};

export default CardAddStyles;
