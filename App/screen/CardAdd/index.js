import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {Platform} from 'react-native';
import {connect} from 'react-redux';

import * as Actions from '../../redux/actions';
import CardAddStyles from './CardAddStyles';
import HeaderWithTwoBtn from "../../component/HeaderWithTwoBtn";
import {BLACK, GREEN} from "../../helper/Color";
import DeleteAccount from "../../component/DeleteAccount";

class CardAdd extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cardNumber: '',
      cvv: '',
      expireDate: '',
      nameOnCard: '',
      zip: '',
      showCardType: false,
      selectedIndex: 0,
    }
  }
  
  
  render() {
    return (
      <CardAddStyles.WrapperViewVertical>
        
        <HeaderWithTwoBtn
          title={"Add New Card"}
          rightIcon={require("../../assets/Interests/cheerfulDog.png")}
          rightPress={() => alert("RightPress")}
          leftIcon={require('../../assets/Icons/36px/WithDarkBackground/backBoxBlack.png')}
          leftPress={() => this.props.navigation.pop()}
        />
        <CardAddStyles.SafeView>
          <CardAddStyles.MainWrapper>
            <CardAddStyles.CardItemWrapper>
              <CardAddStyles.CardItemInnerWrapper>
                <CardAddStyles.CardItemImage source={require('../../assets/Cards/grey.png')}/>
                <CardAddStyles.CardIssuerTopText>
                  Card Issuer
                </CardAddStyles.CardIssuerTopText>
                <CardAddStyles.CardTypeIcon source={require('../../assets/CardIssuers/visaLogo.png')}/>
                <CardAddStyles.CardInnerBottomView>
                  <CardAddStyles.CardNumberText>
                    {this.state.cardNumber.length < 23 ? '****   ****   ****   ****' : '****   ****   ****   ' + this.state.cardNumber.slice(21, 25)}
                  </CardAddStyles.CardNumberText>
                  <CardAddStyles.RowView>
                    <CardAddStyles.Flex1View>
                      <CardAddStyles.Muli8BoldText>
                        CARDHOLDER NAME
                      </CardAddStyles.Muli8BoldText>
                    </CardAddStyles.Flex1View>
                    <CardAddStyles.Flex1View>
                      <CardAddStyles.Muli8BoldText>
                        EXPIRE DATE
                      </CardAddStyles.Muli8BoldText>
                    </CardAddStyles.Flex1View>
                  </CardAddStyles.RowView>
                  <CardAddStyles.RowView style={{marginBottom: 10}}>
                    <CardAddStyles.Flex1View>
                      <CardAddStyles.Muli8BoldText>
                        {this.state.nameOnCard}
                      </CardAddStyles.Muli8BoldText>
                    </CardAddStyles.Flex1View>
                    <CardAddStyles.Flex1View>
                      <CardAddStyles.Muli8BoldText>
                        {this.state.expireDate}
                      </CardAddStyles.Muli8BoldText>
                    </CardAddStyles.Flex1View>
                  </CardAddStyles.RowView>
                </CardAddStyles.CardInnerBottomView>
              </CardAddStyles.CardItemInnerWrapper>
            </CardAddStyles.CardItemWrapper>
            <CardAddStyles.KeyBoardAvoid
              behavior={Platform.OS === "ios" ? "padding" : "height"}
              enabled>
              <CardAddStyles.AddCardDetailView>
                <CardAddStyles.SourceSansPro16Bold>
                  ADD YOUR DETAILS BELOW
                </CardAddStyles.SourceSansPro16Bold>
                <CardAddStyles.Muli12Regular>
                  We never store your full card information on our systems.
                </CardAddStyles.Muli12Regular>
                <CardAddStyles.TextInputWrapper>
                  <CardAddStyles.SourceSansPro10Bold>
                    CARD NUMBER
                  </CardAddStyles.SourceSansPro10Bold>
                  <CardAddStyles.InputField
                    placeholder={'****   ****   ****    ****'}
                    value={this.state.cardNumber}
                    keyboardType={'numeric'}
                    maxLength={25}
                    onChangeText={(text) => {
                      if (text.length === 4 || text.length === 11 || text.length === 18) {
                        if (text.trim() === this.state.cardNumber.trim()) {
                          this.setState({cardNumber: text})
                        } else {
                          this.setState({cardNumber: text + '   '})
                        }
                      } else {
                        this.setState({cardNumber: text})
                      }
                    }}
                    placeholderTextColor={BLACK.light}/>
                  <CardAddStyles.AbsoluteRightScanBtn>
                    <CardAddStyles.ScanImage source={require('../../assets/OtherIcons/scanBtn.png')}/>
                  </CardAddStyles.AbsoluteRightScanBtn>
                </CardAddStyles.TextInputWrapper>
                <CardAddStyles.TextInputWrapper>
                  <CardAddStyles.RowView style={{paddingHorizontal: 0}}>
                    <CardAddStyles.Flex1View>
                      <CardAddStyles.SourceSansPro10Bold style={{color: BLACK.dark}}>
                        EXPIRE DATE
                      </CardAddStyles.SourceSansPro10Bold>
                      <CardAddStyles.RowInput
                        placeholder={'03/22'}
                        maxLength={5}
                        keyboardType={'numeric'}
                        onChangeText={(text) => {
                          if (text.trim().length === 2) {
                            if (this.state.expireDate.length === 3) {
                              this.setState({expireDate: text})
                            } else {
                              this.setState({expireDate: text + '/'})
                            }
                          } else if (text.trim().length === 3) {
                            this.setState({expireDate: text})
                          } else {
                            this.setState({expireDate: text})
                          }
                        }}
                        value={this.state.expireDate}/>
                    </CardAddStyles.Flex1View>
                    <CardAddStyles.Flex1View>
                      <CardAddStyles.SourceSansPro10Bold style={{color: BLACK.dark}}>
                        CVV
                      </CardAddStyles.SourceSansPro10Bold>
                      <CardAddStyles.RowInput
                        placeholder={'222'}
                        keyboardType={'numeric'}
                        value={this.state.cvv}
                        onChangeText={(text) => this.setState({cvv: text})}
                        maxLength={3}
                        placeholderColor={BLACK.light}/>
                    </CardAddStyles.Flex1View>
                  </CardAddStyles.RowView>
                </CardAddStyles.TextInputWrapper>
                <CardAddStyles.TextInputWrapper>
                  <CardAddStyles.RowView style={{paddingHorizontal: 0}}>
                    <CardAddStyles.Flex1View>
                      <CardAddStyles.SourceSansPro10Bold style={{color: BLACK.dark}}>
                        NAME ON CARD
                      </CardAddStyles.SourceSansPro10Bold>
                      <CardAddStyles.RowInput
                        placeholder={'John Doe'}
                        onChangeText={(text) => this.setState({nameOnCard: text})}
                        maxLength={14} value={this.state.nameOnCard}/>
                    </CardAddStyles.Flex1View>
                    <CardAddStyles.Flex1View>
                      <CardAddStyles.SourceSansPro10Bold style={{color: BLACK.dark}}>
                        ZIP
                      </CardAddStyles.SourceSansPro10Bold>
                      <CardAddStyles.RowInput
                        placeholder={'10282'}
                        maxLength={6}
                        value={this.state.zip}
                        onChangeText={(text) => this.setState({zip: text})}
                        keyboardType={'numeric'}
                        placeholderColor={BLACK.light}/>
                    </CardAddStyles.Flex1View>
                  </CardAddStyles.RowView>
                </CardAddStyles.TextInputWrapper>
                {!this.state.showCardType ? <CardAddStyles.SaveBtn
                  onPress={() => this.setState({showCardType: true})} underlayColor={GREEN.app}>
                  <CardAddStyles.SaveBtnText>
                    Save
                  </CardAddStyles.SaveBtnText>
                </CardAddStyles.SaveBtn> : <CardAddStyles.SwitchWrapper>
                  <CardAddStyles.SwitchInnerWrapper
                    onPress={() => this.setState({selectedIndex: 0})}
                    style={{backgroundColor: this.state.selectedIndex === 0 ? GREEN.app : BLACK.app}}>
                    <CardAddStyles.SwitchInnerText>
                      PERSONAL
                    </CardAddStyles.SwitchInnerText>
                  </CardAddStyles.SwitchInnerWrapper>
                  <CardAddStyles.SwitchInnerWrapper
                    onPress={() => this.setState({selectedIndex: 1})}
                    style={{backgroundColor: this.state.selectedIndex === 1 ? GREEN.app : BLACK.app}}>
                    <CardAddStyles.SwitchInnerText>
                      BUSINESS
                    </CardAddStyles.SwitchInnerText>
                  </CardAddStyles.SwitchInnerWrapper>
                </CardAddStyles.SwitchWrapper>}
                {this.state.showCardType && <CardAddStyles.RowView>
                  <CardAddStyles.Flex1View>
                    <CardAddStyles.SaveBtn underlayColor={GREEN.btn} style={{backgroundColor: GREEN.btn}}>
                      <CardAddStyles.SaveBtnText>
                        Update/Link
                      </CardAddStyles.SaveBtnText>
                    </CardAddStyles.SaveBtn>
                  </CardAddStyles.Flex1View>
                  <CardAddStyles.Flex1View>
                    <CardAddStyles.SaveBtn
                      underlayColor={GREEN.app}
                      onPress={() => this.setState({deletePress: true})}>
                      <CardAddStyles.SaveBtnText>
                        Delete
                      </CardAddStyles.SaveBtnText>
                    </CardAddStyles.SaveBtn>
                  </CardAddStyles.Flex1View>
                </CardAddStyles.RowView>}
              </CardAddStyles.AddCardDetailView>
            </CardAddStyles.KeyBoardAvoid>
          </CardAddStyles.MainWrapper>
        </CardAddStyles.SafeView>
        {this.state.deletePress && <DeleteAccount
          savePress={() => this.setState({deletePress: false})}
          cancelPress={() => this.setState({deletePress: false})}/>}
      </CardAddStyles.WrapperViewVertical>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    userDetail: state.user.userDetail,
    userToken: state.user.userToken,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CardAdd);
