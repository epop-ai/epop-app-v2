/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as Actions from '../../redux/actions';
import StartupStyles from './StartupStyles';
import {BLACK, GREEN} from '../../helper/Color';

class Startup extends Component {
  render() {
    return (
      <StartupStyles.WrapperViewVertical>
        <StartupStyles.SafeView>
          <StartupStyles.WhiteWrapperView>
            <StartupStyles.WhiteInnerView />
            <StartupStyles.Icon
              source={require('../../assets/Layout/logo.png')}
            />
            <StartupStyles.AppDetailText>
              Your contactless digital receipts in one place!
            </StartupStyles.AppDetailText>
            <StartupStyles.BottomViewWrapper>
              <StartupStyles.AbsoluteImage source={require('../../assets/Layout/background.png')} />
              <StartupStyles.BottomViewWhite />
            <StartupStyles.BottomView>
              <StartupStyles.RowView>
                <StartupStyles.SignInBtn
                  underlayColor={BLACK.transParent}
                  onPress={() => this.props.navigation.navigate('SignIn')}>
                  <StartupStyles.BtnText>Sign In</StartupStyles.BtnText>
                </StartupStyles.SignInBtn>
                <StartupStyles.ScanBtn
                  underlayColor={GREEN.app}
                  onPress={() => this.props.navigation.navigate('Main')}>
                  <StartupStyles.BtnInnerImage
                    source={require('../../assets/Layout/face.png')}
                  />
                </StartupStyles.ScanBtn>
              </StartupStyles.RowView>
              <StartupStyles.JoinBtn onPress={() => this.props.navigation.navigate('SignUp')} underlayColor={GREEN.app}>
                <StartupStyles.BtnText>Join ePOP Today</StartupStyles.BtnText>
              </StartupStyles.JoinBtn>

              <StartupStyles.RowView>
                <StartupStyles.Flex1View>
                  <StartupStyles.OrViewLineView />
                </StartupStyles.Flex1View>
                <StartupStyles.OrText>OR SIGN UP WITH</StartupStyles.OrText>
                <StartupStyles.Flex1View>
                  <StartupStyles.OrViewLineView />
                </StartupStyles.Flex1View>
              </StartupStyles.RowView>
              <StartupStyles.RowView>
                <StartupStyles.SocialBtn>
                  <StartupStyles.BtnInnerImage
                    source={require('../../assets/Layout/facebook.png')}
                  />
                </StartupStyles.SocialBtn>
                <StartupStyles.SocialBtn style={{marginLeft: 17}}>
                  <StartupStyles.BtnInnerImage
                    source={require('../../assets/Layout/apple.png')}
                  />
                </StartupStyles.SocialBtn>
                <StartupStyles.SocialBtn style={{marginLeft: 17}}>
                  <StartupStyles.BtnInnerImage
                    source={require('../../assets/Layout/google.png')}
                  />
                </StartupStyles.SocialBtn>
              </StartupStyles.RowView>
            </StartupStyles.BottomView>
            </StartupStyles.BottomViewWrapper>
            </StartupStyles.WhiteWrapperView>
        </StartupStyles.SafeView>
      </StartupStyles.WrapperViewVertical>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    userDetail: state.user.userDetail,
    userToken: state.user.userToken,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Startup);
