/* eslint-disable prettier/prettier */
import styled from 'styled-components';
import {BLACK, GREEN, WHITE} from '../../helper/Color';
import {FONT, isIphoneXorAbove, SCREEN} from '../../helper/Constant';

const StartupStyles = {
  WrapperViewVertical: styled.View`
    flex: 1;
    justifyContent: center;
    backgroundColor: ${GREEN.app};
  `,
  AbsoluteImage: styled.Image`
    height: 100%;
    width: ${SCREEN.width}px;
    position: absolute;
  `,
  SafeView: styled.View`
    flex: 1;
    justifyContent: flex-end;
    alignItems: center;
    backgroundColor: ${GREEN.app};
  `,
  WhiteWrapperView: styled.View`
    height: 85%;
    width: 100%;
    marginTop: 40px;
  `,
  WhiteInnerView: styled.View`
    height: 100%;
    width: ${SCREEN.width - 46}px;
    backgroundColor: ${WHITE.app};
    borderRadius: 16px;
    position: absolute;
    alignSelf: center;
  `,
  Icon: styled.Image`
    height: ${(SCREEN.width - 86) / 2.8}px;
    width: ${SCREEN.width - 86}px;
    marginVertical: 5px;
    alignSelf: center;
    resizeMode: contain;
  `,
  AppDetailText: styled.Text`
    fontSize: 20px;
    font-family: ${FONT.QuickSand.regular};
    
    paddingHorizontal: 50px;
    alignSelf: center;
    textAlign: center;
  `,
  BottomView: styled.View`
    height: ${SCREEN.height / 1.7}px;
    flex: 1;
    justifyContent: flex-end;
    backgroundColor: ${GREEN.app_light};
    paddingBottom: ${isIphoneXorAbove ? 40 : 20}px;
  `,
  BottomViewWhite: styled.View`
    height: 95%;
    width: ${SCREEN.width - 46}px;
    align-self: center;
    top: 0;
    position: absolute;
    background-color: ${WHITE.signInScreenBack};
    border-bottom-left-radius: 16px;
    border-bottom-left-radius: 16px;
  `,
  BottomViewWrapper: styled.View`
    marginTop: 50px;
    height: ${SCREEN.height / 1.7}px;
    flex: 1;
    justifyContent: flex-end;
  `,
  RowView: styled.View`
    width: ${SCREEN.width - 90}px;
    flexDirection: row;
    alignItems: center;
    paddingVertical: 5px;
    justifyContent: center;
    alignSelf: center;
  `,
  SignInBtn: styled.TouchableHighlight`
    backgroundColor: ${GREEN.btn};
    height: 46px;
    width: ${SCREEN.width - 160}px;
    alignItems: center;
    justifyContent: center;
    borderRadius: 5px;
  `,
  BtnText: styled.Text`
    fontSize: 16px;
    
    color: ${WHITE.app};
  `,
  ScanBtn: styled.TouchableHighlight`
    height: 45px;
    width: 45px;
    marginLeft: 25px;
    justifyContent: center;
    alignItems: center;
  `,
  BtnInnerImage: styled.Image`
    height: 100%;
    width: 100%;
    borderRadius: 5px;
  `,
  JoinBtn: styled.TouchableHighlight`
    width: ${SCREEN.width - 90}px;
    height: 46px;
    alignItems: center;
    justifyContent: center;
    backgroundColor: ${GREEN.app};
    alignSelf: center;
    marginTop: 25px;
    marginBottom: 10px;
    borderRadius: 5px;
  `,
  Flex1View: styled.View`
    flex: 1;
  `,
  OrViewLineView: styled.View`
    height: 1px;
    width: 98%;
    alignSelf: flex-end;
    backgroundColor: ${BLACK.dark};
  `,
  OrText: styled.Text`
    fontSize: 14px;
   
    color: ${BLACK.dark};
    paddingHorizontal: 5px;
    letterSpacing: 2px;
  `,
  SocialBtn: styled.TouchableHighlight`
    height: 44px;
    width: 44px;
    justifyContent: center;
    alignItems: center;
  `,
};

export default StartupStyles;
