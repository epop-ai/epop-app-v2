/* eslint-disable no-alert */
import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Platform } from "react-native";
import { NavigationEvents } from "react-navigation";
import * as ImagePicker from "expo-image-picker";

import * as Actions from "../../redux/actions";
import InitialSetupStyles from "./InitialSetupStyles";
import { GREEN, WHITE } from "../../helper/Color";
import AllSetTutorial from "../../component/AllSetTutorial";
import Modal from "react-native-modal";
import { InterestList } from "../../helper/Constant";

var selectedinterest = [];

class InitialSetup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageUri: "",
      showAllSetPopUp: false,
      openInterestModal: false,
      selectedItemList: [],
    };
  }

  async getAsyncData() {
    if (Platform.OS !== "web") {
      const {
        status,
      } = await ImagePicker.requestMediaLibraryPermissionsAsync();
      if (status !== "granted") {
        alert("Sorry, we need camera roll permissions to make this work!");
      }
    }
  }

  pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      this.setState({ imageUri: result.uri });
    }
  };

  interestItemSelect(itemName) {
    if (selectedinterest.includes(itemName)) {
      const indexOfItem = selectedinterest.indexOf(itemName);
      selectedinterest.splice(indexOfItem, 1);
      this.setState({ selectedItemList: selectedinterest });
    } else {
      selectedinterest.push(itemName);
      this.setState({ selectedItemList: selectedinterest });
    }
  }

  render() {
    return (
      <InitialSetupStyles.WrapperViewVertical>
        <NavigationEvents
          onDidFocus={() => {
            setTimeout(() => {
              this.getAsyncData();
            }, 200);
          }}
        />
        {!this.state.showAllSetPopUp ? (
          <InitialSetupStyles.SafeView>
            <InitialSetupStyles.Muli20Bold>
              Let’s get to know you better!
            </InitialSetupStyles.Muli20Bold>
            <InitialSetupStyles.UserImageWrapper>
              {this.state.imageUri === "" ? (
                <InitialSetupStyles.UserImageText>
                  S
                </InitialSetupStyles.UserImageText>
              ) : (
                <InitialSetupStyles.ImageUser
                  source={{ uri: this.state.imageUri }}
                />
              )}
            </InitialSetupStyles.UserImageWrapper>
            <InitialSetupStyles.AddImageBtn onPress={() => this.pickImage()}>
              <InitialSetupStyles.AddImage
                source={require("../../assets/OtherIcons/updatePic.png")}
              />
            </InitialSetupStyles.AddImageBtn>
            <InitialSetupStyles.UpdateText>
              Update your profile picture
            </InitialSetupStyles.UpdateText>
            {this.state.imageUri !== "" && (
              <InitialSetupStyles.LookingGreatText>
                Looking Great!
              </InitialSetupStyles.LookingGreatText>
            )}
            <InitialSetupStyles.BottomView>
              <InitialSetupStyles.BottomRowView>
                <InitialSetupStyles.ButtonMainWrapper>
                  <InitialSetupStyles.ButtonInnerWrapper
                    style={{ backgroundColor: GREEN.app }}
                  >
                    <InitialSetupStyles.BtnText>
                      Maybe Later
                    </InitialSetupStyles.BtnText>
                  </InitialSetupStyles.ButtonInnerWrapper>
                </InitialSetupStyles.ButtonMainWrapper>

                <InitialSetupStyles.ButtonMainWrapper>
                  <InitialSetupStyles.ButtonInnerWrapper
                    style={{ backgroundColor: GREEN.btn }}
                    onPress={() => this.setState({ openInterestModal: true })}
                  >
                    <InitialSetupStyles.BtnText>
                      Continue
                    </InitialSetupStyles.BtnText>
                  </InitialSetupStyles.ButtonInnerWrapper>
                </InitialSetupStyles.ButtonMainWrapper>
              </InitialSetupStyles.BottomRowView>
            </InitialSetupStyles.BottomView>
          </InitialSetupStyles.SafeView>
        ) : (
          <AllSetTutorial
            startTutorial={() => alert("start Tutorial")}
            iAmGood={() => this.props.navigation.navigate("Main")}
          />
        )}
        <Modal
          coverScreen={false}
          hasBackdrop={false}
          animationIn={"slideInUp"}
          isVisible={this.state.openInterestModal}
        >
          <InitialSetupStyles.AddInterestWrapper>
            <InitialSetupStyles.AddInterestInnerWrapper>
              <InitialSetupStyles.QuickSand16Regular>
                To help you get the most out of ePOP
              </InitialSetupStyles.QuickSand16Regular>
              <InitialSetupStyles.QuickSand16Bold>
                Choose 6 topics that interest you
              </InitialSetupStyles.QuickSand16Bold>
              <InitialSetupStyles.InterestList
                data={InterestList}
                keyExtractor={(item, index) => index.toString()}
                numColumns={3}
                renderItem={(item, index) => (
                  <InitialSetupStyles.InterestItemWrapper
                    onPress={() => this.interestItemSelect(item.item.name)}
                    underlayColor={WHITE.dark}
                  >
                    <InitialSetupStyles.InterestItemInnerWrapper>
                      <InitialSetupStyles.InterestImage
                        source={item.item.image}
                      />
                      <InitialSetupStyles.InterestName>
                        {item.item.name}
                      </InitialSetupStyles.InterestName>
                      {this.state.selectedItemList.includes(item.item.name) && (
                        <InitialSetupStyles.TickInterestAbsolute
                          source={require("../../assets/Icons/36px/WithDarkBackground/saveGreenBackBlack.png")}
                        />
                      )}
                    </InitialSetupStyles.InterestItemInnerWrapper>
                  </InitialSetupStyles.InterestItemWrapper>
                )}
              />
              <InitialSetupStyles.BottomButtonView>
                <InitialSetupStyles.BottomRowView>
                  <InitialSetupStyles.ButtonMainWrapper>
                    <InitialSetupStyles.ButtonInnerWrapper
                      style={{ backgroundColor: GREEN.app }}
                    >
                      <InitialSetupStyles.BtnText>
                        Maybe Later
                      </InitialSetupStyles.BtnText>
                    </InitialSetupStyles.ButtonInnerWrapper>
                  </InitialSetupStyles.ButtonMainWrapper>

                  <InitialSetupStyles.ButtonMainWrapper>
                    <InitialSetupStyles.ButtonInnerWrapper
                      style={{ backgroundColor: GREEN.btn }}
                      onPress={() =>
                        this.setState({
                          openInterestModal: false,
                          showAllSetPopUp: true,
                        })
                      }
                    >
                      <InitialSetupStyles.BtnText>
                        Continue
                      </InitialSetupStyles.BtnText>
                    </InitialSetupStyles.ButtonInnerWrapper>
                  </InitialSetupStyles.ButtonMainWrapper>
                </InitialSetupStyles.BottomRowView>
              </InitialSetupStyles.BottomButtonView>
            </InitialSetupStyles.AddInterestInnerWrapper>
          </InitialSetupStyles.AddInterestWrapper>
        </Modal>
      </InitialSetupStyles.WrapperViewVertical>
    );
  }
}

function mapStateToProps(state) {
  return {
    userDetail: state.user.userDetail,
    userToken: state.user.userToken,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(InitialSetup);
