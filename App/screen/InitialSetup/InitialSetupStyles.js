/* eslint-disable prettier/prettier */
import styled from 'styled-components/native';
import {BLACK, GREEN, WHITE} from '../../helper/Color';
import { FONT, isIphoneXorAbove, SCREEN } from '../../helper/Constant';

const InitialSetupStyles = {
  WrapperViewVertical: styled.View`
    flex: 1;
    backgroundColor: ${GREEN.app};
  `,
  SafeView: styled.SafeAreaView`
    flex: 1;
  `,
  Muli20Bold: styled.Text`
    font-family: ${FONT.Muli.bold};
    font-size: 20px;
    align-self: center;
    color: ${WHITE.app};
    margin-top: 19px;
  `,
  UserImageWrapper: styled.View`
    height: ${SCREEN.width / 2.3}px;
    width: ${SCREEN.width / 2.3}px;
    margin-top: 34px;
    background-color: ${BLACK.app};
    border-radius: 15px;
    align-self: center;
    justify-content: center;
    align-items: center;
  `,
  UserImageText: styled.Text`
    font-size: 75px;
    font-family: ${FONT.Muli.bold};
    color: ${WHITE.dark};
  `,
  ImageUser: styled.Image`
    height: ${SCREEN.width / 2.3}px;
    width: ${SCREEN.width / 2.3}px;
    align-self: center;
    border-radius: 15px;
  `,
  AddImageBtn: styled.TouchableHighlight`
    height: 40px;
    width: 40px;
    align-self: center;
    margin-top: 25px;
  `,
  AddImage: styled.Image`
    height: 40px;
    width: 40px;
    resizeMode: contain;
    align-self: center;
  `,
  UpdateText: styled.Text`
    font-family: ${FONT.SourceSansPro.bold};
    font-size: 10px;
    color: ${WHITE.dark};
    align-self: center;
  `,
  LookingGreatText: styled.Text`
    font-size: 18px;
    font-family: ${FONT.Muli.boldItalic};
    color: ${WHITE.dark};
    align-self: center;
    margin-top: 22px;
  `,
  BottomView: styled.View`
    flex: 1;
    justify-content: flex-end;
    padding-bottom: 20px;
  `,
  BottomRowView: styled.View`
    height: 46px;
    width: 100%;
    flex-direction: row;
  `,
  ButtonMainWrapper: styled.View`
    flex: 1;
    justify-content: center;
    align-items: center;
  `,
  ButtonInnerWrapper: styled.TouchableHighlight`
    height: 100%;
    width: 85%;
    border-radius: 4px;
    border-width: 0.3px;
    border-color: ${BLACK.appLight};
    shadowColor: ${BLACK.appLight};
    shadowOpacity: 0.3;
    shadowRadius: 10px;
    justify-content: center;
    align-items: center;
  `,
  BtnText: styled.Text`
    font-size: 16px;
    font-family: ${FONT.Muli.bold};
    color: ${WHITE.dark};
  `,
  AddInterestWrapper: styled.TouchableHighlight`
    height: ${SCREEN.height}px;
    width: ${SCREEN.width}px;
    align-self: center;
    justify-content: flex-end;
  `,
  AddInterestInnerWrapper: styled.View`
    border-radius: 10px;
    width: 95%;
    background-color: ${WHITE.dark};
    margin-bottom: 10px;
    align-self: center;
  `,
  QuickSand16Regular: styled.Text`
    font-size: 16px;
    font-family: ${FONT.QuickSand.regular};
    color: ${BLACK.appLight};
    align-self: center;
    paddingHorizontal: 10px;
    padding-top: 5px;
  `,
  QuickSand16Bold: styled.Text`
    font-family: ${FONT.QuickSand.bold};
    font-size: 16px;
    color: ${BLACK.dark};
    font-weight: bold;
    paddingHorizontal: 10px;
    align-self: center;
  `,
  InterestList: styled.FlatList`
     height: ${SCREEN.height / 2}px;
  `,
  InterestItemWrapper: styled.TouchableHighlight`
    height: ${SCREEN.width / 3.2}px;
    width: ${SCREEN.width / 3.2}px;
    justify-content: center;
    align-items: center;
  `,
  InterestItemInnerWrapper: styled.View`
    height: 100%;
    width: 100%;
    justify-content: center;
    align-items: center;
  `,
  InterestImage: styled.Image`
    height: 70%;
    width: 70%;
  `,
  InterestName: styled.Text`
    marginTop: 5px;
    fontFamily: ${FONT.SourceSansPro.regular};
    color: ${GREEN.app};
  `,
  TickInterestAbsolute: styled.Image`
    position: absolute;
    height: 40px;
    width: 40px;
    top: ${(SCREEN.width / 3.2)/4.5}px
  `,
  BottomButtonView: styled.View`
    height: 90px;
    justify-content: center;
    width: 100%;
  `
};

export default InitialSetupStyles;
