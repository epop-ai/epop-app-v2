/* eslint-disable react-native/no-inline-styles */
import React, {Component} from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import PhoneInput from "react-native-phone-input";

import * as Actions from "../../redux/actions";
import SignUpCodeVerificationStyles from "./SignUpCodeVerificationStyles";
import {BLACK, WHITE} from "../../helper/Color";
import {FONT} from "../../helper/Constant";

class SignUpCodeVerification extends Component {
  componentDidMount() {
  }
  
  updateInfo() {
    this.setState({
      valid: this.phone.isValidNumber(),
      type: this.phone.getNumberType(),
      value: this.phone.getValue(),
    });
  }
  
  render() {
    return (
      <SignUpCodeVerificationStyles.WrapperViewVertical>
        <SignUpCodeVerificationStyles.SafeView>
          <SignUpCodeVerificationStyles.Scroll>
          <SignUpCodeVerificationStyles.Muli20Bold>
            One-time Passcode
          </SignUpCodeVerificationStyles.Muli20Bold>
          <SignUpCodeVerificationStyles.AppLogo
            source={require("../../assets/Layout/logo.png")}
          />
          <SignUpCodeVerificationStyles.Muli20Bold style={{marginTop: 10}}>
            Verification code
          </SignUpCodeVerificationStyles.Muli20Bold>
          <SignUpCodeVerificationStyles.BottomViewWrapper>
            <SignUpCodeVerificationStyles.LightBlackWrapper>
              <SignUpCodeVerificationStyles.QuickSandRegular16>
                We just sent a verification code{"\n"}to 1 646 123 4567
              </SignUpCodeVerificationStyles.QuickSandRegular16>
              <SignUpCodeVerificationStyles.CodeVerificationWrapper>
                <SignUpCodeVerificationStyles.CodeVerificationTextInputInner>
                  <SignUpCodeVerificationStyles.CodeVerificationTextInput
                    autoFocus={true}
                    keyboardType={"numeric"}
                    maxLength={1}
                    onChangeText={(text) => {
                      if (text.toString().trim() !== "") {
                        this.secondTextInput.focus();
                      }
                    }}
                  />
                </SignUpCodeVerificationStyles.CodeVerificationTextInputInner>
                <SignUpCodeVerificationStyles.CodeVerificationTextInputInner>
                  <SignUpCodeVerificationStyles.CodeVerificationTextInput
                    ref={(input) => {
                      this.secondTextInput = input;
                    }}
                    keyboardType={"numeric"}
                    maxLength={1}
                    onChangeText={(text) => {
                      if (text.toString().trim() !== "") {
                        this.thirdTextInput.focus();
                      }
                    }}
                  />
                </SignUpCodeVerificationStyles.CodeVerificationTextInputInner>
                <SignUpCodeVerificationStyles.CodeVerificationTextInputInner>
                  <SignUpCodeVerificationStyles.CodeVerificationTextInput
                    ref={(input) => {
                      this.thirdTextInput = input;
                    }}
                    keyboardType={"numeric"}
                    maxLength={1}
                    onChangeText={(text) => {
                      if (text.toString().trim() !== "") {
                        this.fourthTextInput.focus();
                      }
                    }}
                  />
                </SignUpCodeVerificationStyles.CodeVerificationTextInputInner>
                <SignUpCodeVerificationStyles.CodeVerificationTextInputInner>
                  <SignUpCodeVerificationStyles.CodeVerificationTextInput
                    ref={(input) => {
                      this.fourthTextInput = input;
                    }}
                    keyboardType={'numeric'}
                    maxLength={1} onChangeText={(text) => {
                    if (text.toString().trim() !== '') {
                      this.props.navigation.navigate('SignUpUserDetail')
                    }
                  }}
                  />
                </SignUpCodeVerificationStyles.CodeVerificationTextInputInner>
              </SignUpCodeVerificationStyles.CodeVerificationWrapper>
              <SignUpCodeVerificationStyles.QuickSandRegular16 style={{paddingHorizontal: 15, paddingVertical: 15}}>
                you don’t receive the code in{'\n'}30 seconds tap below
                to resend it
              </SignUpCodeVerificationStyles.QuickSandRegular16>
              <SignUpCodeVerificationStyles.ResendBtn>
                <SignUpCodeVerificationStyles.ResendBtnText>
                           Resend
                </SignUpCodeVerificationStyles.ResendBtnText>
              </SignUpCodeVerificationStyles.ResendBtn>
            </SignUpCodeVerificationStyles.LightBlackWrapper>
          </SignUpCodeVerificationStyles.BottomViewWrapper>
          </SignUpCodeVerificationStyles.Scroll>
        </SignUpCodeVerificationStyles.SafeView>
      </SignUpCodeVerificationStyles.WrapperViewVertical>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    userDetail: state.user.userDetail,
    userToken: state.user.userToken,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignUpCodeVerification);
