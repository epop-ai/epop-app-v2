/* eslint-disable prettier/prettier */
import styled from 'styled-components';
import {BLACK, GREEN, WHITE} from '../../helper/Color';
import {FONT, SCREEN} from '../../helper/Constant';

const SignUpCodeVerificationStyles = {
  WrapperViewVertical: styled.View`
    flex: 1;
    backgroundColor: ${WHITE.dark};
  `,
  SafeView: styled.SafeAreaView`
    flex: 1;
    padding-top: 9px;
  `,
  Scroll: styled.ScrollView`
    flex: 1;
  `,
  Muli20Bold: styled.Text`
    font-size: 20px;
    font-family: ${FONT.Muli.bold};
    color: ${BLACK.app};
    margin-top: 19px;
    align-self: center;
  `,
  AppLogo: styled.Image`
    height: 117px;
    width: ${SCREEN.width -64}px;
    align-self: center;
    margin-top: 15px;
    resizeMode: contain;
  `,
  BottomViewWrapper: styled.View`
    flex: 1;
    padding-top: 15px;
  `,
  LightBlackWrapper: styled.View`
    width: ${SCREEN.width - 20}px;
    height: 95%;
    align-self: center;
    border-radius: 12px;
    padding-top: 33px;
    padding-bottom: 200px;
    background-color: ${WHITE.light};
    border-width: 1px;
    border-color: ${BLACK.borderLight};
  `,
  QuickSandRegular16: styled.Text`
    font-family: ${FONT.QuickSand.regular};
    color: ${BLACK.textColorDark};
    font-size: 16px;
    align-self: center;
    align-self: center;
    text-align: center;
  `,
  PhoneInputWrapper: styled.View`
    height: 46px;
    width: ${SCREEN.width - 80}px;
    align-self: center;
    justify-content: center;
    background-color: ${WHITE.dark};
    paddingHorizontal: 15px;
    border-radius: 6px;
    margin-top: 26px;
  `,
  ResendBtn: styled.TouchableHighlight`
    height: 46px;
    background-color: ${GREEN.app};
    width: ${SCREEN.width - 80}px;
    align-self: center;
    border-radius: 6px;
    margin-top: 14px;
    justify-content: center;
    align-items: center;
  `,
  ResendBtnText: styled.Text`
    font-size: 16px;
    font-family: ${FONT.Muli.bold};
    color: ${WHITE.app};
  `,
  AlreadyHaveText: styled.Text`
    font-size: 13px;
    font-family: ${FONT.SourceSansPro.regular};
    color: ${GREEN.app};
    margin-top: 46px;
    align-self: center;
  `,
  CodeVerificationWrapper: styled.View`
    height: 100px;
    width: 100%;
    align-items: center;
    flex-direction: row;
    justify-content: center;
    margin-top: 15px;
  `,
  CodeVerificationTextInputInner: styled.View`
    height: 90px;
    width: 55px;
    backgroundColor: ${WHITE.dark};
    border-radius: 20px;
    marginHorizontal: 5px;
    justify-content: center;
    align-items: center;
  `,
  CodeVerificationTextInput: styled.TextInput`
    border-bottom-width: 1px;
    border-color: ${GREEN.btn};
    width: 50%;
    height: 60%;
    text-align: center;
    font-size: 35px;
    font-family: ${FONT.SourceSansPro.regular};
    color: ${GREEN.btn};
  `,
};

export default SignUpCodeVerificationStyles;
