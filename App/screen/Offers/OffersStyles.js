/* eslint-disable prettier/prettier */
import styled from 'styled-components/native';
import {BLACK, GREEN, WHITE} from '../../helper/Color';
import {FONT, isIphoneXorAbove, SCREEN} from "../../helper/Constant";

const OffersStyles = {
  WrapperViewVertical: styled.View`
    flex: 1;
    padding-bottom: ${isIphoneXorAbove ?  80 : 60}px;
    backgroundColor: ${GREEN.app};
  `,
  SafeView: styled.SafeAreaView`
    flex: 1;
  `,
  RowView: styled.View`
    flex-direction: row;
    height: 55px;
    width: 100%;
  `,
  TopTabWrapper: styled.View`
    flex: 1;
    justify-content: center;
    align-items: center;
    border-bottom-color: ${GREEN.btn};
  `,
  TopTabInnerWrapper: styled.TouchableHighlight`
    height: 60%;
    width: 60%;
    justify-content: center;
    align-items: center;
    border-radius: 25px;
  `,
  TabText: styled.Text`
    fontSize: 12px;
    font-family: ${FONT.Muli.semiBold};
    color: ${WHITE.dark};
  `,
  ScrollView: styled.ScrollView`
    flex: 1;
  `,
  OffersTopWrapper: styled.View`
    flex: 1;
  `,
  OffersBottomWrapper: styled.View`
    height: 40px;
    width: 100%;
    justify-content: center;
    align-items: center;
  `,
  MyCashBackBtn: styled.TouchableHighlight`
    height: 40px;
    width: 70%;
    background-color: ${GREEN.btn};
    justify-content: center;
    align-items: center;
    border-radius: 5px;
  `,
  DividerWhite: styled.View`
    height: 3px;
    width: 100%;
    background-color: ${WHITE.dark};
    marginVertical: 5px;
  `,
  MyCashBackText: styled.Text`
    fontSize: 16px;
    fontFamily: ${FONT.QuickSand.regular};
    color: ${WHITE.dark};
  `,
  ShowDetailWrapper: styled.View`
    height: ${SCREEN.height}px;
    background-color: ${GREEN.app};
    width: 100%;
  `,
  DetailScroll: styled.ScrollView`
    flex: 1;
    marginBottom: 23px;
  `,
  SimpleView: styled.View`
    flex: 1;
  `,
  DetailWhiteView: styled.View`
    height: 100%;
    width: 95%;
    alignSelf: center;
    background-color: ${WHITE.dark};
    borderRadius: 10px;
    paddingHorizontal: 35px;
  `,
  DetailIcon: styled.Image`
    height: 85px;
    width: 85px;
    borderRadius: 10px;
    marginTop: 35px;
    marginBottom: 33px;
    alignSelf: center;
    resizeMode: contain;
  `,
  QuickSand15Bold: styled.Text`
    fontSize: 15px;
    fontFamily: ${FONT.QuickSand.bold};
    alignSelf: center;
  `,
  QuickSand15Regular: styled.Text`
    fontSize: 15px;
    fontFamily: ${FONT.QuickSand.regular};
    alignSelf: center;
  `,
  Muli14Regular: styled.Text`
    fontSize: 14px;
    fontFamily: ${FONT.Muli.regular};
  `,
  Divider: styled.View`
    background-color: ${BLACK.light};
    height: 1px;
    width: 100%;
    marginVertical: 10px;
    align-self: center;
  `,
  DetailBottomView: styled.View`
    justifyContent: center;
    width: 100%;
    marginTop: 10px;
    height: 50px;
    marginBottom: 40px;
  `,
  AppIconAbsolute: styled.Image`
    height: 30px;
    width: ${SCREEN.width / 2.5}px;
    position: absolute;
    right: 0;
    resizeMode: contain;
  `,
  ActivateBtn: styled.TouchableHighlight`
    height: 46px;
    width: ${SCREEN.width - 80}px;
    align-self: center;
    background-color: ${GREEN.btn};
    justify-content: center;
    align-items: center;
    position: absolute;
    bottom: 11px;
  `,
  OfferActivatedBtn: styled.Image`
    position: absolute;
    right: 41px;
    top: 34px;
    height: 36px;
    width: 36px;
    resizeMode: contain;
  `,
};

export default OffersStyles;
