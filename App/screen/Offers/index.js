import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Modal} from "react-native";

import * as Actions from '../../redux/actions';
import OffersStyles from './OffersStyles';
import HeaderWithTwoBtn from "../../component/HeaderWithTwoBtn";
import {BLUE, GREEN, RED} from "../../helper/Color";
import HorizontalListWithTitle from "../../component/HorizontalListWithTitle";
import {FEATURED, FONT, FoodGroceries, NewOffers} from "../../helper/Constant";
import CashbackOffersList from "../../component/CashbackOffersList";
import SearchModel from "../../component/SearchModel";


class Offers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTab: 0,
      selectedItem: {},
      showDetail: false,
      showMyOffers: false,
      showProfilePopUP: false,
      showSearchPopUp: false,
    }
  }
  
  renderCashBack() {
    return (
      <OffersStyles.SafeView>
        <OffersStyles.OffersTopWrapper>
          <OffersStyles.ScrollView showsVerticalScrollIndicator={false}>
            <HorizontalListWithTitle
              listTitle={'Featured'} list={FEATURED}
              listItemClick={(item) => this.setState({selectedItem: item, showDetail: true})}/>
            <HorizontalListWithTitle
              listTitle={'New Offers'} list={NewOffers}
              listItemClick={(item) => this.setState({selectedItem: item, showDetail: true})}/>
            <HorizontalListWithTitle
              listTitle={'Food & Groceries'} list={FoodGroceries}
              listItemClick={(item) => this.setState({selectedItem: item, showDetail: true})}/>
            <HorizontalListWithTitle
              listTitle={'Electronics'} list={FoodGroceries}
              listItemClick={(item) => this.setState({selectedItem: item, showDetail: true})}/>
          </OffersStyles.ScrollView>
        </OffersStyles.OffersTopWrapper>
        <OffersStyles.OffersBottomWrapper>
          <OffersStyles.MyCashBackBtn underlayColor={GREEN.btn} onPress={() => this.setState({showMyOffers: true})}>
            <OffersStyles.MyCashBackText>
              My Cashback Offers
            </OffersStyles.MyCashBackText>
          </OffersStyles.MyCashBackBtn>
        </OffersStyles.OffersBottomWrapper>
      </OffersStyles.SafeView>
    )
  }
  
  renderPromos() {
    return (
      <OffersStyles.ScrollView showsVerticalScrollIndicator={false}>
        
        <OffersStyles.ScrollView showsVerticalScrollIndicator={false}>
          <HorizontalListWithTitle
            listTitle={'Featured'} list={FEATURED}
            listItemClick={(item) => this.setState({selectedItem: item, showDetail: true})}/>
          <HorizontalListWithTitle
            listTitle={'New Offers'} list={NewOffers}
            listItemClick={(item) => this.setState({selectedItem: item, showDetail: true})}/>
          <HorizontalListWithTitle
            listTitle={'Food & Groceries'} list={FoodGroceries}
            listItemClick={(item) => this.setState({selectedItem: item, showDetail: true})}/>
          <HorizontalListWithTitle
            listTitle={'Electronics'} list={FoodGroceries}
            listItemClick={(item) => this.setState({selectedItem: item, showDetail: true})}/>
        </OffersStyles.ScrollView>
      </OffersStyles.ScrollView>
    )
  }
  
  renderMyOffers() {
    return (
      <OffersStyles.SimpleView>
        <CashbackOffersList
          listTitle={'Used Cashback Offers'} list={NewOffers}
          listHeaderComponent={<OffersStyles.SimpleView>
            <HorizontalListWithTitle
              listTitle={'Activated Offers'} list={FEATURED}
              listItemClick={(item) => this.setState({selectedItem: item})}/>
            <OffersStyles.DividerWhite />
            <HorizontalListWithTitle
              listTitle={'Expired Offers'} list={NewOffers}
              listDetail={"These deals have expired but keep an eye out for new deals"}
              listItemClick={(item) => this.setState({selectedItem: item})}/>
            <OffersStyles.DividerWhite />
          </OffersStyles.SimpleView>}
          listItemClick={(item) => this.setState({selectedItem: item})}/>

      </OffersStyles.SimpleView>
    )
  }
  
  render() {
    return (
      <OffersStyles.WrapperViewVertical>
        <HeaderWithTwoBtn
          title={"My Offers"}
          rightIcon={require("../../assets/Interests/cheerfulDog.png")}
          rightPress={() => this.props.navigation.navigate('Profile')}
          leftIcon={require('../../assets/Icons/36px/WithDarkBackground/searchBackBlack.png')}
          leftPress={() => this.setState({showSearchPopUp: true})}
        />
        <OffersStyles.SafeView>
          <OffersStyles.RowView>
            <OffersStyles.TopTabWrapper
              style={{borderBottomWidth: this.state.selectedTab === 0 ? 4 : 0}}>
              <OffersStyles.TopTabInnerWrapper
                onPress={() => this.setState({selectedTab: 0})}
                underlayColor={GREEN.btn}
                style={{backgroundColor: this.state.selectedTab === 0 ? GREEN.btn : GREEN.app}}>
                <OffersStyles.TabText>
                  CASHBACK
                </OffersStyles.TabText>
              </OffersStyles.TopTabInnerWrapper>
            </OffersStyles.TopTabWrapper>
            <OffersStyles.TopTabWrapper
              style={{borderBottomWidth: this.state.selectedTab === 1 ? 4 : 0}}>
              <OffersStyles.TopTabInnerWrapper
                onPress={() => this.setState({selectedTab: 1})}
                underlayColor={GREEN.btn}
                style={{backgroundColor: this.state.selectedTab === 1 ? GREEN.btn : GREEN.app}}>
                <OffersStyles.TabText>
                  PROMOS
                </OffersStyles.TabText>
              </OffersStyles.TopTabInnerWrapper>
            </OffersStyles.TopTabWrapper>
          </OffersStyles.RowView>
          {this.state.selectedTab === 0 ? this.renderCashBack() : this.renderPromos()}
        </OffersStyles.SafeView>
        
        
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.showDetail}
          onRequestClose={() => {
            this.setState({showDetail: false});
          }}
        >
          <OffersStyles.ShowDetailWrapper>
            <HeaderWithTwoBtn
              title={"Offer Detail"}
              rightIcon={require("../../assets/Icons/36px/WithDarkBackground/shareIconBackBlack.png")}
              rightPress={() => alert("RightPress")}
              leftIcon={require('../../assets/Icons/46px/WithDarkBackground/backIconBlack.png')}
              leftPress={() => this.setState({showDetail: false})}
            />
            <OffersStyles.DetailScroll>
              <OffersStyles.DetailWhiteView>
                {this.state.offerActivated && <OffersStyles.OfferActivatedBtn
                  source={require('../../assets/Icons/36px/WithDarkBackground/saveGreenBackBlack.png')}/>}
                <OffersStyles.DetailIcon source={this.state.selectedItem.icon}/>
                <OffersStyles.QuickSand15Bold>
                  {this.state.selectedItem.off}
                </OffersStyles.QuickSand15Bold>
                <OffersStyles.QuickSand15Regular
                  style={{color: this.state.selectedItem.timeLeft < 5 ? RED.app : GREEN.app}}>
                  {this.state.selectedItem.timeLeft}{' days left'}
                </OffersStyles.QuickSand15Regular>
                <OffersStyles.QuickSand15Regular>
                  Expires on 12/09/2020
                </OffersStyles.QuickSand15Regular>
                <OffersStyles.Divider/>
                <OffersStyles.Muli14Regular>
                  Earn 5% cash back on all your Aveda.com purchases until a $15.00 cash back maximum is reached.
                  <OffersStyles.Muli14Regular style={{fontFamily: FONT.Muli.bold}}>Offer
                    is valid online only.</OffersStyles.Muli14Regular>{'\n'}Our mission at Aveda is to care for the
                  world we live in, from the products we
                  make to the ways in which we give back to society – beauty, wellness, environment, fashion and art,
                  since 1978. Our products are proudly people-tested and cruelty-free.{'\n'}{'\n'}
                  <OffersStyles.Muli14Regular style={{textDecorationLine: 'underline', color: BLUE.facebook}}>
                    Click here to order online now.
                  </OffersStyles.Muli14Regular>{'\n'}{'\n'}
                  Offer expires 12/9/2020.<OffersStyles.Muli14Regular style={{fontFamily: FONT.Muli.bold}}>Payment must
                  be made directly with the merchant. Offer not valid on Aveda
                  products purchased through other retailers.</OffersStyles.Muli14Regular> Offer not valid on
                  third-party delivery services. Payment
                  must be made on or before offer expiration date.
                </OffersStyles.Muli14Regular>
                <OffersStyles.Divider/>
                <OffersStyles.DetailBottomView>
                  <OffersStyles.Muli14Regular>
                    Offer: A007334612514
                  </OffersStyles.Muli14Regular>
                  <OffersStyles.AppIconAbsolute source={require('../../assets/Layout/logo.png')}/>
                </OffersStyles.DetailBottomView>
              </OffersStyles.DetailWhiteView>
            </OffersStyles.DetailScroll>
            <OffersStyles.ActivateBtn
              underlayColor={GREEN.btn}
              onPress={() => this.setState({offerActivated: !this.state.offerActivated})}>
              <OffersStyles.MyCashBackText>
                {this.state.offerActivated ? 'Remove Offer' : 'Activate Offer'}
              </OffersStyles.MyCashBackText>
            </OffersStyles.ActivateBtn>
          </OffersStyles.ShowDetailWrapper>
        </Modal>
  
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.showMyOffers}
          onRequestClose={() => {
            this.setState({showMyOffers: false});
          }}
        >
          <OffersStyles.ShowDetailWrapper>
            <HeaderWithTwoBtn
              title={"My Cashback Offers"}
              rightIcon={require("../../assets/Icons/36px/WithDarkBackground/shareIconBackBlack.png")}
              rightPress={() => alert("RightPress")}
              leftIcon={require('../../assets/Icons/46px/WithDarkBackground/backIconBlack.png')}
              leftPress={() => this.setState({showMyOffers: false})}
            />
              {this.renderMyOffers()}
          </OffersStyles.ShowDetailWrapper>
        </Modal>
        {this.state.showSearchPopUp && <SearchModel
          pressClose={() => this.setState({showSearchPopUp: false})}/>}
      </OffersStyles.WrapperViewVertical>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    userDetail: state.user.userDetail,
    userToken: state.user.userToken,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Offers);
