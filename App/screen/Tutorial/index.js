import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as Actions from '../../redux/actions';
import TutorialStyles from './TutorialStyles';
import HeaderWithTwoBtn from '../../component/HeaderWithTwoBtn';
import {BLACK} from "../../helper/Color";

class Tutorial extends Component {
  
  render() {
    return (
      <TutorialStyles.WrapperViewVertical>
        <HeaderWithTwoBtn
          leftIcon={require('../../assets/Icons/32px/WithDarkBackground/back.png')}
          leftPress={() => this.props.navigation.pop()}
          title={'Tutorials'}
          rightIcon={require('../../assets/Icons/32px/WithDarkBackground/share.png')}/>
        <TutorialStyles.SafeView>
            <TutorialStyles.WhiteWrapperView>
              <TutorialStyles.Muli15Regular>
                Which tour would you want to take today?
              </TutorialStyles.Muli15Regular>
              <TutorialStyles.RowView>
                <TutorialStyles.RowViewLeftIconWrapper>
                  <TutorialStyles.RowLeftIcon source={require('../../assets/OtherIcons/supportRead.png')} />
                </TutorialStyles.RowViewLeftIconWrapper>
                <TutorialStyles.RowRightView>
                  <TutorialStyles.Muli15Regular>
                    Helicopter overview
                  </TutorialStyles.Muli15Regular>
                </TutorialStyles.RowRightView>
              </TutorialStyles.RowView>
              <TutorialStyles.RowView>
                <TutorialStyles.RowViewLeftIconWrapper>
                  <TutorialStyles.RowLeftIcon source={require('../../assets/OtherIcons/supportRead.png')} />
                </TutorialStyles.RowViewLeftIconWrapper>
                <TutorialStyles.RowRightView>
                  <TutorialStyles.Muli15Regular>
                    Navigate through your receipts
                  </TutorialStyles.Muli15Regular>
                </TutorialStyles.RowRightView>
              </TutorialStyles.RowView>
              <TutorialStyles.RowView>
                <TutorialStyles.RowViewLeftIconWrapper>
                  <TutorialStyles.RowLeftIcon source={require('../../assets/OtherIcons/supportRead.png')} />
                </TutorialStyles.RowViewLeftIconWrapper>
                <TutorialStyles.RowRightView>
                  <TutorialStyles.QuickSand15Regular>
                    Manage your cards
                  </TutorialStyles.QuickSand15Regular>
                </TutorialStyles.RowRightView>
              </TutorialStyles.RowView>
            </TutorialStyles.WhiteWrapperView>
        </TutorialStyles.SafeView>
      </TutorialStyles.WrapperViewVertical>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    userDetail: state.user.userDetail,
    userToken: state.user.userToken,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Tutorial);
