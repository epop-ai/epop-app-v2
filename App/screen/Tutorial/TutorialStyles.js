/* eslint-disable prettier/prettier */
import styled from 'styled-components/native';
import {BLACK, GREEN, WHITE} from '../../helper/Color';
import {FONT, isIphoneXorAbove, SCREEN} from "../../helper/Constant";

const TutorialStyles = {
  WrapperViewVertical: styled.View`
    flex: 1;
    justifyContent: center;
    backgroundColor: ${GREEN.app};
    paddingBottom: ${isIphoneXorAbove ? 80 : 60}px ;
  `,
  SafeView: styled.SafeAreaView`
    flex: 1;
  `,
  Scroll: styled.ScrollView`
    flex: 1;
  `,
  WhiteWrapperView: styled.View`
    backgroundColor: ${WHITE.dark};
    borderRadius: 10px;
    paddingVertical: 23px;
    paddingHorizontal: 9px;
    width: ${SCREEN.width - 20}px;
    alignSelf: center;
    height: 100%;
  `,
  Muli15Regular: styled.Text`
    fontSize: 15px;
    fontFamily: ${FONT.Muli.regular};
    color: ${BLACK.app};
  `,
  QuickSand15Regular: styled.Text`
    fontSize: 15px;
    fontFamily: ${FONT.QuickSand.regular};
    color: ${BLACK.app};
  `,
  RowView: styled.View`
    flexDirection: row;
    alignItems: center;
    width: 100%;
    height: 60px;
    marginTop: 15px;
  `,
  RowViewLeftIconWrapper: styled.View`
    height: 100%;
    width: 50px;
    justifyContent: center;
    alignItems: center;
  `,
  RowLeftIcon: styled.Image`
    height: 20px;
    width: 20px;
    resizeMode: contain;
  `,
  RowRightView: styled.View`
    flex: 1
  `,
  
};

export default TutorialStyles;
