/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {Platform} from 'react-native';
import {connect} from 'react-redux';

import * as Actions from '../../redux/actions';
import SignInStyles from './SignInStyles';
import {GREEN} from '../../helper/Color';
import StartupStyles from "../Startup/StartupStyles";
import {SCREEN} from "../../helper/Constant";

class SignIn extends Component {
  render() {
    return (
      <SignInStyles.WrapperViewVertical>
        <SignInStyles.SafeView>
          <SignInStyles.WhiteWrapperView>
            <SignInStyles.WhiteInnerView />
            <SignInStyles.Icon
              source={require('../../assets/Layout/logo.png')}
            />
            <SignInStyles.AppDetailText>
              Your contactless digital receipts in one place!
            </SignInStyles.AppDetailText>
            <StartupStyles.BottomViewWrapper
              keyboardVerticalOffset={Platform.OS === "ios" ? SCREEN.width /30 : 0}
              behavior={Platform.OS === "ios" ? "padding" : "height"}
              enabled>
              <StartupStyles.AbsoluteImage source={require('../../assets/Layout/background.png')} />
              <StartupStyles.BottomViewWhite />
            <SignInStyles.BottomView>
              <SignInStyles.Scroll>
              <SignInStyles.TextInputWrapper>
                <SignInStyles.TextInputInner />
              </SignInStyles.TextInputWrapper>

              <SignInStyles.TextInputWrapper style={{marginTop: 13}}>
                <SignInStyles.TextInputInner />
              </SignInStyles.TextInputWrapper>
              <SignInStyles.ForgotText>Forgot password</SignInStyles.ForgotText>
              <SignInStyles.ForgotTextView>
                <SignInStyles.HaveAccountText>
                  Don’t have an account yet?{' '}
                  <SignInStyles.HaveAccountText onPress={() => this.props.navigation.pop()} style={{color: GREEN.app}}>
                    Sign Up Now
                  </SignInStyles.HaveAccountText>
                </SignInStyles.HaveAccountText>
              </SignInStyles.ForgotTextView>
              <SignInStyles.RowView>
                <SignInStyles.SignInBtn onPress={() => this.props.navigation.navigate('Main')} underlayColor={GREEN.app}>
                  <SignInStyles.BtnText>Sign In</SignInStyles.BtnText>
                </SignInStyles.SignInBtn>
                <SignInStyles.ScanBtn
                  underlayColor={GREEN.app}
                  onPress={() => this.props.navigation.navigate('InitialSetup')}>
                  <SignInStyles.BtnInnerImage
                    source={require('../../assets/Layout/face.png')}
                  />
                </SignInStyles.ScanBtn>
              </SignInStyles.RowView>

              <SignInStyles.RowView>
                <SignInStyles.Flex1View>
                  <SignInStyles.OrViewLineView />
                </SignInStyles.Flex1View>
                <SignInStyles.OrText>OR SIGN IN WITH</SignInStyles.OrText>
                <SignInStyles.Flex1View>
                  <SignInStyles.OrViewLineView />
                </SignInStyles.Flex1View>
              </SignInStyles.RowView>

              <SignInStyles.RowView>
                <SignInStyles.SocialBtn>
                  <SignInStyles.BtnInnerImage
                    source={require('../../assets/Layout/facebook.png')}
                  />
                </SignInStyles.SocialBtn>
                <SignInStyles.SocialBtn style={{marginLeft: 17}}>
                  <SignInStyles.BtnInnerImage
                    source={require('../../assets/Layout/apple.png')}
                  />
                </SignInStyles.SocialBtn>
                <SignInStyles.SocialBtn style={{marginLeft: 17}}>
                  <SignInStyles.BtnInnerImage
                    source={require('../../assets/Layout/google.png')}
                  />
                </SignInStyles.SocialBtn>
              </SignInStyles.RowView>
              </SignInStyles.Scroll>
            </SignInStyles.BottomView>
            </StartupStyles.BottomViewWrapper>
            </SignInStyles.WhiteWrapperView>
        </SignInStyles.SafeView>
      </SignInStyles.WrapperViewVertical>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    userDetail: state.user.userDetail,
    userToken: state.user.userToken,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
