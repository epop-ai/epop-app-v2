/* eslint-disable react-native/no-inline-styles */
import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Platform, Alert, Keyboard } from "react-native";

import * as Actions from "../../redux/actions";
import SignUpPasswordSetStyles from "./SignUpPasswordSetStyles";
import { BLACK, GREEN, WHITE } from "../../helper/Color";
import DateOfBirth from "../../component/DateOfBirth";
import AllSet from "../../component/AllSet";

class SignUpPasswordSet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      DOB: "",
      pass: "",
      re_pass: "",
      DOBPopUp: false,
      rePassSecureType: true,
      passSecureType: true,
      setAllPopUp: false,
    };
  }

  render() {
    const rAllFilled =
      this.state.DOB.length > 0 &&
      this.state.pass.trim().length > 0 &&
      this.state.re_pass.trim().length > 0;
    return (
      <SignUpPasswordSetStyles.WrapperViewVertical>
        <SignUpPasswordSetStyles.SafeView>
          <SignUpPasswordSetStyles.Scroll>
            <SignUpPasswordSetStyles.Muli20Bold>
              Goodbye paper receipts!
            </SignUpPasswordSetStyles.Muli20Bold>
            <SignUpPasswordSetStyles.AppLogo
              source={require("../../assets/Layout/logo.png")}
            />
            <SignUpPasswordSetStyles.BottomViewWrapper>
              <SignUpPasswordSetStyles.LightBlackWrapper>
                <SignUpPasswordSetStyles.PhoneInputWrapper>
                  <SignUpPasswordSetStyles.DobWrapper
                    underlayColor={WHITE.dark}
                    onPress={() => {
                      Keyboard.dismiss();
                      this.setState({ DOBPopUp: true });
                    }}
                  >
                    <SignUpPasswordSetStyles.DobText
                      style={{
                        color:
                          this.state.DOB !== ""
                            ? BLACK.dark
                            : BLACK.borderLight,
                      }}
                    >
                      {this.state.DOB !== "" ? this.state.DOB : "Date of birth"}
                    </SignUpPasswordSetStyles.DobText>
                  </SignUpPasswordSetStyles.DobWrapper>
                </SignUpPasswordSetStyles.PhoneInputWrapper>
                <SignUpPasswordSetStyles.PhoneInputWrapper>
                  <SignUpPasswordSetStyles.InputText
                    placeholder={"Password"}
                    placeholderTextColor={BLACK.borderLight}
                    value={this.state.pass}
                    onChangeText={(pass) => this.setState({ pass })}
                    secureTextEntry={this.state.passSecureType}
                  />
                  <SignUpPasswordSetStyles.AbsoluteRightIcon
                    underlayColor={WHITE.dark}
                    onPress={() =>
                      this.setState({
                        passSecureType: !this.state.passSecureType,
                      })
                    }
                  >
                    <SignUpPasswordSetStyles.AbsoluteIconInner
                      source={require("../../assets/Icons/32px/NoBackground/editBlack.png")}
                    />
                  </SignUpPasswordSetStyles.AbsoluteRightIcon>
                </SignUpPasswordSetStyles.PhoneInputWrapper>
                <SignUpPasswordSetStyles.PhoneInputWrapper>
                  <SignUpPasswordSetStyles.InputText
                    placeholder={"Re-enter Password"}
                    placeholderTextColor={BLACK.borderLight}
                    value={this.state.re_pass}
                    onChangeText={(re_pass) => this.setState({ re_pass })}
                    secureTextEntry={this.state.rePassSecureType}
                  />
                  <SignUpPasswordSetStyles.AbsoluteRightIcon
                    underlayColor={WHITE.dark}
                    onPress={() =>
                      this.setState({
                        rePassSecureType: !this.state.rePassSecureType,
                      })
                    }
                  >
                    <SignUpPasswordSetStyles.AbsoluteIconInner
                      source={require("../../assets/Icons/32px/NoBackground/editBlack.png")}
                    />
                  </SignUpPasswordSetStyles.AbsoluteRightIcon>
                </SignUpPasswordSetStyles.PhoneInputWrapper>
                <SignUpPasswordSetStyles.FinishBtn
                  style={{
                    backgroundColor: rAllFilled ? GREEN.app : BLACK.borderLight,
                  }}
                  onPress={() => {
                    if (rAllFilled) {
                      this.setState({ setAllPopUp: true });
                      Keyboard.dismiss();
                    } else {
                      Alert.alert("Please fill all data");
                    }
                  }}
                  underlayColor={BLACK.appLight}
                >
                  <SignUpPasswordSetStyles.FinishBtnText>
                    Finish
                  </SignUpPasswordSetStyles.FinishBtnText>
                </SignUpPasswordSetStyles.FinishBtn>
              </SignUpPasswordSetStyles.LightBlackWrapper>
            </SignUpPasswordSetStyles.BottomViewWrapper>
          </SignUpPasswordSetStyles.Scroll>
        </SignUpPasswordSetStyles.SafeView>
        {this.state.DOBPopUp && (
          <DateOfBirth
            savePress={(date) => this.setState({ DOB: date, DOBPopUp: false })}
            cancelPress={() => this.setState({ DOBPopUp: false })}
          />
        )}
        {this.state.setAllPopUp && (
          <AllSet
            singInCall={() =>
              this.props.navigation.navigate("SignIn", {
                email: "test@gmail.com",
                password: this.state.pass,
              })
            }
          />
        )}
      </SignUpPasswordSetStyles.WrapperViewVertical>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    userDetail: state.user.userDetail,
    userToken: state.user.userToken,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUpPasswordSet);
