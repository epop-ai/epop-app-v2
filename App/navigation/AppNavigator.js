import {Animated, Easing} from 'react-native';
import {createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import {createBottomTabNavigator} from 'react-navigation-tabs';

import CardAdd from '../screen/CardAdd';
import Cards from '../screen/Cards';
import InitialSetup from '../screen/InitialSetup';
import Offers from '../screen/Offers';
import Profile from '../screen/Profile';
import SignIn from '../screen/SignIn';
import SignUp from '../screen/SignUp';
import SignUpCodeVerification from '../screen/SignUpCodeVerification';
import SignUpPasswordSet from '../screen/SignUpPasswordSet';
import SignUpUserDetail from '../screen/SignUpUserDetail';
import Splash from '../screen/Splash';
import Startup from '../screen/Startup';
import Supports from '../screen/Supports';
import Tutorial from '../screen/Tutorial';
import Receipts from '../screen/Receipts';
import Reports from '../screen/Reports';

import {GREEN} from '../helper/Color';

import BottomNavContainer from './BottomNav';

// https://github.com/react-community/react-navigation/issues/1254
const noTransitionConfig = () => ({
  transitionSpec: {
    duration: 0.1,
    timing: Animated.timing,
    easing: Easing.circle,
  },
});

const TabStackOne = createStackNavigator(
  {
    Cards: {screen: Cards},
  },
  {
    headerMode: 'none',
    cardStyle: {
      backgroundColor: GREEN.app,
      opacity: 1,
    },
  },
);
const TabStackTwo = createStackNavigator(
  {
    Receipts: {screen: Receipts},
  },
  {
    headerMode: 'none',
    cardStyle: {
      backgroundColor: GREEN.app,
      opacity: 1,
    },
  },
);

const TabStackThree = createStackNavigator(
  {
    Reports: {screen: Reports},
  },
  {
    headerMode: 'none',
    cardStyle: {
      backgroundColor: GREEN.app,
      opacity: 1,
    },
  },
);

const TabStackFour = createStackNavigator(
  {
    Offers: {screen: Offers},
  },
  {
    headerMode: 'none',
    cardStyle: {
      backgroundColor: GREEN.app,
      opacity: 1,
    },
  },
);

const TabStackFive = createStackNavigator(
  {
    Supports: {screen: Supports},
  },
  {
    headerMode: 'none',
    cardStyle: {
      backgroundColor: GREEN.app,
      opacity: 1,
    },
  },
);

//Tab Navigation
const TabNavigator = createBottomTabNavigator(
  {
    Receipts: {screen: TabStackTwo},
    Cards: {screen: TabStackOne},
    Reports: {screen: TabStackThree},
    Offers: {screen: TabStackFour},
    Supports: {screen: TabStackFive},
  },
  {
    tabBarComponent: BottomNavContainer,
    tabBarPosition: 'bottom',
    swipeEnabled: false,
    animationEnabled: false,
  },
);

const HomeStack = createStackNavigator({
  Main: {
    screen: TabNavigator,
    navigationOptions: {headerShown: false, gestureEnabled: true},
  },
  CardAdd: {
    screen: CardAdd,
    navigationOptions: {headerShown: false, gestureEnabled: true},
  },
  Tutorial: {
    screen: Tutorial,
    navigationOptions: {headerShown: false, gestureEnabled: true},
  },
  Profile: {
    screen: Profile,
    navigationOptions: {headerShown: false, gestureEnabled: true},
  },
});

// login stack
const LoginStack = createStackNavigator({
  Startup: {
    screen: Startup,
    navigationOptions: {headerShown: false, gestureEnabled: true},
  },
  SignIn: {
    screen: SignIn,
    navigationOptions: {headerShown: false, gestureEnabled: true},
  },
  SignUp: {
    screen: SignUp,
    navigationOptions: {headerShown: false, gestureEnabled: true},
  },
  SignUpCodeVerification: {
    screen: SignUpCodeVerification,
    navigationOptions: {headerShown: false, gestureEnabled: true},
  },
  SignUpUserDetail: {
    screen: SignUpUserDetail,
    navigationOptions: {headerShown: false, gestureEnabled: true},
  },
  SignUpPasswordSet: {
    screen: SignUpPasswordSet,
    navigationOptions: {headerShown: false, gestureEnabled: true},
  },
  InitialSetup: {
    screen: InitialSetup,
    navigationOptions: {headerShown: false, gestureEnabled: true},
  },
});

const MainApp = createStackNavigator({
  Splash: {
    screen: Splash,
    navigationOptions: {
      headerShown: false,
      gestureEnabled: false,
    },
  },
});

// eslint-disable-next-line no-undef
let SwitchNavigator;
export default SwitchNavigator = createSwitchNavigator(
  {
    App: MainApp,
    Auth: LoginStack,
    Main: HomeStack,
  },
  {
    initialRouteName: 'App',
    transitionConfig: noTransitionConfig,
  },
);
