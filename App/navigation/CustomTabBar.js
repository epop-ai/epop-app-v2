/* eslint-disable react-native/no-inline-styles */
import * as React from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
export function CustomTabBar(props) {
  const navigateToFirstScreen = () => {
    props.navigation.navigate('Receipts');
  };

  const navigateToSecondScreen = () => {
    props.navigation.navigate('Card');
  };

  const navigateToThirdScreen = () => {
    props.navigation.navigate('Reports');
  };

  const navigateToFourthScreen = () => {
    props.navigation.navigate('Offers');
  };

  const navigateToFifthScreen = () => {
    props.navigation.navigate('Supports');
  };

  return (
    <View style={styles.TabBarMainContainer}>
      <TouchableOpacity
        onPress={navigateToFirstScreen}
        activeOpacity={0.6}
        style={styles.button}>
        <Text style={styles.TextStyle}> SCREEN 1 </Text>
      </TouchableOpacity>

      <View style={{height: 50, backgroundColor: '#fff', width: 2}} />

      <TouchableOpacity
        onPress={navigateToSecondScreen}
        activeOpacity={0.6}
        style={styles.button}>
        <Text style={styles.TextStyle}> SCREEN 2 </Text>
      </TouchableOpacity>

      <View style={{height: 50, backgroundColor: '#fff', width: 2}} />

      <TouchableOpacity
        onPress={navigateToThirdScreen}
        activeOpacity={0.6}
        style={styles.button}>
        <Text style={styles.TextStyle}> SCREEN 3 </Text>

        <View style={{height: 50, backgroundColor: '#fff', width: 2}} />

        <TouchableOpacity
          onPress={navigateToFourthScreen}
          activeOpacity={0.6}
          style={styles.button}>
          <Text style={styles.TextStyle}> SCREEN 3 </Text>
        </TouchableOpacity>

        <View style={{height: 50, backgroundColor: '#fff', width: 2}} />

        <TouchableOpacity
          onPress={navigateToFifthScreen}
          activeOpacity={0.6}
          style={styles.button}>
          <Text style={styles.TextStyle}> SCREEN 3 </Text>
        </TouchableOpacity>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  TabBarMainContainer: {
    justifyContent: 'space-around',
    height: 50,
    flexDirection: 'row',
    width: '100%',
  },

  button: {
    height: 50,
    paddingTop: 5,
    paddingBottom: 5,
    backgroundColor: '#00BCD4',
    justifyContent: 'center',
    alignItems: 'center',
    flexGrow: 1,
  },

  TextStyle: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 20,
  },
});
