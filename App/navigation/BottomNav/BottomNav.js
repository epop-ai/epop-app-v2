/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {TouchableOpacity} from 'react-native';
import BottomNavStyle from './BottomNavStyle';
import {GREEN, BLUE, BLACK, RED} from '../../helper/Color';
import Icon1 from 'react-native-vector-icons/AntDesign';
import Icon2 from 'react-native-vector-icons/Feather';
import Icon3 from 'react-native-vector-icons/FontAwesome';

const BottomNav = ({
  navigation,
  checkPreviewState,
  index,
  isIphoneXorAbove,
  opacityActually,
}) => (
  <BottomNavStyle.WrapperView
    style={{height: isIphoneXorAbove ? 80 : 60, opacity: opacityActually}}>
    <BottomNavStyle.LinkWrapper
      style={{marginBottom: isIphoneXorAbove ? 15 : 0}}>
      <TouchableOpacity
        style={{minWidth: 40, alignSelf: 'center'}}
        onPress={() => {
          if (opacityActually === 1) {
            navigation('Receipts', 0);
          }
        }}>
        <BottomNavStyle.IconWrapperView>
          {index === 0 ? (
            <BottomNavStyle.Icon
              source={require('../../assets/Icons/46px/WithDarkBackground/receiptsIconBlack.png')}
            />
          ) : (
            <BottomNavStyle.Icon
              source={require('../../assets/Icons/46px/NoBackground/receiptsIconWhite.png')}
            />
          )}
        </BottomNavStyle.IconWrapperView>
      </TouchableOpacity>
    </BottomNavStyle.LinkWrapper>
    <BottomNavStyle.LinkWrapper
      style={{marginBottom: isIphoneXorAbove ? 15 : 0}}>
      <TouchableOpacity
        style={{minWidth: 40, alignSelf: 'center'}}
        onPress={() => {
          if (opacityActually === 1) {
            navigation('Cards', 1);
          }
        }}>
        <BottomNavStyle.IconWrapperView>
          {index === 1 ? (
            <BottomNavStyle.Icon
              source={require('../../assets/Icons/46px/WithDarkBackground/cardsIconBlack.png')}
            />
          ) : (
            <BottomNavStyle.Icon
              source={require('../../assets/Icons/46px/NoBackground/cardsIconWhite.png')}
            />
          )}
        </BottomNavStyle.IconWrapperView>
      </TouchableOpacity>
    </BottomNavStyle.LinkWrapper>
    <BottomNavStyle.LinkWrapper
      style={{marginBottom: isIphoneXorAbove ? 15 : 0}}>
      <TouchableOpacity
        style={{minWidth: 40, alignSelf: 'center'}}
        onPress={() => {
          if (opacityActually === 1) {
            navigation('Reports', 2);
          }
        }}>
        <BottomNavStyle.IconWrapperView>
          {index === 2 ? (
            <BottomNavStyle.Icon
              source={require('../../assets/Icons/46px/WithDarkBackground/reportIconBlack.png')}
            />
          ) : (
            <BottomNavStyle.Icon
              source={require('../../assets/Icons/46px/NoBackground/reportsIconWhite.png')}
            />
          )}
        </BottomNavStyle.IconWrapperView>
      </TouchableOpacity>
    </BottomNavStyle.LinkWrapper>
    <BottomNavStyle.LinkWrapper
      style={{marginBottom: isIphoneXorAbove ? 15 : 0}}>
      <TouchableOpacity
        style={{minWidth: 40, alignSelf: 'center'}}
        onPress={() => {
          if (opacityActually === 1) {
            navigation('Offers', 3);
          }
        }}>
        <BottomNavStyle.IconWrapperView>
          {index === 3 ? (
            <BottomNavStyle.Icon
              source={require('../../assets/Icons/46px/WithDarkBackground/offersIconBlack.png')}
            />
          ) : (
            <BottomNavStyle.Icon
              source={require('../../assets/Icons/46px/NoBackground/offersIconWhite.png')}
            />
          )}
        </BottomNavStyle.IconWrapperView>
      </TouchableOpacity>
    </BottomNavStyle.LinkWrapper>
    <BottomNavStyle.LinkWrapper
      style={{marginBottom: isIphoneXorAbove ? 15 : 0}}>
      <TouchableOpacity
        style={{minWidth: 40, alignSelf: 'center'}}
        onPress={() => {
          if (opacityActually === 1) {
            navigation('Supports', 4);
          }
        }}>
        <BottomNavStyle.IconWrapperView>
          {index === 4 ? (
            <BottomNavStyle.Icon
              source={require('../../assets/Icons/46px/WithDarkBackground/supportIconBlack.png')}
            />
          ) : (
            <BottomNavStyle.Icon
              source={require('../../assets/Icons/46px/NoBackground/supportIconWite.png')}
            />
          )}
        </BottomNavStyle.IconWrapperView>
      </TouchableOpacity>
    </BottomNavStyle.LinkWrapper>
  </BottomNavStyle.WrapperView>
);

export default BottomNav;
