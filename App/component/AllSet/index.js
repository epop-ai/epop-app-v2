/* eslint-disable react-native/no-inline-styles */
import React, { useState } from "react";
import {
  Image,
  Text,
  TouchableHighlight,
  View,
  TouchableOpacity,
} from "react-native";
import { FONT, isIphoneXorAbove, SCREEN } from "../../helper/Constant";
import { BLACK, GREEN, WHITE } from "../../helper/Color";

function AllSet({ singInCall }) {
  return (
    <TouchableOpacity
      activeOpacity={1}
      style={{
        height: SCREEN.height,
        alignItems: "center",
        width: SCREEN.width,
        alignSelf: "center",
        backgroundColor: BLACK.transParent,
        position: "absolute",
        justifyContent: "center",
      }}
    >
      <View
        style={{
          minHeight: 250,
          width: "80%",
          backgroundColor: GREEN.app,
          alignSelf: "center",
          alignItems: "center",
          paddingVertical: 40,
        }}
      >
        <Image
          style={{
            height: 40,
            width: 40,
            resizeMode: "contain",
            marginTop: 26,
            alignSelf: "center",
          }}
          source={require("../../assets/Icons/36px/WithDarkBackground/saveGreenBackBlack.png")}
        />
        <Text
          style={{
            fontSize: 24,
            fontFamily: FONT.QuickSand.bold,
            color: WHITE.dark,
            marginTop: 20,
          }}
        >
          Great, you are all set!
        </Text>
        <Text
          style={{
            fontSize: 18,
            fontFamily: FONT.QuickSand.regular,
            color: WHITE.dark,
            marginTop: 10,
            textAlign: "center",
          }}
        >
          Just check your email to confirm your account
        </Text>
        <TouchableHighlight
          onPress={singInCall}
          style={{
            height: 46,
            width: "70%",
            backgroundColor: GREEN.btn,
            borderRadius: 5,
            marginTop: 13,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Text
            style={{
              fontFamily: FONT.Muli.bold,
              fontSize: 20,
              color: WHITE.dark,
            }}
          >
            Sign In
          </Text>
        </TouchableHighlight>
      </View>
    </TouchableOpacity>
  );
}

export default AllSet;
