/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {View, Image, TouchableHighlight, Text, Platform} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import DatePicker from 'react-native-datepicker';

import {BLACK, GREEN, WHITE} from '../../helper/Color';
import {FONT, SCREEN} from '../../helper/Constant';

function SetCalendarPopUp({cancelPress, openEndDatePress, openStartDatePress}) {
  const [startDate, setStartDate] = useState(new Date(1598051730000));
  const [endDate, setEndDate] = useState(new Date(1598051730000));
  
  const onChangeStart = (event, selectedDate) => {
    const currentDate = selectedDate || startDate;
    setStartDate(currentDate);
  };
  
  const onChangeEnd = (event, selectedDate) => {
    const currentDate = selectedDate || endDate;
    setEndDate(currentDate);
  };

  
  return (
    <TouchableHighlight
      activeOpacity={1}
      style={{
        height: '100%',
        width: '100%',
        position: 'absolute',
        backgroundColor: BLACK.transParent,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <View
        style={{
          width: '85%',
          backgroundColor: GREEN.app,
          paddingVertical: 5,
          paddingHorizontal: 5,
        }}>
        <Text
          style={{fontSize: 16, fontFamily: FONT.Muli.bold, color: WHITE.dark, alignSelf: 'center', marginTop: 16,}}>
          Start Date
        </Text>
        {Platform.OS === 'android' ?
          <View style={{alignSelf: 'center'}}>
            <DatePicker
              style={{width: SCREEN.width - 90}}
              date={startDate} //initial date from state
              mode="date" //The enum of date, datetime and time
              placeholder="select date"
              format="DD-MM-YYYY"
              minDate="01-01-2016"
              androidMode={'calendar'}
              maxDate={new Date().getDate()}
              confirmBtnText="Confirm"
              showIcon={false}
              cancelBtnText="Cancel"
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  left: 0,
                  top: 4,
                  marginLeft: 0,
                },
                dateInput: {
                  backgroundColor: WHITE.app,
                  borderRadius: 10,
                },
                dateText:{
                  fontSize: 15,
                  fontFamily: FONT.Muli.bold,
                }
              }}
              onDateChange={(date) => {
                setStartDate(date);
              }}
            />
          </View>
          :
          <View style={{backgroundColor: WHITE.dark, height: 140, width: '80%', alignSelf: 'center', borderRadius: 15}}>
            <DateTimePicker
              testID="date"
              style={{height: 140}}
              value={startDate}
              mode="date"
              inline={true}
              is24Hour={true}
              display="spinner"
              onChange={onChangeStart}
            />
          </View>}
        <Text
          style={{
            fontSize: 16, fontFamily: FONT.Muli.bold,
            color: WHITE.dark, alignSelf: 'center', marginTop: 16
          }}>
          End Date
        </Text>
        {Platform.OS === 'android' ?
          <View style={{alignSelf: 'center'}}>
            <DatePicker
              style={{width: SCREEN.width - 90}}
              date={endDate} //initial date from state
              mode="date" //The enum of date, datetime and time
              placeholder="select date"
              format="DD-MM-YYYY"
              minDate="01-01-2016"
              display="calendar"
              maxDate={new Date().getDate()}
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              showIcon={false}
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  left: 0,
                  top: 4,
                  marginLeft: 0,
                },
                dateInput: {
                  backgroundColor: WHITE.app,
                  borderRadius: 10,
                },
                dateText:{
                  fontSize: 15,
                  fontFamily: FONT.Muli.bold,
                }
              }}
              onDateChange={(date) => {
                setEndDate(date);
              }}
            />
          </View>
          :
          <View style={{backgroundColor: WHITE.dark, height: 140, width: '80%', alignSelf: 'center', borderRadius: 15}}>
            <DateTimePicker
              testID="date"
              style={{height: 140}}
              value={endDate}
              mode="date"
              inline={true}
              is24Hour={true}
              display="spinner"
              onChange={onChangeEnd}
            />
          </View>}
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <View style={{flex: 0.5, marginLeft: 10}}>
            <TouchableHighlight
              style={{height: 40, width: 40}}
              onPress={cancelPress}
              underlayColor={GREEN.app}>
              <Image
                style={{height: '100%', width: '100%', resizeMode: 'contain'}}
                source={require('../../assets/Icons/36px/NoBackground/closeInvertedIconBlack.png')}
              />
            </TouchableHighlight>
          </View>
          <View
            style={{
              flex: 0.5,
              alignItems: 'flex-end',
              marginRight: 10,
            }}>
            <TouchableHighlight
              style={{height: 40, width: 40}}
              onPress={cancelPress}
              underlayColor={GREEN.app}>
              <Image
                style={{height: '100%', width: '100%', resizeMode: 'contain'}}
                source={require('../../assets/Icons/36px/NoBackground/saveIconBlack.png')}
              />
            </TouchableHighlight>
          </View>
        </View>
      </View>
    </TouchableHighlight>
  );
}

export default SetCalendarPopUp;
