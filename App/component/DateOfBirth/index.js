/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {Image, Text, TouchableHighlight, View, TouchableOpacity, Platform} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import {FONT, SCREEN} from '../../helper/Constant';
import {BLACK, GREEN, WHITE} from "../../helper/Color";
import DatePicker from "react-native-datepicker";

function DateOfBirth({cancelPress, savePress,}) {
  const [DOB, setDOB] = useState(new Date());
  
  
  function onChange(event, date) {
    if (date) {
      setDOB(date)
    }
  }
  
  return (
    <TouchableOpacity
      activeOpacity={1}
      style={{
        height: SCREEN.height,
        alignItems: 'center',
        width: SCREEN.width,
        alignSelf: 'center',
        backgroundColor: BLACK.transParent,
        position: 'absolute',
        justifyContent: 'center',
      }}>
      <View style={{minHeight: 100, width: '80%', backgroundColor: GREEN.app, alignSelf: 'center'}}>
        <Text style={{
          color: WHITE.dark,
          fontSize: 16,
          fontFamily: FONT.Muli.bold,
          alignSelf: 'center',
          marginVertical: 12
        }}>Date of birth</Text>
        {Platform.OS === 'android' ?
          <View style={{alignSelf: 'center'}}>
            <DatePicker
              style={{width: SCREEN.width  - 80, marginTop: 10}}
              date={DOB} //initial date from state
              mode="date" //The enum of date, datetime and time
              placeholder="select date"
              format="DD-MM-YYYY"
              minDate="01-01-2016"
              display="calendar"
              showIcon={false}
              maxDate={new Date().getDate()}
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  left: 0,
                  top: 4,
                  marginLeft: 0,
                },
                dateInput: {
                  backgroundColor: WHITE.app,
                  borderRadius: 10,
                },
                dateText: {
                  fontSize: 15,
                  fontFamily: FONT.Muli.bold,
                }
              }}
              onDateChange={(date) => {
                setDOB(date);
              }}
            />
          </View>
          :
          <View style={{height: 160, width: '80%', borderRadius: 15, backgroundColor: WHITE.dark, alignSelf: 'center'}}>
            <DateTimePicker
              style={{height: 160}}
              testID="dateTimePicker"
              value={DOB}
              mode={'date'}
              display="spinner"
              is24Hour={true}
              onChange={onChange}
            />
          </View>}
        <View style={{width: '80%', alignSelf: 'center', flexDirection: 'row', height: 60}}>
          <View style={{flex: 1}}>
            <TouchableHighlight style={{height: '100%', width: 25, justifyContent: 'center'}} onPress={cancelPress}
                                underlayColor={GREEN.app}>
              <Image style={{height: 40, width: 40, resizeMode: 'contain'}}
                     source={require('../../assets/Icons/36px/NoBackground/closeInvertedIconBlack.png')}/>
            </TouchableHighlight>
          </View>
          <View style={{flex: 1}}>
            <View style={{flex: 1, alignItems: 'flex-end'}}>
              <TouchableHighlight style={{height: '100%', width: 25, justifyContent: 'center'}}
                                  onPress={() => Platform.OS === 'android' ? savePress(DOB) : savePress(DOB.toDateString())} underlayColor={GREEN.app}>
                <Image style={{height: 40, width: 40, resizeMode: 'contain'}}
                       source={require('../../assets/Icons/36px/NoBackground/saveIconBlack.png')}/>
              </TouchableHighlight>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

export default DateOfBirth;
