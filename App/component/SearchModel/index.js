/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {FlatList, Text, TouchableHighlight, View, TouchableOpacity, SafeAreaView} from 'react-native';
import {FONT, isIphoneXorAbove, SCREEN} from '../../helper/Constant';
import {BLACK, GREEN, WHITE} from "../../helper/Color";
import HeaderWithSearch from "../HeaderWithSearch";

function SearchModel({pressClose}) {
  const [searchList] = useState(['Amazon', 'Uber', "Macy's"]);
  const [searchText, setSearchText] = useState('');
  const [selectedIndex, setSelectedIndex] = useState(0);
  
  return (
    <TouchableOpacity
      activeOpacity={1}
      style={{
        height: isIphoneXorAbove ? SCREEN.height - 80 : SCREEN.height - 60,
        width: SCREEN.width,
        paddingHorizontal: 16,
        alignSelf: 'center',
        backgroundColor: GREEN.app,
        position: 'absolute',
      }}>
      
      <HeaderWithSearch
        rightIcon={require("../../assets/Icons/36px/WithDarkBackground/backBoxBlack.png")}
        searchValue={searchText}
        rightPress={pressClose}
        searchChange={(text) => setSearchText(text)}
      />
      <SafeAreaView style={{flex: 1}}>
        {searchText.trim().length < 1 &&
        <Text style={{fontFamily: FONT.Muli.bold, fontSize: 16, color: WHITE.app}}>
          Recent Search
        </Text>}
        {searchText.trim().length < 1 ? <FlatList
          keyExtractor={(item, index) => index.toString()}
          data={searchList}
          renderItem={(item) =>
            <TouchableOpacity
              activeOpacity={1}
              onPress={() => setSearchText(item.item)}
              style={{
                height: 45,
                width: '100%',
                paddingHorizontal: 10,
                justifyContent: 'center',
                borderBottomWidth: 0.3,
                borderBottomColor: WHITE.cream
              }}>
              <Text style={{fontSize: 16, fontFamily: FONT.Muli.regular, color: WHITE.app}}>
                {item.item}
              </Text>
            </TouchableOpacity>}/> : <View style={{flex: 1}}>
          <View style={{
            height: 40,
            width: SCREEN.width - 100,
            borderRadius: 20,
            alignSelf: 'center',
            backgroundColor: BLACK.app,
            flexDirection: 'row',
          }}>
            <TouchableHighlight
              onPress={() => setSelectedIndex(0)}
              underlayColor={selectedIndex === 0 ? GREEN.btn : BLACK.app}
              style={{
                flex: 1,
                borderWidth: 2,
                borderRadius: 20,
                borderColor: BLACK.app,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: selectedIndex === 0 ? GREEN.btn : BLACK.app,
              }}>
              <Text style={{fontSize: 16, color: WHITE.app, fontFamily: FONT.Muli.bold}}>OLDEST</Text>
            </TouchableHighlight>
            <TouchableHighlight
              onPress={() => setSelectedIndex(1)}
              underlayColor={selectedIndex === 1 ? GREEN.btn : BLACK.app}
              style={{
                flex: 1,
                borderWidth: 2,
                borderRadius: 20,
                borderColor: BLACK.app,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: selectedIndex === 1 ? GREEN.btn : BLACK.app,
              }}>
              <Text style={{fontSize: 16, color: WHITE.app, fontFamily: FONT.Muli.bold}}>LOWEST</Text>
            </TouchableHighlight>
            <TouchableHighlight
              onPress={() => setSelectedIndex(2)}
              underlayColor={selectedIndex === 2 ? GREEN.btn : BLACK.app}
              style={{
                flex: 1,
                borderWidth: 2,
                borderRadius: 20,
                borderColor: BLACK.app,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: selectedIndex === 2 ? GREEN.btn : BLACK.app,
              }}>
              <Text style={{fontSize: 16, color: WHITE.app, fontFamily: FONT.Muli.bold}}>Z TO A</Text>
            </TouchableHighlight>
          </View>
          <View style={{flex: 1, paddingTop: 15}}>
            <View style={{
              width: SCREEN.width - 30,
              alignSelf: 'center',
              backgroundColor: BLACK.app,
              borderTopRightRadius: 15,
              borderTopLeftRadius: 15,
              height: 40,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
              <Text style={{fontSize: 16, fontFamily: FONT.QuickSand.bold, color: WHITE.dark}}>Search Results</Text>
            </View>
            <View style={{
              width: SCREEN.width - 30,
              alignSelf: 'center',
              backgroundColor: WHITE.app,
              paddingBottom: isIphoneXorAbove ? 60 : 40,
            }}>
              <FlatList
                keyExtractor={(item, index) => index.toString()}
                data={['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']}
                renderItem={() => <TouchableHighlight
                  style={{height: 45, width: '100%', alignSelf: 'center'}}>
                  <View style={{height: 45, width: '100%', alignSelf: 'center'}}>
                    <View style={{flexDirection: 'row', alignItems: 'center', paddingTop: 5}}>
                      <View style={{flex: 0.7, justifyContent: 'center', paddingTop: 5, paddingLeft: 58}}>
                        <Text style={{fontFamily: FONT.Muli.bold, fontSize: 16, color: BLACK.appLight}}>
                          {searchText}
                        </Text>
                        <Text style={{fontSize: 12, fontFamily: FONT.Muli.light, color: BLACK.appLight}}>
                          {Math.floor(Math.random() * 20)} - jan - 2021
                        </Text>
                      </View>
                      <View style={{flex: 0.3, justifyContent: 'center', alignItems: 'flex-end', paddingTop: 5, paddingRight: 20}}>
                        <Text style={{
                          fontSize: 16,
                          fontFamily: FONT.Muli.bold,
                          color: BLACK.appLight
                        }}>${Math.floor(Math.random() * 40)}</Text>
                      </View>
                    </View>
                  </View>
                </TouchableHighlight>}/>
            </View>
          </View>
        </View>}
      </SafeAreaView>
    </TouchableOpacity>
  );
}

export default SearchModel;
