/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {Image, Text, TextInput, TouchableHighlight, View} from 'react-native';
import {BLACK, GREEN, WHITE} from '../../helper/Color';
import {FONT, isIphoneXorAbove, SCREEN} from '../../helper/Constant';

function HeaderWithSearch({rightIcon, rightPress, searchChange, searchValue}) {
  const [searchText, setSearchText] = useState(searchValue);
  return (
    <View
      style={{
        flexDirection: 'row',
        height: isIphoneXorAbove ? 80 : 60,
        alignItems: 'flex-end',
        width: SCREEN.width - 30,
        alignSelf: 'center',
      }}>
      <View
        style={{
          height: 50,
          width: SCREEN.width - 66,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View style={{
          height: 36,
          width: '95%',
          borderRadius: 7,
          backgroundColor: BLACK.app,
          flexDirection: 'row',
          alignItems: 'center'
        }}>
          <Image
            style={{height: 36, width: 36, resizeMode: 'contain', marginLeft: 5}}
            source={require('../../assets/Icons/36px/NoBackground/searchIconWhite.png')}/>
          <TextInput
            placeholder="Search Text"
            value={searchValue}
            maxLength={20}
            placeholderTextColor={WHITE.light}
            onChangeText={(e) => {
              searchChange(e)
              setSearchText(e)}}
            style={{
              height: 36,
              width: SCREEN.width - 102,
              paddingHorizontal: 5,
              color: WHITE.app,
              fontFamily: FONT.Muli.bold,
              fontSize: 16,
            }}/>
          {searchValue!==''  && <TouchableHighlight
            onPress={() => {
              searchChange('')
              setSearchText('')
            }}
            style={{
              height: 29,
              width: 29,
              position: 'absolute',
              right: 10,
              justifyContent: 'center',
              alignItems: 'center'
            }}>
            <Image
              style={{height: 29, width: 29, resizeMode: 'contain'}}
              source={require('../../assets/Icons/36px/NoBackground/closeFieldIconWhite.png')}/>
          </TouchableHighlight>}
        </View>
      </View>
      <TouchableHighlight
        underlayColor={GREEN.app}
        onPress={rightPress}
        style={{
          height: 50,
          width: 50,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        {rightIcon ? <Image style={{height: 36, width: 36}} source={rightIcon}/> : <View/>}
      </TouchableHighlight>
    </View>
  );
}

export default HeaderWithSearch;
