/* eslint-disable no-alert */
/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {View, Image, TouchableHighlight, Text, ScrollView} from 'react-native';
import Barcode from 'react-native-barcode-expo';

import {BLACK, BLUE, WHITE} from '../../helper/Color';
import {FONT} from '../../helper/Constant';


import ReceiptDetailTags from '../ReceiptDetailTags';

function ReceiptDetail({ item, tagListUpdate, extraData, defaultTags, updateDefaultTags, scrollEnableCall}) {
  const [tagList, setTagList] = useState(['Warranty']);
  const [tagModel, setTagModel] = useState(false);
  
  
    function randomNumber() {
      return Math.floor(Math.random() * 2);
    };
  
  return (
    <View
      style={{
        flex: 1,
      }}>
      <View
        style={{
          borderRadius: 10,
          backgroundColor: WHITE.dark,
          flex: 1,
          paddingVertical: 5,
        }}>
        <ScrollView>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <TouchableHighlight
              style={{height: 23, width: 23, marginLeft: 5}}
              onPress={() => {
                scrollEnableCall(tagModel)
                setTagModel(!tagModel)}}
              underlayColor={WHITE.dark}>
              <Image
                style={{height: '100%', width: '100%', resizeMode: 'contain'}}
                source={require('../../assets/Icons/32px/NoBackground/editBlack.png')}
              />
            </TouchableHighlight>
            {tagList.length < 1 && <Image
              style={{height: 23, width: 23, resizeMode: 'contain'}}
              source={require('../../assets/Icons/32px/NoBackground/envelopeBlack.png')}
            />}
            <Text
              style={{
                fontSize: 16,
                fontFamily: FONT.Muli.regular,
                color: BLACK.dark,
              }}>
              Tags:{' '}
            </Text>
            <View style={{flex: 1}}>
              <View
                style={{flex: 0.65, flexDirection: 'row', alignItems: 'center'}}>
                <ScrollView horizontal style={{flex: 1}} showsHorizontalScrollIndicator={false}>
                  {tagList.map((data, index) => (
                    <Text
                      key={index.toString()}
                      style={{
                        fontSize: 16,
                        fontFamily: FONT.Muli.regular,
                        color: BLACK.dark,
                      }}>
                      {data + ' '}
                    </Text>
                  ))}
                </ScrollView>
              </View>
            </View>
            <View
              style={{flex: 0.35, justifyContent: 'center', alignItems: 'flex-end',}}>
              <Image
                style={{height: 30, width: 30, resizeMode: 'contain', marginRight: 10}}
                source={require('../../assets/Categories/groceriesIcon.png')}
              />
            </View>
          </View>
          <Image
            style={{
              height: 110,
              width: 110,
              resizeMode: 'contain',
              alignSelf: 'center',
              marginTop: 5,
              borderRadius: 5,
            }}
            source={item.logo}
          />
          <Text
            style={{
              fontFamily: FONT.Muli.bold,
              fontSize: 55,
              alignSelf: 'center',
              textAlign: 'center',
              color: BLACK.dark,
              marginTop: 15,
            }}>
            {item.amount}
          </Text>
          <View
            style={{
              marginVertical: 20,
              backgroundColor: WHITE.off,
              height: 1,
              width: '90%',
              alignSelf: 'center',
            }}
          />
          <View
            style={{
              flexDirection: 'row',
              paddingHorizontal: 5,
              width: '90%',
              alignSelf: 'center',
            }}>
            <View style={{flex: 0.7, justifyContent: 'center'}}>
              <Text
                style={{
                  fontFamily: FONT.QuickSand.semiBold,
                  fontSize: 14,
                  color: BLACK.appLight,
                }}>
                Trip Fare
              </Text>
            </View>
            <View
              style={{
                flex: 0.3,
                alignItems: 'flex-end',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontFamily: FONT.QuickSand.semiBold,
                  fontSize: 14,
                  color: BLACK.appLight,
                }}>
                $10.91
              </Text>
            </View>
          </View>
          <View
            style={{
              marginVertical: 20,
              backgroundColor: WHITE.off,
              height: 1,
              width: '90%',
              alignSelf: 'center',
            }}
          />
          
          <View
            style={{
              flexDirection: 'row',
              paddingHorizontal: 5,
              width: '90%',
              alignSelf: 'center',
            }}>
            <View style={{flex: 0.7, justifyContent: 'center'}}>
              <Text
                style={{
                  fontFamily: FONT.QuickSand.semiBold,
                  fontSize: 14,
                  color: BLACK.appLight,
                }}>
                Subtotal
              </Text>
            </View>
            <View
              style={{
                flex: 0.3,
                alignItems: 'flex-end',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontFamily: FONT.QuickSand.semiBold,
                  fontSize: 14,
                  color: BLACK.appLight,
                }}>
                $10.91
              </Text>
            </View>
          </View>
          
          <View
            style={{
              flexDirection: 'row',
              paddingHorizontal: 5,
              width: '90%',
              alignSelf: 'center',
              marginVertical: 5,
            }}>
            <View style={{flex: 0.7, justifyContent: 'center'}}>
              <Text
                style={{
                  fontFamily: FONT.QuickSand.semiBold,
                  fontSize: 14,
                  color: BLACK.appLight,
                }}>
                NY congress Fee
              </Text>
            </View>
            <View
              style={{
                flex: 0.3,
                alignItems: 'flex-end',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontFamily: FONT.QuickSand.semiBold,
                  fontSize: 14,
                  color: BLACK.appLight,
                }}>
                $10.91
              </Text>
            </View>
          </View>
          
          <View
            style={{
              flexDirection: 'row',
              paddingHorizontal: 5,
              width: '90%',
              alignSelf: 'center',
              marginVertical: 5,
            }}>
            <View style={{flex: 0.7, justifyContent: 'center'}}>
              <Text
                style={{
                  fontFamily: FONT.QuickSand.semiBold,
                  fontSize: 14,
                  color: BLACK.appLight,
                }}>
                State Sale Tax
              </Text>
            </View>
            <View
              style={{
                flex: 0.3,
                alignItems: 'flex-end',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontFamily: FONT.QuickSand.semiBold,
                  fontSize: 14,
                  color: BLACK.appLight,
                }}>
                $10.91
              </Text>
            </View>
          </View>
          
          <View
            style={{
              flexDirection: 'row',
              paddingHorizontal: 5,
              width: '90%',
              alignSelf: 'center',
              marginVertical: 5,
            }}>
            <View style={{flex: 0.7, justifyContent: 'center'}}>
              <Text
                style={{
                  fontFamily: FONT.QuickSand.semiBold,
                  fontSize: 14,
                  color: BLACK.appLight,
                }}>
                NY BLACK card Fund
              </Text>
            </View>
            <View
              style={{
                flex: 0.3,
                alignItems: 'flex-end',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontFamily: FONT.QuickSand.semiBold,
                  fontSize: 14,
                  color: BLACK.appLight,
                }}>
                $10.91
              </Text>
            </View>
          </View>
          
          <View
            style={{
              flexDirection: 'row',
              paddingHorizontal: 5,
              width: '90%',
              alignSelf: 'center',
              marginVertical: 5,
            }}>
            <View style={{flex: 0.7, justifyContent: 'center'}}>
              <Text
                style={{
                  fontFamily: FONT.QuickSand.semiBold,
                  fontSize: 14,
                  color: BLACK.appLight,
                }}>
                Wait Time
              </Text>
            </View>
            <View
              style={{
                flex: 0.3,
                alignItems: 'flex-end',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontFamily: FONT.QuickSand.semiBold,
                  fontSize: 14,
                  color: BLACK.appLight,
                }}>
                $10.91
              </Text>
            </View>
          </View>
          
          <View
            style={{
              flexDirection: 'row',
              paddingHorizontal: 5,
              width: '90%',
              alignSelf: 'center',
              marginVertical: 5,
            }}>
            <View style={{flex: 0.7, justifyContent: 'center'}}>
              <Text
                style={{
                  fontFamily: FONT.QuickSand.bold,
                  fontSize: 14,
                  color: BLACK.dark,
                }}>
                Total
              </Text>
            </View>
            <View
              style={{
                flex: 0.3,
                alignItems: 'flex-end',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontFamily: FONT.QuickSand.bold,
                  fontSize: 14,
                  color: BLACK.dark,
                }}>
                $14.15
              </Text>
            </View>
          </View>
          <View
            style={{
              marginVertical: 20,
              backgroundColor: WHITE.off,
              height: 1,
              width: '90%',
              alignSelf: 'center',
            }}
          />
          {randomNumber() === 1 ? <View>
            <Text
              style={{
                width: '90%',
                alignSelf: 'center',
                fontFamily: FONT.QuickSand.regular,
                color: BLACK.appLight,
                fontSize: 12,
                lineHeight: 20,
              }}>
              Driver are critical to commutes right now sat thank with trip. Driver
              are critical to commutes right now sat thank with trip. Driver are
              critical to commutes right now sat thank with trip.{'\n'}Rate or trip
            </Text>
            <Text
              style={{
                width: '90%',
                alignSelf: 'center',
                fontFamily: FONT.QuickSand.regular,
                color: BLACK.appLight,
                fontSize: 12,
                lineHeight: 20,
              }}>
              License Plate: T4764Ft
            </Text>
            <Text
              style={{
                width: '90%',
                alignSelf: 'center',
                fontFamily: FONT.QuickSand.regular,
                color: BLACK.appLight,
                fontSize: 12,
                lineHeight: 20,
              }}>
              FHV License Number: T4764Ft
            </Text>
            <Text
              style={{
                width: '90%',
                alignSelf: 'center',
                fontFamily: FONT.QuickSand.regular,
                color: BLACK.appLight,
                fontSize: 12,
                lineHeight: 20,
              }}>
              Driver TLC License Number: T4764F7876
            </Text>
    
            <View
              style={{
                marginVertical: 20,
                backgroundColor: WHITE.off,
                height: 1,
                width: '90%',
                alignSelf: 'center',
              }}
            />
            <View
              style={{flexDirection: 'row', width: '90%', alignSelf: 'center'}}>
              <View style={{flex: 1}}>
                <Text
                  style={{
                    fontFamily: FONT.Muli.semiBold,
                    color: BLACK.app,
                    fontSize: 12,
                    lineHeight: 20,
                  }}>
                  MC4287 (Countless)
                </Text>
              </View>
              <View style={{flex: 1, alignItems: 'flex-end'}}>
                <Text
                  style={{
                    fontFamily: FONT.QuickSand.regular,
                    color: BLACK.app,
                    fontSize: 12,
                    lineHeight: 20,
                  }}>
                  June 8 2020 at 8:54pm
                </Text>
              </View>
            </View>
    
            <View
              style={{flexDirection: 'row', width: '90%', alignSelf: 'center'}}>
              <View style={{flex: 1}}>
                <Image
                  style={{height: 30, width: 40, resizeMode: 'contain'}}
                  source={require('../../assets/CardIssuers/mcLogo.png')}/>
              </View>
              <View style={{flex: 1, alignItems: 'flex-end'}}>
                <Text
                  style={{
                    fontFamily: FONT.QuickSand.regular,
                    color: BLACK.app,
                    fontSize: 12,
                    lineHeight: 20,
                  }}>
                  #jzBS
                </Text>
              </View>
            </View>
    
            <View
              style={{flexDirection: 'row', width: '90%', alignSelf: 'center'}}>
              <View style={{flex: 1}}>
                <Text
                  style={{
                    fontFamily: FONT.Muli.semiBold,
                    color: BLACK.app,
                    fontSize: 12,
                    lineHeight: 20,
                  }}>
                  JOHN DOE
                </Text>
              </View>
              <View style={{flex: 1, alignItems: 'flex-end'}}>
                <Text
                  style={{
                    fontFamily: FONT.QuickSand.regular,
                    color: BLACK.app,
                    fontSize: 12,
                    lineHeight: 20,
                  }}>
                  Auth Code: 883471
                </Text>
              </View>
            </View>
    
            <Text
              style={{
                width: '90%',
                alignSelf: 'center',
                fontFamily: FONT.QuickSand.regular,
                color: BLACK.appLight,
                fontSize: 12,
                lineHeight: 20,
              }}>
              Driver are critical to commutes right now sat thank with trip. Driver
              are critical to commutes right now sat thank with trip. Driver are
              critical to commutes right now sat thank with trip.
              <Text style={{
                fontFamily: FONT.QuickSand.regular,
                color: BLUE.app,
                textDecorationLine: 'underline',
                fontSize: 13
              }}>
                Learn More
              </Text>
            </Text>
            <View
              style={{
                marginVertical: 20,
                backgroundColor: WHITE.off,
                height: 1,
                width: '90%',
                alignSelf: 'center',
              }}
            />
            <View
              style={{flexDirection: 'row', width: '90%', alignSelf: 'center'}}>
              <View style={{flex: 1}}>
                <Text
                  style={{
                    fontFamily: FONT.QuickSand.regular,
                    color: BLACK.app,
                    fontSize: 12,
                    lineHeight: 20,
                  }}>
                  AID:A0000002501088
                </Text>
              </View>
            </View>
            <View style={{flex: 1, alignItems: 'flex-end'}}>
              <Image
                style={{height: 30, width: 40, resizeMode: 'contain'}}
                source={require('../../assets/Layout/logo.png')}/>
            </View>
          </View> : <View style={{height: 40, width: '90%', alignSelf: 'center'}}>
            <Barcode value="Hello World" format="CODE128" />
          </View>}
        </ScrollView>
      </View>
      {tagModel &&
      <ReceiptDetailTags
        cancelPress={() => {
          scrollEnableCall(true)
          setTagModel(false)}}
        selectedTags={tagList}
        extraData={extraData}
        defaultTags={defaultTags}
        updateDefaultTags={updateDefaultTags}
        onSelectTags={(tagName) => {
          if (tagList.includes(tagName)) {
            let listOfTagSelected = [];
            listOfTagSelected = tagList;
            const indexOfTagThere = listOfTagSelected.indexOf(tagName);
            listOfTagSelected.splice(indexOfTagThere, 1);
            setTagList(listOfTagSelected);
            tagListUpdate(setTagList)
          } else {
            let listOfTagSelected = [];
            listOfTagSelected = tagList;
            listOfTagSelected.push(tagName)
            setTagList(listOfTagSelected)
            tagListUpdate(setTagList)
          }
        }}/>}
    </View>
  );
}

export default ReceiptDetail;
