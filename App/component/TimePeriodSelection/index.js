/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {Image, TouchableHighlight, View, TouchableOpacity, Text, FlatList} from 'react-native';
import {FONT, SCREEN} from '../../helper/Constant';
import {BLACK, GREEN, WHITE} from "../../helper/Color";

function TimePeriodSelection({pressSave, pressClose}) {
  const [timeSelect, setTimeSelect] = useState('Week');
  const timePeriodList = ['Week To Date', 'Month to Date', 'Year to Date', 'Week', 'Month', 'Year', 'Last Week']
  
  return (
    <TouchableOpacity
      activeOpacity={1}
      style={{
        height: SCREEN.height,
        alignItems: 'center',
        width: SCREEN.width,
        alignSelf: 'center',
        backgroundColor: BLACK.transParent,
        position: 'absolute',
        justifyContent: 'center',
      }}>
      <View style={{ width: '80%', backgroundColor: GREEN.app, alignSelf: 'center'}}>
        <Text style={{
          color: WHITE.dark,
          fontSize: 16,
          fontFamily: FONT.Muli.bold,
          alignSelf: 'center',
          marginVertical: 12
        }}>Time Period Selection</Text>
        <View style={{width: '80%', borderRadius: 15, backgroundColor: WHITE.dark, alignSelf: 'center'}}>
          <FlatList
            style={{marginVertical: 40}}
            data={timePeriodList}
            keyExtractor={(item, index) => index.toString()}
            renderItem={(item, index) =>
              <Text onPress={() => setTimeSelect(item.item)}
                style={{fontFamily: FONT.QuickSand.bold, fontSize: 16, color: timeSelect === item.item ? BLACK.dark : WHITE.appLight, alignSelf: 'center', marginTop: 6}}>
                {item.item}
              </Text>
          } />
        </View>
        <View style={{width: '80%', alignSelf: 'center', flexDirection: 'row', height: 60}}>
          <View style={{flex: 1}}>
            <TouchableHighlight style={{height: '100%', width: 25, justifyContent: 'center'}} onPress={pressClose}
                                underlayColor={GREEN.app}>
              <Image style={{height: 40, width: 40, resizeMode: 'contain'}}
                     source={require('../../assets/Icons/36px/NoBackground/closeInvertedIconBlack.png')}/>
            </TouchableHighlight>
          </View>
          <View style={{flex: 1}}>
            <View style={{flex: 1, alignItems: 'flex-end'}}>
              <TouchableHighlight
                style={{height: '100%', width: 25, justifyContent: 'center'}}
                                  onPress={() => pressSave(timeSelect)} underlayColor={GREEN.app}>
                <Image style={{height: 40, width: 40, resizeMode: 'contain'}}
                       source={require('../../assets/Icons/36px/NoBackground/saveIconBlack.png')}/>
              </TouchableHighlight>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
);
}

export default TimePeriodSelection;
