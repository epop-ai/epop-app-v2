/* eslint-disable react-native/no-inline-styles */
import React, {useState} from "react";
import {
  FlatList,
  Text,
  TouchableOpacity,
  View,
  Image,
} from "react-native";
import {FONT, isIphoneXorAbove, SCREEN} from "../../helper/Constant";
import {BLACK, GREEN, RED, WHITE} from "../../helper/Color";

function HorizontalListWithTitle({list, listItemClick, listTitle, listDetail}) {
  return (
    <View
      activeOpacity={1}
      style={{
        height: listDetail ? 270 : 220,
        width: SCREEN.width,
        marginTop: 10,
        justifyContent: 'center',
        paddingLeft: 20,
      }}
    >
      <Text style={{fontFamily: FONT.Muli.bold, color: WHITE.dark, fontSize: 14, marginBottom: 7}}>
        {listTitle}
      </Text>
      {listDetail && <Text style={{fontFamily: FONT.Muli.regular, color: WHITE.dark, fontSize: 12, marginBottom: 7}}>
        {listDetail}
      </Text>}
      <FlatList
        data={list} keyExtractor={(item, index) => index.toString()}
        horizontal
        showsHorizontalScrollIndicator={false}
        renderItem={(item, index) =>
          <TouchableOpacity
            onPress={() => listItemClick(item.item)}
            activeOpacity={0.9}
            style={{
              height: 171,
              width: 126,
              backgroundColor: WHITE.dark,
              borderRadius: 12,
              alignItems: 'center',
              marginRight: 20
            }}>
            <Image
              source={item.item.icon}
              style={{
                height: 56,
                width: 56,
                alignSelf: 'center',
                marginTop: 14,
                marginBottom: 10,
                resizeMode: 'contain'
              }}/>
            <View
              style={{
                height: 1,
                width: '80%',
                alignSelf: 'center',
                backgroundColor: BLACK.textColor,
                marginBottom: 7
              }}/>
            <Text style={{fontSize: 12, fontFamily: FONT.Muli.regular}}>
              {item.item.name}
            </Text>
            <Text style={{fontSize: 10, fontFamily: FONT.Muli.regular, marginTop: 5, color: GREEN.btn}}>
              {item.item.off}
            </Text>
            {listTitle === 'Featured' && <Text style={{
              fontSize: 12,
              fontFamily: FONT.Muli.regular,
              marginTop: 10,
              color: item.item.timeLeft < 5 ? RED.app : BLACK.app
            }}>
              {item.item.timeLeft}{' days left'}
            </Text>}
          </TouchableOpacity>}/>
    </View>
  );
}

export default HorizontalListWithTitle;
