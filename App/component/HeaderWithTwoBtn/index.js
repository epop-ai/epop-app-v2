/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Image, Text, TouchableHighlight, View} from 'react-native';
import {BLACK, GREEN, WHITE} from '../../helper/Color';
import {FONT, isIphoneXorAbove, SCREEN} from '../../helper/Constant';

function HeaderWithTwoBtn({leftIcon, leftPress, rightIcon, rightPress, title}) {
  // const [listTiming, setListTiming] = useState(timingArray);
  return (
    <View
      style={{
        flexDirection: 'row',
        height: isIphoneXorAbove ? 80 : 60,
        alignItems: 'center',
        width: SCREEN.width - 30,
        alignSelf: 'center',
      }}>
      <TouchableHighlight
        underlayColor={GREEN.app}
        onPress={leftPress}
        style={{
          height: 36,
          width: 36,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        {leftIcon ? <Image style={{height: '100%', width: '100%'}} source={leftIcon}/> : <View/>}
      </TouchableHighlight>
      <View
        style={{
          height: 50,
          width: SCREEN.width - 102,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{fontFamily: FONT.Muli.bold, color: WHITE.dark, fontSize: 20}}>
          {title}
        </Text>
      </View>
      <TouchableHighlight
        underlayColor={GREEN.app}
        onPress={rightPress}
        style={{
          height: 36,
          width: 36,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        {rightIcon ? <Image style={{height: '100%', width: '100%'}} source={rightIcon}/> : <View/>}
      </TouchableHighlight>
    </View>
  );
}
export default HeaderWithTwoBtn;
