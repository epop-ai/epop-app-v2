/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Image, Text, TouchableHighlight, View} from 'react-native';
import {BLACK, BLUE, GREEN, WHITE} from '../../helper/Color';
import {FONT, SCREEN} from '../../helper/Constant';

function ReportListItem({item, itemClick}) {
  // const [listTiming, setListTiming] = useState(timingArray);
  return (
    <View
      style={{
        backgroundColor: WHITE.dark,
        width: SCREEN.width,
        height: 51,
        justifyContent: 'center',
      }}>
      {item.name !== 'ad' ? (
        <TouchableHighlight
          onPress={itemClick}
          underlayColor={BLUE.extraLight}
          style={{
            borderRadius: 12,
            width: SCREEN.width,
            alignSelf: 'center',
            flexDirection: 'row',
            alignItems: 'center',
            height: 60,
            borderBottomWidth: 0.4,
            borderBottomColor: BLACK.borderLight,
          }}>
          <View style={{height: '100%', width: '100%', flexDirection: 'row', alignItems: 'center'}}>
            <Image
              style={{
                height: 30,
                width: 30,
                resizeMode: 'contain',
                marginLeft: 24,
                borderRadius: 5,
              }}
              source={item.logo}
            />
            <View style={{marginLeft: 12}}>
              <Text style={{fontSize: 16, fontFamily: FONT.Muli.bold, color: BLACK.appLight}}>
                {item.name}
              </Text>
              <Text
                style={{
                  fontSize: 12,
                  fontFamily: FONT.Muli.regular,
                  color: BLACK.appLight,
                  marginTop: 2,
                }}>
                {item.date}
              </Text>
            </View>
            <View style={{flex: 1, alignItems: 'flex-end', height: 50, justifyContent: 'center', marginRight: 28}}>
              <Text style={{fontSize: 16, fontFamily: FONT.Muli.bold, color: BLACK.appLight}}>
                {item.amount}
              </Text>
                <Text
                    style={{
                        fontSize: 12,
                        fontFamily: FONT.Muli.regular,
                        color: BLACK.appLight,
                        marginTop: 2,
                    }}>
                </Text>
            </View>
          </View>
        </TouchableHighlight>
      ) : (
        <View
          style={{
            height: 51,
            width: SCREEN.width,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: GREEN.app,
          }}>
          <Image
            style={{height: 51, width: SCREEN.width, resizeMode: 'contain'}}
            source={require('../../assets/Ads/appleMusicAdd2.png')}
          />
        </View>
      )}
      <View style={{height: 1, width: SCREEN.width - 80, backgroundColor: WHITE.cream, alignSelf: 'center', marginVertical: 5}} />
    </View>
  );
}
export default ReportListItem;
