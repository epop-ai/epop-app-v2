/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {Image, Text, TouchableHighlight, View, TouchableOpacity, SafeAreaView} from 'react-native';
import {FONT, isIphoneXorAbove, SCREEN} from '../../helper/Constant';
import {BLACK, GREEN, WHITE} from "../../helper/Color";
import HeaderWithSearch from "../HeaderWithSearch";
import * as ImagePicker from "expo-image-picker";

function InterestModal({pressClose}) {
  const [searchList] = useState(['Amazon', 'Uber', "Macy's"]);
  const [imageUri, setImageUri] = useState('https://thumbs.dreamstime.com/b/user-profile-icon-abstract-digital-design-blue-background-isolated-172062850.jpg');
  const [selectedIndex, setSelectedIndex] = useState(0);
  
  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });
    
    if (!result.cancelled) {
      setImageUri(result.uri)
    }
  };
  
  return (
    <TouchableOpacity
      activeOpacity={1}
      style={{
        flex: 1,
        paddingHorizontal: 18,
        alignSelf: 'center',
        backgroundColor: GREEN.app,
      }}>
      <SafeAreaView style={{flex: 1}}>
        <View style={{flex: 0.3, justifyContent: 'flex-end', alignItems: 'center'}}>
          <View style={{height: '60%', width: '50%'}}>
            <Image
              style={{height: '50%', width: '40%', resizeMode: 'contain', borderRadius: 15}}
              source={{uri: 'https://ps.w.org/wp-user-avatar/assets/icon-256x256.png?rev=1755722'}}/>
            <TouchableOpacity
              onPress={() => pickImage()}
              activeOpacity={0.8}
              style={{height: 35, width: 39, position: 'absolute', right: 10, bottom: 10}}>
              <Image
                style={{height: 35, width: 39}}
                source={require('../../assets/OtherIcons/updatePic.png')}/>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{flex: 0.7}}>
          <View style={{height: '90%', width: '90%', backgroundColor: WHITE.dark, alignSelf: 'center'}}>
          
          </View>
        </View>
      </SafeAreaView>
    </TouchableOpacity>
  );
}

export default InterestModal;
