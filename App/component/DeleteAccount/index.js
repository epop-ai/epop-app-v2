/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {Image, Text, TouchableHighlight, View, TouchableOpacity} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import {FONT, isIphoneXorAbove, SCREEN} from '../../helper/Constant';
import {BLACK, GREEN, WHITE} from "../../helper/Color";

function DeleteAccount({cancelPress, savePress,}) {
  
  
  return (
    <TouchableOpacity
      activeOpacity={1}
      style={{
        height: SCREEN.height,
        alignItems: 'center',
        width: SCREEN.width,
        alignSelf: 'center',
        backgroundColor: BLACK.transParent,
        position: 'absolute',
        justifyContent: 'center',
      }}>
      <View style={{minHeight: 250, width: '80%', backgroundColor: GREEN.app, alignSelf: 'center'}}>
        <Text style={{
          color: WHITE.dark,
          fontSize: 20,
          fontFamily: FONT.QuickSand.bold,
          alignSelf: 'center',
          marginVertical: 12
        }}>Delete card</Text>
        <View style={{
          backgroundColor: WHITE.dark,
          borderRadius: 10,
          alignSelf: 'center',
          width: '70%',
          marginVertical: 10,
          paddingVertical: 10
        }}>
          <Text style={{
            fontSize: 20,
            fontFamily: FONT.QuickSand.bold,
            alignSelf: 'center',
            marginVertical: 12,
            textAlign: 'center',
          }}>Are you sure you want to delete this card?</Text>
          <Text style={{
            fontSize: 16,
            fontFamily: FONT.QuickSand.regular,
            alignSelf: 'center',
            marginVertical: 12,
            paddingHorizontal: 5,
            textAlign: 'center',
          }}>Note, this card will still be kept securely on our files to ensure you can access your past receipts and in
            case you decide to reactivate the card in the future</Text>
        </View>
        <View style={{width: '70%', alignSelf: 'center', flexDirection: 'row', height: 60,}}>
          <View style={{flex: 1}}>
            <TouchableHighlight
              style={{height: '100%', width: 25, justifyContent: 'center'}}
              onPress={cancelPress}
              underlayColor={GREEN.app}>
              <Image
                style={{height: 40, width: 40, resizeMode: 'contain'}}
                source={require('../../assets/Icons/36px/NoBackground/closeInvertedIconBlack.png')}/>
            </TouchableHighlight>
          </View>
          <View style={{flex: 1}}>
            <View style={{flex: 1, alignItems: 'flex-end'}}>
              <TouchableHighlight
                style={{height: '100%', width: 25, justifyContent: 'center'}}
                onPress={savePress} underlayColor={GREEN.app}>
                <Image
                  style={{height: 40, width: 40, resizeMode: 'contain'}}
                  source={require('../../assets/Icons/36px/NoBackground/saveIconBlack.png')}/>
              </TouchableHighlight>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

export default DeleteAccount;
