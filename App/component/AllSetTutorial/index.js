/* eslint-disable react-native/no-inline-styles */
import React, {useState} from "react";
import {
  Image,
  Text,
  TouchableHighlight,
  View,
  TouchableOpacity,
} from "react-native";
import {FONT, isIphoneXorAbove, SCREEN} from "../../helper/Constant";
import {BLACK, GREEN, WHITE} from "../../helper/Color";

function AllSetTutorial({iAmGood, startTutorial}) {
  return (
    <TouchableOpacity
      activeOpacity={1}
      style={{
        height: SCREEN.height,
        alignItems: "center",
        width: SCREEN.width,
        alignSelf: "center",
        backgroundColor: BLACK.transParent,
        position: "absolute",
        justifyContent: "center",
      }}
    >
      <View
        style={{
          minHeight: 250,
          width: "80%",
          backgroundColor: GREEN.app,
          alignSelf: "center",
          alignItems: "center",
          paddingVertical: 40,
          justifyContent: 'center',
        }}
      >
        <Text
          style={{
            fontSize: 20,
            fontFamily: FONT.QuickSand.bold,
            color: WHITE.dark,
            fontWeight: '600',
          }}
        >
          All Set! Welcome!
        </Text>
        <Text
          style={{
            fontSize: 18,
            fontFamily: FONT.QuickSand.regular,
            color: WHITE.dark,
            marginTop: 10,
            textAlign: "center",
            paddingHorizontal: 20,
          }}
        >
          Let's take you through a{'\n'}quick tutorial to{'\n'}familiarize yourself with{'\n'}the app ?
        </Text>
        <TouchableHighlight
          onPress={startTutorial}
          style={{
            height: 46,
            width: "70%",
            backgroundColor: GREEN.btn,
            borderRadius: 5,
            marginTop: 13,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Text
            style={{
              fontFamily: FONT.Muli.bold,
              fontSize: 20,
              color: WHITE.dark,
            }}
          >
            Start Tutorial
          </Text>
        </TouchableHighlight>
        <TouchableHighlight
          onPress={iAmGood}
          style={{
            height: 46,
            width: "70%",
            backgroundColor: BLACK.app,
            borderRadius: 5,
            marginTop: 13,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Text
            style={{
              fontFamily: FONT.Muli.bold,
              fontSize: 20,
              color: WHITE.dark,
            }}
          >
            I am good to go!
          </Text>
        </TouchableHighlight>
      </View>
    </TouchableOpacity>
  );
}

export default AllSetTutorial;
