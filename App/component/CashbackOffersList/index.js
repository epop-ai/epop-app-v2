/* eslint-disable react-native/no-inline-styles */
import React, {useState} from "react";
import {
  FlatList,
  Text,
  TouchableOpacity,
  View,
  Image, TouchableHighlight,
} from "react-native";
import {FONT, isIphoneXorAbove, SCREEN} from "../../helper/Constant";
import {BLACK, BLUE, GREEN, RED, WHITE} from "../../helper/Color";

function CashbackOffersList({list, listTitle, listDetail, listHeaderComponent}) {
  return (
    <View
      activeOpacity={1}
      style={{
        width: '100%',
        marginTop: 10,
        justifyContent: 'center',
      }}>
      <FlatList
        keyExtractor={(item, index) => index.toString()}
        data={list} keyExtractor={(item, index) => index.toString()}
        showsHorizontalScrollIndicator={false}
        ListHeaderComponent={
          <View>
            {listHeaderComponent}
            <Text style={{fontFamily: FONT.Muli.bold, color: WHITE.dark, fontSize: 14, marginBottom: 7, marginLeft: 15,}}>
              {listTitle}
            </Text>
            <View style={{backgroundColor: WHITE.dark, width: '90%', alignSelf: 'center', borderTopLeftRadius: 10, borderTopRightRadius: 10, paddingBottom: 10}}>
    
              <View style={{height: 40, width: '100%', flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: GREEN.app}}>
                <View style={{paddingLeft: 12, flex: 1, justifyContent: 'center'}}>
                  <Text style={{fontSize: 15, fontFamily: FONT.Muli.regular, color: BLACK.app}}>
                    Cash back from deals:
                  </Text>
                </View>
                <View style={{flex: 1, alignItems: 'flex-end', justifyContent: 'center'}}>
                  <Text style={{fontSize: 16, fontFamily: FONT.Muli.bold, color: BLACK.appLight, marginRight: 11}}>
                    $ { Math.floor(Math.random() * 10) + 1 }
                  </Text>
                </View>
              </View>
          </View>
          </View>
        }
        renderItem={(item, index) =>
          <TouchableHighlight
            underlayColor={BLUE.extraLight}
            style={{
              width: '90%',
              alignSelf: 'center',
              paddingVertical: 20,
              flexDirection: 'row',
              alignItems: 'center',
              backgroundColor: WHITE.dark,
            }}>
            <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center'}}>
              <Image
                style={{
                  height: 30,
                  width: 30,
                  resizeMode: 'contain',
                  marginLeft: 15,
                  borderRadius: 8,
                }}
                source={item.item.icon}
              />
              <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{paddingLeft: 12, flex: 1}}>
                <Text style={{fontSize: 12, fontFamily: FONT.Muli.bold, color: BLACK.appLight}}>
                  {item.item.name}
                </Text>
                <Text
                  style={{
                    fontSize: 10,
                    fontFamily: FONT.Muli.regular,
                    color: BLACK.appLight,
                    marginTop: 2,
                  }}>
                  Earned Date: 12-May-2020 Transaction Amount: $22.20
                </Text>
              </View>
              <View style={{flex: 1, alignItems: 'flex-end', justifyContent: 'center'}}>
                <Text style={{fontSize: 16, fontFamily: FONT.Muli.bold, color: BLACK.appLight, marginRight: 11}}>
                  $ { Math.floor(Math.random() * 10) + 1 }
                </Text>
                <Text
                  style={{
                    fontSize: 12,
                    fontFamily: FONT.Muli.regular,
                    color: BLACK.appLight,
                    marginTop: 2,
                  }}>
                </Text>
              </View>
              </View>
            </View>
          </TouchableHighlight>}
      />
    </View>
  );
}

export default CashbackOffersList;
