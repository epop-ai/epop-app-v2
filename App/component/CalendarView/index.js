/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {CalendarList} from 'react-native-calendars';

import {Image, Text, TouchableHighlight, View, TouchableOpacity} from 'react-native';
import {FONT, isIphoneXorAbove, SCREEN} from '../../helper/Constant';
import {BLACK, GREEN, WHITE} from "../../helper/Color";
import HeaderWithTwoBtn from "../HeaderWithTwoBtn";

function CalendarView({pressSave, pressClose, openEditCalender}) {
  
  return (
    <TouchableOpacity
      activeOpacity={1}
      style={{
        height: isIphoneXorAbove ? SCREEN.height - 100 : SCREEN.height - 60,
        alignItems: 'center',
        width: SCREEN.width,
        alignSelf: 'center',
        backgroundColor: GREEN.app,
        position: 'absolute',
      }}>
      
      <HeaderWithTwoBtn
        leftIcon={require("../../assets/Icons/36px/WithDarkBackground/closeFieldBackBlack.png")}
        leftPress={pressClose}
        title={"Select a Range"}
        rightIcon={require("../../assets/Icons/36px/WithDarkBackground/saveGreenBackBlack.png")}
        rightPress={pressSave}
      />
      <View style={{
        height: isIphoneXorAbove ? SCREEN.height - 150 : SCREEN.height - 100,
        width: '90%',
        backgroundColor: WHITE.app,
        alignSelf: 'center',
        borderRadius: 10,
        paddingHorizontal: 10,
        marginBottom: 100,
        paddingTop: 10
      }}>
        <CalendarList
          // Callback which gets executed when visible months change in scroll view. Default = undefined
          onVisibleMonthsChange={(months) => {console.log('now these months are visible', months);}}
          // Max amount of months allowed to scroll to the past. Default = 50
          pastScrollRange={50}
          // Max amount of months allowed to scroll to the future. Default = 50
          futureScrollRange={50}
          // Enable or disable scrolling of calendar list
          scrollEnabled={true}
          // Enable or disable vertical scroll indicator. Default = false
          showScrollIndicator={true}
          markedDates={{
            '2021-02-22': {startingDay: true, color: GREEN.app, borderRadius: 10},
            '2021-02-23': {color: '#70d7c7', textColor: 'white'},
            '2021-02-24': {color: '#70d7c7', textColor: 'white', marked: true, dotColor: 'white'},
            '2021-02-25': {color: '#70d7c7', textColor: 'white'},
            '2021-02-26': {color: GREEN.app, textColor: 'white', endingDay: true},
          }}
          // Date marking style [simple/period/multi-dot/custom]. Default = 'simple'
          markingType={'period'}
        />
        <TouchableHighlight
          underlayColor={WHITE.app}
          onPress={openEditCalender}
          style={{
            position: 'absolute',
            right: 45,
            top: 25,
            height: 20,
            width: 20,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image style={{height: 23, width: 23, resizeMode: 'contain'}}
                 source={require('../../assets/Icons/32px/NoBackground/editBlack.png')}/>
        </TouchableHighlight>
      </View>
    </TouchableOpacity>
  );
}

export default CalendarView;
