/* eslint-disable react-native/no-inline-styles */
import React, {useState, useRef} from 'react';
import {View, Image, TouchableHighlight, Text, TextInput, FlatList, Platform, KeyboardAvoidingView} from 'react-native';
import {BLACK, GREEN, WHITE} from '../../helper/Color';
import {FONT, SCREEN} from '../../helper/Constant';

function ReceiptDetailTags({cancelPress, selectedTags, onSelectTags, extraData, defaultTags, updateDefaultTags}) {
  const [focus, setFocus] = useState(false);
  const [textInputValue, setTextInputValue] = useState('');
  const ref_input2 = useRef();
  return (
    <TouchableHighlight
      activeOpacity={1}
      style={{
        height: '100%',
        width: '100%',
        position: 'absolute',
        backgroundColor: BLACK.transParent,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <KeyboardAvoidingView
        style={{flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        enabled>
      <View
        style={{
          width: SCREEN.width - 80,
          backgroundColor: GREEN.app,
          paddingVertical: 5,
          paddingHorizontal: 5,
        }}>
        <Text
          style={{
            fontSize: 20,
            fontFamily: FONT.Muli.bold,
            color: WHITE.dark,
            alignSelf: 'center',
          }}>
          My Tags
        </Text>
        <View
          style={{
            width: '90%',
            paddingVertical: 10,
            backgroundColor: WHITE.dark,
            borderRadius: 10,
            alignSelf: 'center',
            marginVertical: 5,
          }}>
          <FlatList
            keyExtractor={(item, index) => index.toString()}
            data={defaultTags}
            extraData={defaultTags}
            renderItem={(item) => (
            <TouchableHighlight
              underlayColor={WHITE.dark}
              keyExtractor={(item, index) => index.toString()}
              extraData={extraData}
              onPress={() => onSelectTags(item.item)}
              style={{
                flexDirection: 'row',
                alignSelf: 'center',
                width: '100%',
                marginTop: 10,
              }}>
              <View style={{
                flexDirection: 'row',
                justifyContent: 'flex-end',
                width: '90%',}}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'flex-end',
                  alignItems: 'flex-end',
                }}>
                <Text style={{fontFamily: FONT.Muli.bold, fontSize: 20, marginRight: 30}}>
                  {item.item}
                </Text>
                <View style={{flex: 1, alignItems: 'flex-end'}}>
                  {!selectedTags.includes(item.item) ? (
                    <View
                      style={{
                        borderRadius: 10,
                        borderWidth: 1,
                        borderColor: BLACK.dark,
                        width: 20,
                        height: 20,
                        marginLeft: 10,
                      }}
                    />
                  ) : (
                    <View
                      style={{
                        height: 20,
                        width: 20,
                        borderRadius: 10,
                        borderWidth: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginLeft: 10,
                      }}>
                      <View style={{height: '100%', width: '100%', backgroundColor: GREEN.btn, borderRadius: 10}}/>
                    </View>
                  )}
                </View>
              </View>
              </View>
            </TouchableHighlight>)}/>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 10,
              borderTopWidth: 0.4,
              borderTopColor: BLACK.borderLight,
              paddingTop: 10,
              height: 50,
            }}>
            <TouchableHighlight
              onPress={() => ref_input2.current.focus()}
              underlayColor={WHITE.cream}
              style={{
                height: 30,
                width: 30,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                style={{height: '90%', width: '90%', resizeMode: 'contain'}}
                source={require('../../assets/OtherIcons/plus.png')}
              />
            </TouchableHighlight>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <TextInput
                ref={ref_input2}
                onFocus={() => setFocus(true)}
                onChangeText={(text) => setTextInputValue(text)}
                value={textInputValue}
                onSubmitEditing={event => {
                  let tagsNow = defaultTags;
                  if (tagsNow.includes(event.nativeEvent.text.trim())) {} else {
                    tagsNow.push(event.nativeEvent.text)
                    onSelectTags(event.nativeEvent.text);
                    updateDefaultTags(tagsNow);
                    setTextInputValue('');
                    setFocus(false);
                  }
                }}
                style={{
                  flex: 1,
                  paddingHorizontal: 10,
                  height: '90%',
                  width: '90%',
                  alignSelf: 'center',
                  borderRadius: 15,
                  backgroundColor: focus ? WHITE.textInputSelected : WHITE.dark,
                }}
              />
            </View>
          </View>
        </View>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <View style={{flex: 0.5, marginLeft: 10}}>
            <TouchableHighlight
              style={{height: 40, width: 40}}
              onPress={cancelPress}
              underlayColor={GREEN.app}>
              <Image
                style={{height: '100%', width: '100%', resizeMode: 'contain'}}
                source={require('../../assets/Icons/36px/NoBackground/closeInvertedIconBlack.png')}
              />
            </TouchableHighlight>
          </View>
          <View
            style={{
              flex: 0.5,
              alignItems: 'flex-end',
              marginRight: 10,
            }}>
            <TouchableHighlight
              style={{height: 40, width: 40}}
              onPress={cancelPress}
              underlayColor={GREEN.app}>
              <Image
                style={{height: '100%', width: '100%', resizeMode: 'contain'}}
                source={require('../../assets/Icons/36px/NoBackground/saveIconBlack.png')}
              />
            </TouchableHighlight>
          </View>
        </View>
      </View>
      </KeyboardAvoidingView>
      
    </TouchableHighlight>
  );
}

export default ReceiptDetailTags;
