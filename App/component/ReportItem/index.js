/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Image, Text, TouchableHighlight, View} from 'react-native';
import {BLACK, BLUE, GREEN, WHITE} from '../../helper/Color';
import {FONT, SCREEN} from '../../helper/Constant';

function ReportItem({item, itemClick, index}) {
  return (
    <View
      style={{
        backgroundColor: WHITE.dark,
        width: SCREEN.width - 20,
        borderTopRightRadius: index === 0 ? 10 : 0,
        borderTopLeftRadius: index === 0 ? 10 : 0,
      }}>
      {item.name !== 'ad' ? (
        <TouchableHighlight
          onPress={itemClick}
          underlayColor={BLUE.extraLight}
          style={{
            borderRadius: 12,
            paddingVertical: 10,
            width: SCREEN.width - 50,
            alignSelf: 'center',
            marginVertical: 10,
            flexDirection: 'row',
            alignItems: 'center',
            height: 50,
          }}>
          <View style={{height: '100%', width: '100%', flexDirection: 'row', alignItems: 'center'}}>
            <Image
              style={{
                height: 30,
                width: 30,
                resizeMode: 'contain',
                marginLeft: 5,
                borderRadius: 8,
              }}
              source={item.logo}
            />
            <View style={{marginLeft: 12}}>
              <Text style={{fontSize: 16, fontFamily: FONT.Muli.bold, color: BLACK.appLight}}>
                {item.name}
              </Text>
              <Text
                style={{
                  fontSize: 12,
                  fontFamily: FONT.Muli.light,
                  color: BLUE.facebook,
                  marginTop: 2,
                }}>
                { Math.floor(Math.random() * 10) + 1+ ' ' }transactions
              </Text>
            </View>
            <View style={{flex: 1, alignItems: 'flex-end', height: 50, justifyContent: 'center'}}>
              <Text style={{fontSize: 16, fontFamily: FONT.Muli.bold, color: BLACK.appLight}}>
                {item.amount}
              </Text>
              <Text
                style={{
                  fontSize: 12,
                  fontFamily: FONT.Muli.regular,
                  color: BLACK.appLight,
                  marginTop: 2,
                }}>
              </Text>
            </View>
          </View>
        </TouchableHighlight>
      ) : (
        <View
          style={{
            height: 80,
            width: SCREEN.width,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: GREEN.app,
          }}>
          <Image
            style={{height: 55, width: SCREEN.width, resizeMode: 'contain'}}
            source={require('../../assets/Ads/appleMusicAdd2.png')}
          />
        </View>
      )}
    </View>
  );
}
export default ReportItem;
