/* eslint-disable react-native/no-inline-styles */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import * as Font from 'expo-font';

import { ActivityIndicator, BackHandler, StatusBar, View } from 'react-native';
import React, { Component } from 'react';

import { Provider } from 'react-redux';
import SwitchNavigator from './App/navigation/AppNavigator';
import { createAppContainer } from 'react-navigation';
import store from './App/redux/store';

const MainNavigator = createAppContainer(SwitchNavigator);

let customFonts = {
  MuliBold: require('./App/assets/fonts/Muli-Bold.ttf'),
  MuliBoldItalic: require('./App/assets/fonts/Muli-BoldItalic.ttf'),
  MuliItalic: require('./App/assets/fonts/Muli-Italic.ttf'),
  MuliLight: require('./App/assets/fonts/Muli-Light.ttf'),
  MuliSemiBold: require('./App/assets/fonts/Muli-SemiBold.ttf'),
  Muli: require('./App/assets/fonts/Muli.ttf'),
  QuicksandBold: require('./App/assets/fonts/Quicksand-Bold.ttf'),
  QuicksandLight: require('./App/assets/fonts/Quicksand-Light.ttf'),
  QuicksandMedium: require('./App/assets/fonts/Quicksand-Medium.ttf'),
  QuicksandRegular: require('./App/assets/fonts/Quicksand-Regular.ttf'),
  QuicksandSemiBold: require('./App/assets/fonts/Quicksand-SemiBold.ttf'),
  SourceSansProBlack: require('./App/assets/fonts/SourceSansPro-Black.ttf'),
  SourceSansProBold: require('./App/assets/fonts/SourceSansPro-Bold.ttf'),
  SourceSansProExtraLight: require('./App/assets/fonts/SourceSansPro-ExtraLight.ttf'),
  SourceSansProLight: require('./App/assets/fonts/SourceSansPro-Light.ttf'),
  SourceSansProRegular: require('./App/assets/fonts/SourceSansPro-Regular.ttf'),
  SourceSansProSemiBold: require('./App/assets/fonts/SourceSansPro-SemiBold.ttf'),
};

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isReady: false,
      loaded: false,
    };
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton() {
    return true;
  }

  async componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    this._loadFontsAsync().then((r) => this.setState({ isReady: true }));
  }

  _loadFontsAsync = async () => {
    await Font.loadAsync(customFonts);
  };

  render() {
    return this.state.isReady ? (
      <Provider store={store}>
        <View style={{ flex: 1 }}>
          <StatusBar hidden={true} />
          <MainNavigator />
        </View>
      </Provider>
    ) : (
      <View
        style={{
          backgroundColor: 'transparent',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <ActivityIndicator
          size='large'
          color={'#000000'}
          style={{ marginTop: 30 }}
        />
      </View>
    );
  }
}
